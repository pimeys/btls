extern crate btls_module_defs;
use btls_module_defs::{HandleType, PubkeyReply, SignatureReply};
use btls_module_defs::BTLS_ALG_ECDSA_P256_SHA256;

#[derive(Clone)]
struct KeyHandle
{
	dummy: u8
}

impl HandleType for KeyHandle
{
	fn get_key_handle(name: &str, _errcode: &mut u32) -> Option<Self>
	{
		if name == "foo" { return Some(KeyHandle{dummy:0}) }
		None
	}
	fn request_sign(&self, reply: SignatureReply, _algorithm: u16, _data: &[u8])
	{
		//We can't actually sign with this.
		reply.finish(None)
	}
	fn request_pubkey<'a>(&self, reply: PubkeyReply<'a>)
	{
		let data = include_bytes!("key.raw");
		reply.finish(&data[..], &[BTLS_ALG_ECDSA_P256_SHA256])
	}
}


#[no_mangle]
pub unsafe extern "C" fn btls_mod_get_key_handle(a: *const btls_module_defs::c_char, b: *mut u32) -> u64
{
	KeyHandle::with_get_key_handle(a, b)
}

#[no_mangle]
pub unsafe extern "C" fn btls_mod_request_sign(a: u64, b: SignatureReply, c: u16, d: *const u8, e: usize)
{
	KeyHandle::with_request_sign(a, b, c, d, e)
}

#[no_mangle]
pub unsafe extern "C" fn btls_mod_request_pubkey(a: u64, b: PubkeyReply)
{
	KeyHandle::with_request_pubkey(a, b)
}
