//!Signature verification

#![forbid(unsafe_code)]
#![forbid(missing_docs)]

extern crate btls_aux_signature_algo;
use self::btls_aux_signature_algo::{SignatureAlgorithm, SignatureType, WrappedRecognizedSignatureAlgorithm};
extern crate btls_aux_ed448;
use self::btls_aux_ed448::{ed448_known_algo, ed448_verify_pkix};
extern crate btls_aux_signature_ecdsa;
use self::btls_aux_signature_ecdsa::{ecdsa_known_algo, ecdsa_verify_pkix};
extern crate btls_aux_signature_ed25519;
use self::btls_aux_signature_ed25519::{ed25519_known_algo, ed25519_verify_pkix};
extern crate btls_aux_signature_rsa;
use self::btls_aux_signature_rsa::{rsa_known_algo, rsa_verify_pkix};

struct SignatureVerifier
{
	recognize: fn(/*recalgo:*/&WrappedRecognizedSignatureAlgorithm, /*flags:*/u32) -> bool,
	validate: fn(/*key:*/&[u8], /*tbs:*/&[u8], /*recalgo*:*/&WrappedRecognizedSignatureAlgorithm,
		/*signature:*/&[u8], /*flags:*/u32) -> Result<(), ()>,
}

static VERIFIERS: [SignatureVerifier; 4] = [
	SignatureVerifier{recognize: ed25519_known_algo, validate: ed25519_verify_pkix},
	SignatureVerifier{recognize: ed448_known_algo, validate: ed448_verify_pkix},
	SignatureVerifier{recognize: ecdsa_known_algo, validate: ecdsa_verify_pkix},
	SignatureVerifier{recognize: rsa_known_algo, validate: rsa_verify_pkix},
];

///Validate a signature `signature` of algorithm `sigalgo` over message `tbs` under key `key`. `flags` is the
///condition flags.
///
///If the signature does not verify, fails with `Err(())`.
pub fn validate_signature<'a>(key: &[u8], tbs: &[u8], signature: SignatureBlock<'a>, flags: u32) ->
	Result<(), ()>
{
	let recalgo = WrappedRecognizedSignatureAlgorithm::from(signature.algorithm).ok_or(())?;
	for i in VERIFIERS.iter() {
		if (i.recognize)(&recalgo, flags) {
			return (i.validate)(key, tbs, &recalgo, signature.signature, flags);
		}
	}
	Err(())		//Not supported.
}

fn check_sig_algorithm_low(sigalgo: SignatureAlgorithm, flags: u32) -> bool
{
	let recalgo = match WrappedRecognizedSignatureAlgorithm::from(sigalgo) {
		Some(x) => x,
		None => return false
	};
	for i in VERIFIERS.iter() {
		if (i.recognize)(&recalgo, flags) { return true; }		//This verifier supports.
	}
	false		//Not supported.
}

///Is signature algorithm `sigalgo` supported with condition flags `flags`?
pub fn check_sig_algorithm_supported(sigalgo: SignatureAlgorithm, flags: u32) -> bool
{
	check_sig_algorithm_low(sigalgo, flags)
}

///A Signature block.
///
///Carries together an signature algorithm specification, plus a signature value.
#[derive(Copy,Clone,Debug)]
pub struct SignatureBlock<'a>
{
	///Algorithm.
	pub algorithm: SignatureAlgorithm<'a>,
	///Signature data.
	pub signature: &'a [u8]
}

impl<'a> SignatureBlock<'a>
{
	///Make signature block out of X.509 algorithm `algo` (with outer SEQUENCE header) and signature payload
	///`sig` (BIT STRING header and padding indicator has been stripped).
	pub fn from_x509(algo: &'a [u8], sig: &'a [u8]) -> SignatureBlock<'a>
	{
		SignatureBlock {
			algorithm: SignatureAlgorithm::X509(algo),
			signature: sig,
		}
	}
	///Make signature block out of TLS SignatureType `algo` and signature payload `sig`.
	pub fn from_tls(algo: SignatureType, sig: &'a [u8]) -> SignatureBlock<'a>
	{
		SignatureBlock {
			algorithm: SignatureAlgorithm::Tls(algo),
			signature: sig,
		}
	}
	///Validate a signature on message `tbs` using key `key` (given in X.509 SPKI format). Condition flags
	///used are `flags`.
	///
	///The condition flags may include bitwise OR of:
	///
	/// * VERIFY_FLAG_ALLOW_SHA1: Allow SHA-1.
	/// * VERIFY_FLAG_NO_RSA_PKCS1: Disallow RSA PKCS#1 v1.5
	///
	///If the signature does not verify, fails with `()`.
	pub fn verify(self, key: &[u8], tbs: &[u8], flags: u32) -> Result<(), ()>
	{
		validate_signature(key, tbs, self, flags)
	}
	///Convert a TLS DigitallySigned structure `dsig` into a signature block.
	pub fn from3<DigitallySigned:FromDigitallySigned<'a>+'a>(dsig: DigitallySigned) -> SignatureBlock<'a>
	{
		SignatureBlock {
			algorithm: SignatureAlgorithm::Tls(dsig.get_algorithm()),
			signature: dsig.get_payload(),
		}
	}
}

///Trait: Cast to TLS DigitallySigned structure.
pub trait FromDigitallySigned<'a>: Copy
{
	///Get the algorithm number.
	fn get_algorithm(self) -> SignatureType;
	///Get the signature payload.
	fn get_payload(self) -> &'a [u8];
}
