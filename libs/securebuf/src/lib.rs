//!More secure buffers.
//!
//!Buffers that are wiped on drop and routine to reliably wipe a buffer.

#![no_std]
#![forbid(missing_docs)]
#![allow(unsafe_code)]
extern crate btls_aux_serialization;
use btls_aux_serialization::SlicingExt;
use core::ops::{Deref, DerefMut};
use core::ptr::write_volatile;

///Wipe the contents of buffer `buf`.
///
///The writes are declared volatile, so buffer is overwritten even if it is not used afterwards.
#[allow(unsafe_code)]
pub fn wipe_buffer(buf: &mut [u8])
{
	for i in buf.iter_mut() {
		//This is safe since u8 is not Drop and the cast happens from reference (so the address is guaranteed
		//to be valid).
		unsafe{write_volatile(i as *mut u8, 0)};
	}
}

#[allow(unsafe_code)]
mod zerovalid
{
	///All zeroes is valid value.
	pub unsafe trait ZeroValid: Sized {
		///The all zeroes value.
		fn zero() -> Self;
	}
	unsafe impl ZeroValid for u8 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for i8 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for u16 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for i16 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for u32 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for i32 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for u64 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for i64 { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for usize { fn zero() -> Self { 0 }}
	unsafe impl ZeroValid for isize { fn zero() -> Self { 0 }}
}
pub use zerovalid::ZeroValid;

///Wipe the contents of buffer `buf`.
///
///The writes are declared volatile, so buffer is overwritten even if it is not used afterwards.
#[allow(unsafe_code)]
pub fn wipe_buffer2<T:ZeroValid>(buf: &mut [T])
{
	for i in buf.iter_mut() {
		//This is safe since T is not Drop and the cast happens from reference (so the address is guaranteed
		//to be valid).
		unsafe{write_volatile(i as *mut T, T::zero())};
	}
}

///Trait for "Behaves like a buffer".
pub trait Buffer: Deref+DerefMut
{
	///Return a reference to element at index `index` of the buffer.
	///
	///If index is in bounds, returns `Some(x)`, where `x` is a reference to the element. Otherwise returns
	///`None`.
	fn get(&self, index: usize) -> Option<&u8>;
	///Return a mutable reference to element at index `index` of the buffer.
	///
	///If index is in bounds, returns `Some(x)`, where `x` is a mutable reference to the element. Otherwise
	///returns `None`.
	fn get_mut(&mut self, index: usize) -> Option<&mut u8>;
}

///A stack buffer that is wiped upon drop.
///
///The buffer retains a reference to the original buffer, ensuring that buffer is not moved.
pub struct SecStackBuffer<'a>(&'a mut [u8]);

impl<'a> Drop for SecStackBuffer<'a>
{
	fn drop(&mut self)
	{
		wipe_buffer(self.0);
	}
}


impl<'a> Deref for SecStackBuffer<'a>
{
	type Target = [u8];
	fn deref(&self) -> &[u8]
	{
		self.0
	}
}

impl<'a> DerefMut for SecStackBuffer<'a>
{
	fn deref_mut(&mut self) -> &mut [u8]
	{
		self.0
	}
}

impl<'a> SecStackBuffer<'a>
{
	///Create a new buffer backed by `backing` of size `size`.
	///
	///If the backing store is not big enough, fails with `()`.
	pub fn new<'b>(backing: &'b mut [u8], size: usize) -> Result<SecStackBuffer<'b>, ()>
	{
		Ok(SecStackBuffer(backing.slice_np_len_mut(0, size).ok_or(())?))
	}
}

impl<'a> Buffer for SecStackBuffer<'a>
{
	fn get(&self, index: usize) -> Option<&u8>
	{
		self.0.get(index)
	}
	fn get_mut(&mut self, index: usize) -> Option<&mut u8>
	{
		self.0.get_mut(index)
	}
}
