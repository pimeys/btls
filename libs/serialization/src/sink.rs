//Silence warnings about std_imports if !stdlib.
#![allow(unused_imports)]

use super::{Asn1Tag, LengthTrait};
#[cfg(test)]
extern crate std;
#[cfg(feature="stdlib")]
mod std_imports
{
	extern crate std;
	pub use self::std::vec::Vec;
}
#[cfg(not(feature="stdlib"))]
mod std_imports {}
use self::std_imports::*;

///Sink that outputs raw byte data.
///
///This trait is implemented by struct [`SliceSink`], and additionally if this library was compiled with the `stdlib`
///feature, `Vec<u8>` implements this trait as well.
///
///[`SliceSink`]: struct.SliceSink.html
pub trait Sink
{
	///Append a slice `data` into sink.
	///
	///If the write causes the internal capacity of the sink to be exceeded, fails with `Err(())`.
	fn write_slice(&mut self, data: &[u8]) -> Result<(), ()>;
	///Alter past-written byte at index `ptr` to be `data` in the sink.
	///
	///Attempts to alter invalid indices are silently ignored.
	fn _alter(&mut self, ptr: usize, data: u8);
	///Read past-written byte at index `ptr` in the sink.
	///
	///If the specified index is out of range, returns 0.
	fn _readback(&self, ptr: usize) -> u8;
	///Get number of bytes written so far.
	fn written(&self) -> usize;
	///Remove `amount` bytes of data from the end of the sink.
	///
	///If th amount to remove is greater than amount added, all data is removed.
	fn _pop(&mut self, amount: usize);
	///Write a big-endian 8 bit value `data` into sink.
	///
	///If the write causes the internal capacity of the sink to be exceeded, fails with `()`.
	fn write_u8(&mut self, data: u8) -> Result<(),()>
	{
		self.write_slice(&[data])
	}
	///Write a big-endian 16 bit value `data` into sink.
	///
	///If the write causes the internal capacity of the sink to be exceeded, fails with `()`.
	fn write_u16(&mut self, data: u16) -> Result<(),()>
	{
		self.write_slice(&[(data>>8) as u8, data as u8])
	}
	///Write a big-endian 24 bit value `data` into sink.
	///
	///If the write causes the internal capacity of the sink to be exceeded, fails with `()`.
	fn write_u24(&mut self, data: u32) -> Result<(),()>
	{
		self.write_slice(&[(data>>16) as u8, (data>>8) as u8, data as u8])
	}
	///Write a big-endian 32 bit value `data` into sink.
	///
	///If the write causes the internal capacity of the sink to be exceeded, fails with `()`.
	fn write_u32(&mut self, data: u32) -> Result<(),()>
	{
		self.write_slice(&[(data>>24) as u8, (data>>16) as u8, (data>>8) as u8, data as u8])
	}
	///Write a big-endian 64 bit value `data` into sink.
	///
	///If the write causes the internal capacity of the sink to be exceeded, fails with `()`.
	fn write_u64(&mut self, data: u64) -> Result<(),()>
	{
		self.write_slice(&[(data>>56) as u8, (data>>48) as u8, (data>>40) as u8, (data>>32) as u8,
			(data>>24) as u8, (data>>16) as u8, (data>>8) as u8, data as u8])
	}
	///Alter past-written 8 bit big endian quantity at index `ptr` to be `data` in the sink.
	///
	///If the pointer is out of range, fails with `Err(())`.
	fn alter_u8(&mut self, ptr: usize, data: u8) -> Result<(),()>
	{
		fail_if!(ptr >= self.written(), ());
		self._alter(ptr, data);
		Ok(())
	}
	///Alter past-written 16 bit big endian quantity at index `ptr` to be `data` in the sink.
	///
	///If the pointer is out of range, fails with `Err(())`.
	fn alter_u16(&mut self, ptr: usize, data: u16) -> Result<(),()>
	{
		let ptr1 = ptr.checked_add(1).ok_or(())?;
		fail_if!(ptr1 >= self.written(), ());
		self._alter(ptr, (data>>8) as u8);
		self._alter(ptr1, data as u8);
		Ok(())
	}
	///Alter past-written 24 bit big endian quantity at index `ptr` to be `data` in the sink.
	///
	///If the pointer is out of range, fails with `Err(())`.
	fn alter_u24(&mut self, ptr: usize, data: u32) -> Result<(),()>
	{
		let ptr2 = ptr.checked_add(2).ok_or(())?;
		let ptr1 = ptr.checked_add(1).ok_or(())?;
		fail_if!(ptr2 >= self.written(), ());
		self._alter(ptr, (data>>16) as u8);
		self._alter(ptr1, (data>>8) as u8);
		self._alter(ptr2, data as u8);
		Ok(())
	}
	///Read back past-written 24 bit big endian quantity at index `ptr` from the sink.
	///
	///If the pointer is out of range, fails with `Err(())`. Otherwise returns `Ok(x)`, where `x` is the read
	///back value.
	fn readback_u24(&self, ptr: usize) -> Result<u32,()>
	{
		let ptr2 = ptr.checked_add(2).ok_or(())?;
		let ptr1 = ptr.checked_add(1).ok_or(())?;
		fail_if!(ptr2 >= self.written(), ());
		let mut x = 0;
		x |= (self._readback(ptr) as u32) << 16;
		x |= (self._readback(ptr1) as u32) << 8;
		x |= self._readback(ptr2) as u32;
		Ok(x)
	}
	///Get a marker for current position.
	fn marker_posonly(&self) -> SinkMarker
	{
		SinkMarker(self.written(), 0)
	}
	///Write a big-endian length marker, with size just enough for length range `length`, followed by data
	///written by a closure `cb`.
	///
	///This is very common operation when serializing TLS wire syntax.
	///
	///If the write causes the internal capacity of the sink to be exceeded, fails with `()`. Also fails if
	///the write is bigger than what can be encoded into length marker.
	fn vector_fn<'a,F,L:LengthTrait>(&'a mut self, length: L, mut cb: F) -> Result<(),()> where Self: Sized,
		F: FnMut(&mut Self) -> Result<(),()>
	{
		let marker = match length.get_bytes() {
			0 => {
				SinkMarker(self.written(), 0)
			},
			1 => {
				self.write_u8(0)?;
				SinkMarker(self.written(), 1)
			},
			2 => {
				self.write_u16(0)?;
				SinkMarker(self.written(), 2)
			},
			3 => {
				self.write_u24(0)?;
				SinkMarker(self.written(), 3)
			},
			_ => fail!(())
		};
		cb(self)?;
		marker.commit(self).map_err(|_|())?;
		Ok(())
	}
	///Write a ASN.1 TLV, tag `tag` followed by data written by a closure `cb`.
	///
	///This is very common operation when serializing ASN.1 DER.
	///
	///If the write causes the internal capacity of the sink to be exceeded, fails with `()`. Also fails if
	///the value of the tag is larger than 16,777,215 bytes.
	fn asn1_fn<'a, F>(&'a mut self, tag: Asn1Tag, mut cb: F) -> Result<(),()> where Self: Sized,
		F: FnMut(&mut Self) -> Result<(),()>
	{
		let (base,tag) = match tag {
			Asn1Tag::Universal(x, y) => (0x00 + if y { 0x20 } else { 0x00 }, x),
			Asn1Tag::Application(x, y) => (0x40 + if y { 0x20 } else { 0x00 }, x),
			Asn1Tag::ContextSpecific(x, y) => (0x80 + if y { 0x20 } else { 0x00 }, x),
			Asn1Tag::Private(x, y) => (0xC0 + if y { 0x20 } else { 0x00 }, x),
		};
		if tag < 31 {
			self.write_u8(base | (tag as u8))?;
		} else {
			self.write_u8(base | 31)?;
			//63, 56, 49, ..., 7
			for i in 0..9 {
				let j = 63 - 7 * i;
				if tag >= 1u64<<j { self.write_u8(128 | ((tag >> j) as u8))?; }
			}
			self.write_u8((tag & 127) as u8)?;
		}
		self.write_u32(0x83000000)?;	//83 00 00 00
		let w = self.written();
		let marker = SinkMarker(w, 255);
		cb(self)?;
		marker.commit(self).map_err(|_|())?;
		Ok(())
	}
	///Call callback `cb` with part of sink after marker `marker`.
	fn callback_after(&self, marker: SinkMarker, cb: &mut FnMut(&[u8])->());
}

///A position marker in sink
///
///This is used for subslice lengths and `Sink::callback_after`.
pub struct SinkMarker(usize, usize);

impl SinkMarker
{
	///Commit the marker, writing length of data since into sink `sink`.
	///
	///The sink must be the same this marker was from.
	///
	///If the section is too large, fails with `Err(size)`, where `size` is the size of the section that was
	///actually written.
	pub fn commit<S:Sink>(mut self, sink: &mut S) -> Result<(), usize>
	{
		fail_if!(sink.written() < self.0, 0usize);
		let size = sink.written() - self.0;
		match self.1 {
			0 => {
				//Do nothing, nothing to fix up.
				Ok(())
			},
			1 => {
				fail_if!(size > 255, size);
				fail_if!(self.0 < 1, size);
				sink.alter_u8(self.0 - 1, size as u8).map_err(|_|size)
			},
			2 => {
				fail_if!(size > 65535, size);
				fail_if!(self.0 < 2, size);
				sink.alter_u16(self.0 - 2, size as u16).map_err(|_|size)
			},
			3 => {
				fail_if!(size > 16777215, size);
				fail_if!(self.0 < 3, size);
				sink.alter_u24(self.0 - 3, size as u32).map_err(|_|size)
			},
			255 => {	//ASN.1.
				fail_if!(self.0 < 4, size);	//So substracts will be in-range. The last only
								//needs 3, but there is fourth byte as well.
				if size < 128 {
					//We need to write <len> and shift data by 3.
					sink.alter_u8(self.0 - 4, size as u8).map_err(|_|size)?;
					self.shift(sink, 3, size)
				} else if size < 256 {
					//We need to write 0x81 <len> and shift data by 2.
					sink.alter_u8(self.0 - 4, 0x81).map_err(|_|size)?;
					sink.alter_u8(self.0- 3, size as u8).map_err(|_|size)?;
					self.shift(sink, 2, size)
				} else if size < 65536 {
					//We need to write 0x82 <len> and shift data by 1.
					sink.alter_u8(self.0 - 4, 0x82).map_err(|_|size)?;
					sink.alter_u16(self.0 - 3, size as u16).map_err(|_|size)?;
					self.shift(sink, 1, size)
				} else if size < 16777216 {
					//The length of length is already 0x83, just alter the actual length.
					sink.alter_u24(self.0 - 3, size as u32).map_err(|_|size)
				} else {
					fail!(size);
				}
			},
			_ => fail!(size)
		}
	}
	fn shift<S:Sink>(&mut self, sink: &mut S, amount: usize, size: usize) -> Result<(), usize>
	{
		let mut idx = self.0;
		let maxlen = sink.written();
		fail_if!(idx < amount, size);	//The first iteration is the most extreme.
		while idx < maxlen {
			let x = sink._readback(idx);
			//idx >= amount, so substraction is valid.
			sink.alter_u8(idx - amount, x).map_err(|_|size)?;
			idx += 1;	//This is an index into valid array and never overflows usize.
		}
		sink._pop(amount);
		Ok(())
	}
}

///Sink that backs to writeable slice.
///
///Creates a [`Sink`] that is backed by a mutable slice.
///
///The maximum amount of writable data is of course bounded by the size of the slice.
///
///At end of lifetime, don't forget to call .written() to find out how much data has been written into it, so you
///can truncate the backing slice at the right point.
///
///[`Sink`]: trait.Sink.html
pub struct SliceSink<'a>(&'a mut [u8], usize);

impl<'a> SliceSink<'a>
{
	///Create a new slice sink, backing on slice `slice`.
	pub fn new(slice: &'a mut [u8]) -> SliceSink<'a>
	{
		SliceSink(slice, 0)
	}
}

impl<'a> Sink for SliceSink<'a>
{
	fn write_slice(&mut self, data: &[u8]) -> Result<(),()>
	{
		use super::SlicingExt;
		self.0.slice_np_len_mut(self.1, data.len()).ok_or(())?.copy_from_slice(data);
		self.1 += data.len();
		Ok(())
	}
	fn _alter(&mut self, ptr: usize, data: u8)
	{
		match self.0.get_mut(ptr) {
			Some(x) => *x = data,
			None => ()
		};
	}
	fn _readback(&self, ptr: usize) -> u8
	{
		self.0.get(ptr).map(|x|*x).unwrap_or(0)
	}
	fn written(&self) -> usize { self.1 }
	fn _pop(&mut self, amount: usize)
	{
		self.1 = self.1.saturating_sub(amount);
	}
	fn callback_after(&self, marker: SinkMarker, cb: &mut FnMut(&[u8])->())
	{
		use super::SlicingExt;
		match self.0.slice_np(marker.0..) {
			Some(x) => cb(x),
			None => cb(&[])
		};
	}
}

#[cfg(feature="stdlib")]
impl Sink for Vec<u8>
{
	fn write_slice(&mut self, data: &[u8]) -> Result<(),()>
	{
		self.extend_from_slice(data);
		Ok(())
	}
	fn _alter(&mut self, ptr: usize, data: u8)
	{
		match self.get_mut(ptr) {
			Some(x) => *x = data,
			None => ()
		};
	}
	fn _readback(&self, ptr: usize) -> u8
	{
		self.get(ptr).map(|x|*x).unwrap_or(0)
	}
	fn _pop(&mut self, amount: usize)
	{
		let nlen = self.len().saturating_sub(amount);
		self.truncate(nlen);
	}
	fn written(&self) -> usize { self.len() }
	fn callback_after(&self, marker: SinkMarker, cb: &mut FnMut(&[u8])->())
	{
		use super::SlicingExt;
		match self.slice_np(marker.0..) {
			Some(x) => cb(x),
			None => cb(&[])
		};
	}
}

#[test]
fn test_slicesink_basic()
{
	let mut buf = [0; 32];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.write_slice("Hello,".as_bytes()).unwrap();
		ssink.write_slice(" World!".as_bytes()).unwrap();
		ssink.written()
	};
	let buf2 = &buf[..len];
	assert_eq!(buf2, "Hello, World!".as_bytes());
}

#[test]
fn write_overflow_0()
{
	let mut buf = [0; 6];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.write_slice("XXXXXX".as_bytes()).unwrap();
		ssink.write_slice("YYY".as_bytes()).unwrap_err();
		ssink.written()
	};
	assert_eq!(len, 6);
}

#[test]
fn write_overflow_1()
{
	let mut buf = [0; 7];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.write_slice("XXXXXX".as_bytes()).unwrap();
		ssink.write_slice("YYY".as_bytes()).unwrap_err();
		ssink.written()
	};
	assert_eq!(len, 6);
}

#[test]
fn write_asn1_empty()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 32];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |_x|Ok(())).unwrap();
		ssink.written()
	};
	assert_eq!(&buf[..len], b"\x02\x00");
}

#[test]
fn write_asn1_1()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 32];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|sub.write_u8(5)).unwrap();
		ssink.written()
	};
	assert_eq!(&buf[..len], b"\x02\x01\x05");
}

#[test]
fn write_asn1_127()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 150];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag,|sub|{
			sub.write_u8(5).unwrap();
			for i in 0..126 { sub.write_u8(i).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 129);
	assert_eq!(&buf[..6], b"\x02\x7f\x05\x00\x01\x02");
}

#[test]
fn write_asn1_128()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 150];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		{
			ssink.asn1_fn(tag, |sub|{
				sub.write_u8(5).unwrap();
				for i in 0..127 { sub.write_u8(i).unwrap(); }
				Ok(())
			}).unwrap();
		}
		ssink.written()
	};
	assert_eq!(len, 131);
	assert_eq!(&buf[..7], b"\x02\x81\x80\x05\x00\x01\x02");
}

#[test]
fn write_asn1_255()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 300];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(5).unwrap();
			for i in 0..254 { sub.write_u8(i).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 258);
	assert_eq!(&buf[..7], b"\x02\x81\xff\x05\x00\x01\x02");
}

#[test]
fn write_asn1_256()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 300];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(5).unwrap();
			for i in 0..255 { sub.write_u8(i).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 260);
	assert_eq!(&buf[..8], b"\x02\x82\x01\x00\x05\x00\x01\x02");
}

#[test]
fn write_asn1_259()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 300];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(5).unwrap();
			for i in 0..255 { sub.write_u8(i).unwrap(); }
			for i in 0..3 { sub.write_u8(i).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 263);
	assert_eq!(&buf[..8], b"\x02\x82\x01\x03\x05\x00\x01\x02");
}

#[test]
fn write_asn1_65535()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 70000];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(5).unwrap();
			for _ in 0..65534 { sub.write_u8(0).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 65539);
	assert_eq!(&buf[..6], b"\x02\x82\xFF\xFF\x05\x00");
}

#[test]
fn write_asn1_65536()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 70000];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(5).unwrap();
			for _ in 0..65535 { sub.write_u8(0).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 65541);
	assert_eq!(&buf[..7], b"\x02\x83\x01\x00\x00\x05\x00");
}

#[test]
fn write_asn1_65053()
{
	let tag = Asn1Tag::Universal(2, false);
	let mut buf = [0; 70000];
	let len = {
		let mut ssink = SliceSink::new(&mut buf);
		ssink.asn1_fn(tag, |sub|{
			sub.write_u8(9).unwrap();
			for _ in 0..66052 { sub.write_u8(0).unwrap(); }
			Ok(())
		}).unwrap();
		ssink.written()
	};
	assert_eq!(len, 66058);
	assert_eq!(&buf[..7], b"\x02\x83\x01\x02\x05\x09\x00");
}

#[test]
fn write_asn1_16777215()
{
	use self::std::vec::Vec;
	let tag = Asn1Tag::Universal(2, false);
	let buf = {
		let mut ssink = Vec::new();
		ssink.resize(16777215+128,0);
		let bytes = {
			let mut tmp_ssink = SliceSink::new(&mut ssink);
			tmp_ssink.asn1_fn(tag, |sub|{
				sub.write_u8(9).unwrap();
				for _ in 0..16777214 { sub.write_u8(0).unwrap(); }
				Ok(())
			}).unwrap();
			tmp_ssink.written()
		};
		ssink.resize(bytes, 0);
		ssink
	};
	assert_eq!(buf.len(), 16777220);
	assert_eq!(&buf[..7], b"\x02\x83\xFF\xFF\xFF\x09\x00");
}

#[test]
fn write_asn1_16777216()
{
	use self::std::vec::Vec;
	let tag = Asn1Tag::Universal(2, false);
	let mut ssink = Vec::new();
	ssink.resize(16777215+128,0);
	{
		let mut tmp_ssink = SliceSink::new(&mut ssink);
		tmp_ssink.asn1_fn(tag, |sub| {
			sub.write_u8(9).unwrap();
			for _ in 0..16777215 { sub.write_u8(0).unwrap(); }
			Ok(())
		}).unwrap_err();
	};
}

#[cfg(test)]
fn write_asn1_test(tag: Asn1Tag, answer: &[u8], skip: usize)
{
	use self::std::vec::Vec;
	let buf = {
		let mut ssink = Vec::new();
		ssink.resize(answer.len() + 3, 0);	//The +3 is for extra tag space not winding used.
		let bytes = {
			let mut tmp_ssink = SliceSink::new(&mut ssink);
			tmp_ssink.asn1_fn(tag, |sub|sub.write_slice(&answer[skip..])).unwrap();
			tmp_ssink.written()
		};
		ssink.resize(bytes, 0);
		ssink
	};
	assert_eq!(&buf[..], answer);
}

#[test]
fn write_asn1_basic_tags()
{
	let tag = Asn1Tag::Universal(16, true);
	write_asn1_test(tag, b"\x30\x04ABCD", 2);
	let tag = Asn1Tag::Application(30, false);
	write_asn1_test(tag, b"\x5E\x04ABCD", 2);
	let tag = Asn1Tag::Application(29, true);
	write_asn1_test(tag, b"\x7D\x04ABCD", 2);
	let tag = Asn1Tag::ContextSpecific(28, false);
	write_asn1_test(tag, b"\x9C\x04ABCD", 2);
	let tag = Asn1Tag::ContextSpecific(27, true);
	write_asn1_test(tag, b"\xBB\x04ABCD", 2);
	let tag = Asn1Tag::Private(26, false);
	write_asn1_test(tag, b"\xDA\x04ABCD", 2);
	let tag = Asn1Tag::Private(25, true);
	write_asn1_test(tag, b"\xF9\x04ABCD", 2);
}

#[test]
fn write_asn1_extended_tags()
{
	let tag = Asn1Tag::Application(31, false);
	write_asn1_test(tag, b"\x5F\x1f\x04ABCD", 3);
	let tag = Asn1Tag::Application(127, true);
	write_asn1_test(tag, b"\x7F\x7f\x04ABCD", 3);
	let tag = Asn1Tag::Application(128, true);
	write_asn1_test(tag, b"\x7F\x81\x00\x04ABCD", 4);
	let tag = Asn1Tag::Application(131, true);
	write_asn1_test(tag, b"\x7F\x81\x03\x04ABCD", 4);
	let tag = Asn1Tag::Universal(16383, true);
	write_asn1_test(tag, b"\x3F\xff\x7f\x04ABCD", 4);
	let tag = Asn1Tag::Universal(16384, true);
	write_asn1_test(tag, b"\x3F\x81\x80\x00\x04ABCD", 5);
	let tag = Asn1Tag::Universal(16645, true);
	write_asn1_test(tag, b"\x3F\x81\x82\x05\x04ABCD", 5);
	let tag = Asn1Tag::Private(2097151, true);
	write_asn1_test(tag, b"\xFF\xff\xff\x7f\x04ABCD", 5);
	let tag = Asn1Tag::ContextSpecific(2097152, false);
	write_asn1_test(tag, b"\x9f\x81\x80\x80\x00\x04ABCD", 6);
}

#[cfg(feature="stdlib")]
#[test]
fn test_vector_sink()
{
	let mut ssink = Vec::new();
	ssink.write_slice("ABCD".as_bytes()).unwrap();
	ssink._alter(3, b'E');
	assert_eq!(ssink._readback(2), b'C');
	assert_eq!(ssink._readback(4), 0);
	assert_eq!(&ssink[..], b"ABCE");
	let mark = ssink.marker_posonly();
	ssink.write_slice("ABCD".as_bytes()).unwrap();
	ssink._pop(2);
	assert_eq!(&ssink[..], b"ABCEAB");
	assert_eq!(ssink.written(), 6);
	ssink.callback_after(mark, &mut |x|{
		assert_eq!(x, b"AB");
	})
}
