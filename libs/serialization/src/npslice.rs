use ::core::ops::{Range, RangeFrom, RangeFull, RangeTo};

///Range types for non-panicing slicing
///
///This trait models an index range.
pub trait RangeTrait
{
	///Get the starting index of the range.
	fn start(&self) -> usize;
	///Get the one past ending index of the range.
	///
	///If the range has an end, returns `Some(x)`, where `x` is the first element not in range. If the range
	///has no end, returns `None`.
	fn end(&self) -> Option<usize>;
}

///Extension trait implementing non-panicing subslicing
///
///This trait provodes method to extract subslice from slice without panicing. Great for extracting slices with
///indexes of unknown provenance.
///
///This trait is blanket-implemented for all slice types (`[T]`).
pub trait SlicingExt
{
	///Extract subslice range `r` from this slice.
	///
	///The subslice range can be specified using any of the four normal range types.
	///
	///If the specified range is in-range, returns `Some(subslice)`, where `subslice` is the subslice. If
	///the specified range is out-of-bounds, returns `None`.
	fn slice_np<X:RangeTrait>(&self, r: X) -> Option<&Self>;
	///Extract mutable subslice range `r` from this slice.
	///
	///The subslice range can be specified using any of the four normal range types.
	///
	///If the specified range is in-range, returns `Some(subslice)`, where `subslice` is the subslice. If
	///the specified range is out-of-bounds, returns `None`.
	fn slice_np_mut<X:RangeTrait>(&mut self, r: X) -> Option<&mut Self>;
	///Split a slice into two at index `pt`.
	///
	///If `pt` is in-range, returns `Some((x, y))`, where `x` is the subslice `..pt`, and `y` is the subslice
	///`pt..`. If `pt` is past the end of this slice, returns `None`.
	fn split_at_np<'a>(&'a self, pt: usize) -> Option<(&'a Self, &'a Self)>;
	///Extract subslice start `start` and length `len`.
	///
	///If the specified range is in-bounds, returns `Some(subslice)`, where `subslice` is the subslice. If
	///the specified range is out-of-bounds, returns `None`.
	fn slice_np_len(&self, start: usize, len: usize) -> Option<&Self>
	{
		let end = match start.checked_add(len) { Some(x) => x, None => return None };
		self.slice_np(start..end)
	}
	///Extract mutable subslice start `start` and length `len`.
	///
	///If the specified range is in-bounds, returns `Some(subslice)`, where `subslice` is the subslice. If
	///the specified range is out-of-bounds, returns `None`.
	///Extract mutable subslice with specified start and length.
	fn slice_np_len_mut(&mut self, start: usize, len: usize) -> Option<&mut Self>
	{
		let end = match start.checked_add(len) { Some(x) => x, None => return None };
		self.slice_np_mut(start..end)
	}
}



fn extract_range<X:RangeTrait>(slen: usize, r: X) -> Option<Range<usize>>
{
	let start = r.start();
	let end = r.end().unwrap_or(slen);
	if start > end || end > slen {
		//println!("slice_np failed: range={}..{}, length={}", start, end, self.len());
		//unreachable!();
		return None;
	}
	Some(start..end)
}

impl<T:Sized> SlicingExt for [T]
{
	fn slice_np<X:RangeTrait>(&self, r: X) -> Option<&Self>
	{
		let range = match extract_range(self.len(), r) { Some(x) => x, None => return None };
		Some(&self[range])
	}
	fn slice_np_mut<X:RangeTrait>(&mut self, r: X) -> Option<&mut Self>
	{
		let range = match extract_range(self.len(), r) { Some(x) => x, None => return None };
		Some(&mut self[range])
	}
	fn split_at_np<'a>(&'a self, pt: usize) -> Option<(&'a Self, &'a Self)>
	{
		if let (Some(a), Some(b)) = (self.slice_np(..pt), self.slice_np(pt..)) {
			Some((a, b))
		} else {
			None
		}
	}
}

impl RangeTrait for Range<usize>
{
	fn start(&self) -> usize { self.start }
	fn end(&self) -> Option<usize> { Some(self.end) }
}

impl RangeTrait for RangeTo<usize>
{
	fn start(&self) -> usize { 0 }
	fn end(&self) -> Option<usize> { Some(self.end) }
}

impl RangeTrait for RangeFrom<usize>
{
	fn start(&self) -> usize { self.start }
	fn end(&self) -> Option<usize> { None }
}

impl RangeTrait for RangeFull
{
	fn start(&self) -> usize { 0}
	fn end(&self) -> Option<usize> { None }
}
