//!AEAD encryption/decryption
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

#[macro_use]
extern crate btls_aux_fail;
extern crate btls_aux_chacha20poly1305aead;
extern crate btls_aux_nettle;
use btls_aux_chacha20poly1305aead::Chacha20Poly1305 as EChacha20Poly1305;
use btls_aux_nettle::GcmContext;


use std::sync::Mutex;

///Type of protector.
///
///This enumeration specifies the type of record protector.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum ProtectorType
{
	///AES-128-GCM
	Aes128Gcm,
	///AES-256-GCM
	Aes256Gcm,
	///Chacha20-Poly1305
	Chacha20Poly1305,
}

impl ProtectorType
{
	///Get bitmask constant for the algorithm.
	pub fn algo_const(&self) -> u64
	{
		1u64 << match self {
			&ProtectorType::Aes128Gcm => 0,
			&ProtectorType::Aes256Gcm => 1,
			&ProtectorType::Chacha20Poly1305 => 2,
		}
	}
	///Get human-readable name for the algorithm.
	pub fn as_string(&self) -> &'static str
	{
		match self {
			&ProtectorType::Aes128Gcm => "AES-128-GCM",
			&ProtectorType::Aes256Gcm => "AES-256-GCM",
			&ProtectorType::Chacha20Poly1305 => "Chacha20-Poly1305",
		}
	}
	///Create a new encryptor of the specified type, with key `key`.
	///
	///The `key` must be of of correct length: 16 bytes for AES-128-GCM, and 32 bytes for AES-256-GCM and
	///Chacha20-Poly1305.
	///
	///On success, returns the encryption context.
	///
	///On failure, returns `()`. Especially wrong key length causes a failure.
	pub fn new_encryptor(&self, key: &[u8]) -> Result<EncryptionKey, ()>
	{
		match self {
			&ProtectorType::Aes128Gcm => Ok(EncryptionKey(_EncryptionKey::Gcm(Mutex::new(GcmContext::new(
				key)?)))),
			&ProtectorType::Aes256Gcm => Ok(EncryptionKey(_EncryptionKey::Gcm(Mutex::new(GcmContext::new(
				key)?)))),
			&ProtectorType::Chacha20Poly1305 => Ok(EncryptionKey(_EncryptionKey::Chacha2(Mutex::new(
				EChacha20Poly1305::new(key)?)))),
		}
	}
	///Create a new decryptor of the specified type, with key `key`.
	///
	///The `key` must be of of correct length: 16 bytes for AES-128-GCM, and 32 bytes for AES-256-GCM and
	///Chacha20-Poly1305.
	///
	///On success, returns the decryption context.
	///
	///On failure, returns `()`. Especially wrong key length causes a failure.
	pub fn new_decryptor(&self, key: &[u8]) -> Result<DecryptionKey, ()>
	{
		match self {
			&ProtectorType::Aes128Gcm => Ok(DecryptionKey(_DecryptionKey::Gcm(Mutex::new(GcmContext::new(
				key)?)))),
			&ProtectorType::Aes256Gcm => Ok(DecryptionKey(_DecryptionKey::Gcm(Mutex::new(GcmContext::new(
				key)?)))),
			&ProtectorType::Chacha20Poly1305 => Ok(DecryptionKey(_DecryptionKey::Chacha2(Mutex::new(
				EChacha20Poly1305::new(key)?)))),
		}
	}
}

///An encryptor.
///
///Objects of this class handle encrypting data with AEAD algorithm and key.
pub struct EncryptionKey(_EncryptionKey);
enum _EncryptionKey
{
	Gcm(Mutex<GcmContext>),
	Chacha2(Mutex<EChacha20Poly1305>),
}

///A decryptor.
///
///Objects of this class handle decrypting data with AEAD algorithm and key.
pub struct DecryptionKey(_DecryptionKey);
enum _DecryptionKey
{
	Gcm(Mutex<GcmContext>),
	Chacha2(Mutex<EChacha20Poly1305>),
}

impl EncryptionKey
{
	///Encrypt a piece of data with the AEAD.
	///
	///The plaintext is passed in the first part of `in_out`. The last `pad` bytes of `in_out` are not
	///considered part of plaintext. `pad` has to be at least the size of tag the algorithm adds (currently
	///16 for all algorithms).
	///
	///The nonce used for encryption is in `nonce`, and it has to be correct length (currently 12 for all
	///algorithms). The nonce MUST be unique for all calls to encrypt with the same key, or ALL SECURITY IS
	///LOST. Additionally, the data in `ad` is authenticated.
	///
	///On success, the method returns number of bytes of ciphertext produced, and stored in `in_out`.
	///
	///On failure, this returns `()`. Especially `pad` being too small, or larger than `in_out`. causes
	///failures. Also, fails if `nonce` is of wrong length.
	pub fn encrypt(&self, nonce: &[u8], ad: &[u8], in_out: &mut [u8], pad: usize) -> Result<usize, ()>
	{
		match &self.0 {
			&_EncryptionKey::Gcm(ref x) => match x.lock() {
				Ok(mut y) => y.encrypt(nonce, ad, in_out, pad),
				Err(_) => fail!(())
			},
			&_EncryptionKey::Chacha2(ref x) => match x.lock() {
				Ok(mut y) => y.encrypt(nonce, ad, in_out, pad),
				Err(_) => fail!(())
			},
		}
	}
}

impl DecryptionKey
{
	///Decrypt a piece of data with the AEAD.
	///
	///The ciphertext is passed in as `in_out`. 
	///
	///The nonce used for decryption is in `nonce`, and must match the one passed to the corresponding
	///encryption, or the decryption fails. Additionally, the data in `ad` is compared to the one passed to
	///the encryption function. Again, mismatch causes decryption to fail.
	///
	///On success, the method returns number of bytes of plaintext produced, and stored in `in_out`.
	///
	///On failure, this returns `()`. Especially `pad` being too small, or larger than `in_out`. causes
	///failures. Also, fails if `nonce` is of wrong length, or data authentication fails.
	pub fn decrypt(&self, nonce: &[u8], ad: &[u8], in_out: &mut [u8]) -> Result<usize, ()>
	{
		match &self.0 {
			&_DecryptionKey::Gcm(ref x) => match x.lock() {
				Ok(mut y) => y.decrypt(nonce, ad, in_out),
				Err(_) => fail!(())
			},
			&_DecryptionKey::Chacha2(ref x) => match x.lock() {
				Ok(mut y) => y.decrypt(nonce, ad, in_out),
				Err(_) => fail!(())
			},
		}
	}
}
