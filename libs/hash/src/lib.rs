//!Various routines related to hashes and especially TLS PRF hashes.

#![forbid(unsafe_code)]
#![forbid(missing_docs)]

use std::cmp::min;
use std::convert::AsRef;
use std::fmt::{Debug, Display, Error as FmtError, Formatter};
use std::ops::DerefMut;
#[macro_use]
extern crate btls_aux_fail;
extern crate btls_aux_nettle;
extern crate btls_aux_securebuf;
extern crate sha3;
extern crate digest;
use btls_aux_securebuf::{SecStackBuffer, wipe_buffer};

macro_rules! make_bytes_wrapper
{
	($name:ident, $maxsize:expr) => {
		impl Clone for $name {
			fn clone(&self) -> Self
			{
				$name(self.0, self.1)
			}
		}
		impl Drop for $name { fn drop(&mut self) { wipe_buffer(&mut self.0); } }
		impl $name
		{
			///Convert a octet slice into type.
			///
			///If the octet slice is larger than the maximum size, fails with `Err((size, limit))`, where
			///`size` is the size of slice passed and `limit` is the maximum number of octets allowed.
			pub fn from2(y: &[u8]) -> Result<$name, (usize, usize)>
			{
				if y.len() > $maxsize { return Err((y.len(), $maxsize)); }
				let mut x = $name([0;$maxsize], y.len());
				(&mut x.0[..y.len()]).copy_from_slice(y);
				Ok(x)
			}
		}
		impl Debug for $name
		{
			fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
			{
				if self.1 > self.0.len() {
					fmt.write_fmt(format_args!("<Invalid data: {:?}, length={}>", &self.0[..],
						self.1))
				} else {
					fmt.write_fmt(format_args!("{:?}", &self.0[..self.1]))
				}
			}
		}
		impl AsRef<[u8]> for $name
		{
			fn as_ref(&self) -> &[u8]
			{
				//We outright assume length is valid.
				&self.0[..self.1]
			}
		}
	}
}

///Error in hash function.
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum HashFunctionError
{
	///Output length wrong (actual, reported).
	OutputLengthWrong(&'static str, usize, usize),
	///Output length too big (actual, limit).
	OutputLengthTooBig(&'static str, usize, usize),
	///Input length too big (actual, limit).
	InputLengthTooBig(&'static str, usize, usize),
	///HMAC is undefined with input and output sizes.
	HmacUndefined(&'static str, usize, usize),
	///Context size wrong.
	BadContextLength(&'static str, usize, usize),
	///Dummy hash function.
	DummyHash,
	///Unsupported hash function.
	UnsupportedHash,
}

impl Display for HashFunctionError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::HashFunctionError::*;
		match self {
			&OutputLengthWrong(name, x, y) => fmt.write_fmt(format_args!("Hash output length wrong \
				({}, got {}) for {}", y, x, name)),
			&OutputLengthTooBig(name, x, y) => fmt.write_fmt(format_args!("Hash output length too big \
				({}, limit {}) for {}", x, y, name)),
			&InputLengthTooBig(name, x, y) => fmt.write_fmt(format_args!("Hash input length too big \
				({}, limit {}) for {}", x, y, name)),
			&HmacUndefined(name, x, y) => fmt.write_fmt(format_args!("HMAC undefined for hash \
				(input size {}, output size {}) for {}", x, y, name)),
			&BadContextLength(name, x, y) => fmt.write_fmt(format_args!("Bad derive-secret context \
				length ({}, expected {}) for {}", x, y, name)),
			&DummyHash => fmt.write_str("Invalid operation for dummy hash"),
			&UnsupportedHash => fmt.write_str("Unsupported hash function"),
		}
	}
}

///A TLS PRF hash function.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum HashFunction
{
	///SHA-256.
	Sha256,
	///SHA-384
	Sha384,
	///SHA-512
	Sha512,
	///SHA3-256
	Sha3256,
	///SHA3-384
	Sha3384,
	///SHA3-512.
	Sha3512,
}

///A calculation context for hash function.
pub trait HashFunctionContext
{
	///Return the input block length of the hash.
	///
	///This is used in calculation of HMAC.
	fn input_length(&self) -> usize { self.function().input_length() }
	///Return the output length of the hash.
	fn output_length(&self) -> usize { self.function().output_length() }
	///Reset the hash to its initial state.
	fn reset(&mut self);
	///Add data `data` to data to be hashed.
	fn input(&mut self, data: &[u8]);
	///Get the hash for currently added data.
	///
	///Note: This does not modify the hash state (multitap).
	fn output(&self) -> Result<HashOutput, HashFunctionError>;
	///Get the hash function this context is for.
	fn function(&self) -> HashFunction;
	///Clone the hash state (forking).
	fn clone_state(&self) -> Box<HashFunctionContext+Send>;
	///Add slices in array `data` to data to be hashed in order.
	fn input_sg(&mut self, data: &[&[u8]])
	{
		//By default, just do input calls for each fragment.
		for i in data.iter() { self.input(i); }
	}
}

trait HashFunctionContextExtended: HashFunctionContext+Sized
{
	fn tls13_derive_secret(&mut self, key: &[u8], label: (&str, bool), hash: &HashOutput) ->
		Result<HashOutput, HashFunctionError>
	{
		let func = self.function();
		let sname = func.name();
		let outlen = func.output_length();
		let context = hash.as_ref();
		assert_eq!(context.len(), outlen);
		fail_if!(context.len() != outlen, HashFunctionError::BadContextLength(sname, context.len(),
			outlen));
		//Values very close to usize limit don't appear as buffer sizes.
		let lprefix = if label.1 { "tls13 " } else { "TLS 1.3, " };
		let extlabel = label.0.len() + lprefix.len();
		let xlen = &[(outlen >> 8) as u8, outlen as u8];
		let labellen = &[extlabel as u8];
		let contextlen = &[context.len() as u8];
		let trailer = &[0x01];
		self.hmac(key, |x| {
			x.input(xlen);
			x.input(labellen);
			x.input(lprefix.as_bytes());
			x.input(label.0.as_bytes());
			x.input(contextlen);
			x.input(context);
			x.input(trailer);
		})
	}
	//Limit label to 246 bytes, context to 255 and output to 65535 bytes or 255 output blocks, whichever is less.
	//The second component of label is omit-prefix flag.
	fn tls13_hkdf_expand_label(&mut self, key: &[u8], label: (&str, bool), context: &[u8], data: &mut [u8]) ->
		Result<(), HashFunctionError>
	{
		//Values very close to usize limit don't appear as buffer sizes.
		let lprefix = if label.1 { "tls13 " } else { "TLS 1.3, " };
		let extlabel = label.0.len() + lprefix.len();
		let xlen = &[(data.len() >> 8) as u8, data.len() as u8];
		let labellen = &[extlabel as u8];
		let contextlen = &[context.len() as u8];
		self.hkdf_expand(key, |x| {
			x.input(xlen);
			x.input(labellen);
			x.input(lprefix.as_bytes());
			x.input(label.0.as_bytes());
			x.input(contextlen);
			x.input(context);
		}, data)
	}
	//One shot hashing of data.
	fn oneshot<F>(&mut self, data_cb: F) -> Result<HashOutput, HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		{
			let mut tmp = HashInput(self);
			data_cb(&mut tmp);
		}
		self.output()
	}
	fn hmac<F>(&mut self, key: &[u8], data_cb: F) -> Result<HashOutput, HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		let func = self.function();
		let sname = func.name();
		self.reset();
		let inlen = func.input_length();
		let outlen = func.output_length();
		let mut expkey = [0; MAX_HASH_INPUT];
		fail_if!(inlen > expkey.len(), HashFunctionError::InputLengthTooBig(sname, inlen, expkey.len()));
		let mut expkey = SecStackBuffer::new(&mut expkey, inlen).unwrap();	//Can't fail.
		if key.len() <= expkey.len() {
			let _expkey: &mut [u8] = expkey.deref_mut();
			(&mut _expkey[..key.len()]).copy_from_slice(key);
		} else {
			self.input(key);
			let tmpout = self.output()?;
			let tmpout = tmpout.as_ref();
			fail_if!(expkey.len() < tmpout.len(), HashFunctionError::HmacUndefined(sname, expkey.len(),
				outlen));
			fail_if!(tmpout.len() != outlen, HashFunctionError::OutputLengthWrong(sname, tmpout.len(),
				outlen));
			(&mut expkey[..outlen]).copy_from_slice(tmpout);
			self.reset();
		}
		for i in expkey.deref_mut().iter_mut() { *i ^= 0x36; }
		self.input(&mut expkey);
		{
			let mut tmp = HashInput(self);
			data_cb(&mut tmp);
		}
		let interm = self.output()?;
		self.reset();
		for i in expkey.deref_mut().iter_mut() { *i ^= 0x6A; }
		self.input(&mut expkey);
		self.input(interm.as_ref());
		self.output()
	}
	fn tls_prf<F>(&mut self, key: &[u8], label: &str, data_cb: F, data: &mut [u8]) ->
		Result<(), HashFunctionError> where F: Fn(&mut HashInput) -> ()
	{
		let mut a = self.hmac(key, |x| {
			x.input(label.as_bytes());
			data_cb(x);
		})?;
		let mut ctr = 0;
		while ctr < data.len() {
			let b = self.hmac(key, |x| {
				x.input(a.as_ref());
				x.input(label.as_bytes());
				data_cb(x);
			})?;
			a = self.hmac(key, |x| { x.input(a.as_ref()); })?;
			let _b = b.as_ref();
			//By loop condition above, ctr < data.len().
			let tocopy = min(data.len() - ctr, _b.len());
			//The target fits because tocopy <= data.len() - ctr, and so tocopy + ctr <= data.len().
			//The soruce fits because tocopy <= _b.len().
			(&mut data[ctr..][..tocopy]).copy_from_slice(&_b[..tocopy]);
			//Now, tocopy + ctr is <= data.len() and fits into usize.
			ctr += tocopy;
		}
		Ok(())
	}
	//Don't request over 255 blocks of output! And check that key is at least one output block.
	fn hkdf_expand<F>(&mut self, key: &[u8], data_cb: F, data: &mut [u8]) -> Result<(), HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		let mut ctr = 0;
		let mut b = HashOutput([0;MAX_HASH_OUTPUT],0);
		let mut c = [0u8];
		while ctr < data.len() {
			c[0] += 1;	//
			b = self.hmac(key, |x| {
				x.input(b.as_ref());
				data_cb(x);
				x.input(&c);
			})?;
			let _b = b.as_ref();
			//By loop condition above, ctr < data.len().
			let tocopy = min(data.len() - ctr, _b.len());
			//The target fits because tocopy <= data.len() - ctr, and so tocopy + ctr <= data.len().
			//The soruce fits because tocopy <= _b.len().
			(&mut data[ctr..][..tocopy]).copy_from_slice(&_b[..tocopy]);
			//Now, tocopy + ctr is <= data.len() and fits into usize.
			ctr += tocopy;
		}
		Ok(())
	}
}

///The maximum size of hash input block.
pub const MAX_HASH_INPUT: usize = 136;	//SHA3-256 has 136-byte inputs, all others are less.
///The maximum size of hash output.
pub const MAX_HASH_OUTPUT: usize = 64;	//Assume hashes are below 512 bits.

///A hash output
pub struct HashOutput([u8; MAX_HASH_OUTPUT], usize);
make_bytes_wrapper!(HashOutput, MAX_HASH_OUTPUT);
///A buffer big enough to store one hash input block
pub struct HashInputBlock([u8; MAX_HASH_INPUT], usize);
make_bytes_wrapper!(HashInputBlock, MAX_HASH_INPUT);


impl HashFunction
{
	fn sha256() -> nettlef::Sha256 { nettlef::Sha256::new() }
	fn sha384() -> nettlef::Sha384 { nettlef::Sha384::new() }
	fn sha512() -> nettlef::Sha512 { nettlef::Sha512::new() }
	fn sha3_256() -> nettlef::Sha3256 { nettlef::Sha3256::new() }
	fn sha3_384() -> nettlef::Sha3384 { nettlef::Sha3384::new() }
	fn sha3_512() -> nettlef::Sha3512 { nettlef::Sha3512::new() }
	///Return the input block size of the hash in octets.
	///
	///This is used by HMAC.
	pub fn input_length(&self) -> usize
	{
		match self {
			&HashFunction::Sha256 => 64,
			&HashFunction::Sha384 => 128,
			&HashFunction::Sha512 => 128,
			&HashFunction::Sha3256 => 136,
			&HashFunction::Sha3384 => 104,
			&HashFunction::Sha3512 => 72,
		}
	}
	///Return the output length of hash in octets.
	pub fn output_length(&self) -> usize
	{
		match self {
			&HashFunction::Sha256 => 32,
			&HashFunction::Sha384 => 48,
			&HashFunction::Sha512 => 64,
			&HashFunction::Sha3256 => 32,
			&HashFunction::Sha3384 => 48,
			&HashFunction::Sha3512 => 64,
		}
	}
	///Return the name of the hash.
	pub fn name(&self) -> &'static str
	{
		match self {
			&HashFunction::Sha256 => "SHA-256(PRF)",
			&HashFunction::Sha384 => "SHA-384(PRF)",
			&HashFunction::Sha512 => "SHA-512(PRF)",
			&HashFunction::Sha3256 => "SHA3-256(PRF)",
			&HashFunction::Sha3384 => "SHA3-384(PRF)",
			&HashFunction::Sha3512 => "SHA3-512(PRF)",
		}
	}
	///Return the name of the hash (for human-readable purposes).
	pub fn as_string(&self) -> &'static str
	{
		match self {
			&HashFunction::Sha256 => "SHA-256",
			&HashFunction::Sha384 => "SHA-384",
			&HashFunction::Sha512 => "SHA-512",
			&HashFunction::Sha3256 => "SHA3-256",
			&HashFunction::Sha3384 => "SHA3-384",
			&HashFunction::Sha3512 => "SHA3-512",
		}
	}
	///Make a context for this hash.
	pub fn make_context(&self) -> Box<HashFunctionContext+Send>
	{
		match self {
			&HashFunction::Sha256 => Box::new(Self::sha256()),
			&HashFunction::Sha384 => Box::new(Self::sha384()),
			&HashFunction::Sha512 => Box::new(Self::sha512()),
			&HashFunction::Sha3256 => Box::new(Self::sha3_256()),
			&HashFunction::Sha3384 => Box::new(Self::sha3_384()),
			&HashFunction::Sha3512 => Box::new(Self::sha3_512()),
		}
	}
	///Compute HKDF-Extract with this hash, using key `key` (needs to be at least one hash output), and
	///salt `salt`.
	pub fn hkdf_extract(&self, key: &[u8], salt: &[u8]) -> Result<HashOutput, HashFunctionError>
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().hmac(salt, |x|x.input(key)),
			&HashFunction::Sha384 => Self::sha384().hmac(salt, |x|x.input(key)),
			&HashFunction::Sha512 => Self::sha512().hmac(salt, |x|x.input(key)),
			&HashFunction::Sha3256 => Self::sha3_256().hmac(salt, |x|x.input(key)),
			&HashFunction::Sha3384 => Self::sha3_384().hmac(salt, |x|x.input(key)),
			&HashFunction::Sha3512 => Self::sha3_512().hmac(salt, |x|x.input(key)),
		}
	}
	///Do TLS 1.3 derive-secret using this hash, with key `key`, label `label` (the second flag shortens the
	///prefix to "tls13 " instead of "TLS 1.3, ") and context hash `hash`.
	///
	///The key and context hash has to be equal to hash output in length.
	pub fn tls13_derive_secret(&self, key: &[u8], label: (&str, bool), hash: &HashOutput) ->
		Result<HashOutput, HashFunctionError>
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().tls13_derive_secret(key, label, hash),
			&HashFunction::Sha384 => Self::sha384().tls13_derive_secret(key, label, hash),
			&HashFunction::Sha512 => Self::sha512().tls13_derive_secret(key, label, hash),
			&HashFunction::Sha3256 => Self::sha3_256().tls13_derive_secret(key, label, hash),
			&HashFunction::Sha3384 => Self::sha3_384().tls13_derive_secret(key, label, hash),
			&HashFunction::Sha3512 => Self::sha3_512().tls13_derive_secret(key, label, hash),
		}
	}
	///Do TLS 1.3 hkdf-expand-label operation using this hash, using key `key`, label `label` (the second flag
	///shortens the prefix to "tls13 " instead of "TLS 1.3, ") and context `ctx`. The output is written into
	///`data`.
	///
	///Note, that label is limited to 246 bytes, context to 255, and output to 255 hash outputs.
	pub fn tls13_hkdf_expand_label(&self, key: &[u8], label: (&str, bool), ctx: &[u8], data: &mut [u8]) ->
		Result<(), HashFunctionError>
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().tls13_hkdf_expand_label(key, label, ctx, data),
			&HashFunction::Sha384 => Self::sha384().tls13_hkdf_expand_label(key, label, ctx, data),
			&HashFunction::Sha512 => Self::sha512().tls13_hkdf_expand_label(key, label, ctx, data),
			&HashFunction::Sha3256 => Self::sha3_256().tls13_hkdf_expand_label(key, label, ctx, data),
			&HashFunction::Sha3384 => Self::sha3_384().tls13_hkdf_expand_label(key, label, ctx, data),
			&HashFunction::Sha3512 => Self::sha3_512().tls13_hkdf_expand_label(key, label, ctx, data),
		}
	}
	///Hash the data emitted by `data_cb` in one shot.
	pub fn oneshot<F>(&self, data_cb: F) -> Result<HashOutput, HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().oneshot(data_cb),
			&HashFunction::Sha384 => Self::sha384().oneshot(data_cb),
			&HashFunction::Sha512 => Self::sha512().oneshot(data_cb),
			&HashFunction::Sha3256 => Self::sha3_256().oneshot(data_cb),
			&HashFunction::Sha3384 => Self::sha3_384().oneshot(data_cb),
			&HashFunction::Sha3512 => Self::sha3_512().oneshot(data_cb),
		}
	}
	///Perform HMAC with this hash using specified key `key` and data emitted by `data_cb`.
	pub fn hmac<F>(&self, key: &[u8], data_cb: F) -> Result<HashOutput, HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().hmac(key, data_cb),
			&HashFunction::Sha384 => Self::sha384().hmac(key, data_cb),
			&HashFunction::Sha512 => Self::sha512().hmac(key, data_cb),
			&HashFunction::Sha3256 => Self::sha3_256().hmac(key, data_cb),
			&HashFunction::Sha3384 => Self::sha3_384().hmac(key, data_cb),
			&HashFunction::Sha3512 => Self::sha3_512().hmac(key, data_cb),
		}
	}
	///Perform TLS-PRF operation using specified key `key`, label `label' and data emitted by `data_cb`. The
	///result is written to `data`.
	///
	///Note that this is not collision-resistant: label and context (`data_cb`) mix together.
	pub fn tls_prf<F>(&self, key: &[u8], label: &str, data_cb: F, data: &mut [u8]) ->
		Result<(), HashFunctionError> where F: Fn(&mut HashInput) -> ()
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().tls_prf(key, label, data_cb, data),
			&HashFunction::Sha384 => Self::sha384().tls_prf(key, label, data_cb, data),
			&HashFunction::Sha512 => Self::sha512().tls_prf(key, label, data_cb, data),
			&HashFunction::Sha3256 => Self::sha3_256().tls_prf(key, label, data_cb, data),
			&HashFunction::Sha3384 => Self::sha3_384().tls_prf(key, label, data_cb, data),
			&HashFunction::Sha3512 => Self::sha3_512().tls_prf(key, label, data_cb, data),
		}
	}
	//Don't request over 255 blocks of output! And check that key is at least one output block.
	///Perform raw HKDF-Expand operation with this hash, using key `key` (at least one hash output in length),
	///context emitted by `data_cb`. The results are written to `data`.
	pub fn hkdf_expand<F>(&self, key: &[u8], data_cb: F, data: &mut [u8]) -> Result<(), HashFunctionError>
		where F: Fn(&mut HashInput) -> ()
	{
		match self {
			&HashFunction::Sha256 => Self::sha256().hkdf_expand(key, data_cb, data),
			&HashFunction::Sha384 => Self::sha384().hkdf_expand(key, data_cb, data),
			&HashFunction::Sha512 => Self::sha512().hkdf_expand(key, data_cb, data),
			&HashFunction::Sha3256 => Self::sha3_256().hkdf_expand(key, data_cb, data),
			&HashFunction::Sha3384 => Self::sha3_384().hkdf_expand(key, data_cb, data),
			&HashFunction::Sha3512 => Self::sha3_512().hkdf_expand(key, data_cb, data),
		}
	}
}

///A handle to hash calculation context, passed to callbacks.
pub struct HashInput<'a>(&'a mut HashFunctionContext);

impl<'a> HashInput<'a>
{
	///Send data `data` to be hashed, appending on existing data.
	pub fn input(&mut self, data: &[u8])
	{
		self.0.input(data);
	}
}

#[derive(Copy,Clone)]
struct LengthCounter(usize);

impl HashFunctionContext for LengthCounter
{
	fn input_length(&self) -> usize { 0 }
	fn output_length(&self) -> usize { 0 }
	fn reset(&mut self) { self.0 = 0; }
	fn input(&mut self, data: &[u8]) {
		//Hash functions can't fail.
		self.0 = self.0.wrapping_add(data.len());
	}
	fn output(&self) -> Result<HashOutput, HashFunctionError> {
		Err(HashFunctionError::DummyHash)
	}
	fn function(&self) -> HashFunction { HashFunction::Sha256 }	//Just something.
	fn clone_state(&self) -> Box<HashFunctionContext+Send> {
		Box::new(self.clone())
	}
}

mod nettlef
{
	use super::{HashFunction as HashFunc, HashFunctionContext as HashCtxTrait, HashFunctionContextExtended,
		HashFunctionError, HashOutput};
	pub use btls_aux_nettle::{Sha1Ctx, Sha256Ctx, Sha384Ctx, Sha512Ctx};
	use btls_aux_nettle::{Sha3256Ctx as _Sha3256Ctx, Sha3384Ctx as _Sha3384Ctx, Sha3512Ctx as _Sha3512Ctx};
	use btls_aux_securebuf::wipe_buffer;
	use std::cmp::min;
	use sha3::{Sha3_256, Sha3_384, Sha3_512};
	use digest::Input;
	use digest::FixedOutput;

	#[derive(Clone)]
	struct Sha3256Fallback(Sha3_256);
	#[derive(Clone)]
	struct Sha3384Fallback(Sha3_384);
	#[derive(Clone)]
	struct Sha3512Fallback(Sha3_512);

	macro_rules! low_fallback_sha3
	{
		($name:ident, $outlen:expr, $lowtype:ident) => {
			impl $name
			{
				fn new() -> $name
				{
					$name($lowtype::default())
				}
				fn input(&mut self, data: &[u8])
				{
					self.0.digest(data);
				}
				fn reset(&mut self)
				{
					self.0 = $lowtype::default();
				}
				fn output(&mut self) -> [u8;$outlen]
				{
					let mut buf = [0;$outlen];
					let copy = self.0.clone();
					let res = copy.fixed_result();
					let minspace = min(buf.len(), res.len());
					//minspace should be buf.len() = res.len().
					(&mut buf[..minspace]).copy_from_slice(&res[..minspace]);
					buf
				}
			}
		}
	}

	low_fallback_sha3!(Sha3256Fallback, 32, Sha3_256);
	low_fallback_sha3!(Sha3384Fallback, 48, Sha3_384);
	low_fallback_sha3!(Sha3512Fallback, 64, Sha3_512);

	#[derive(Clone)]
	pub struct Sha3256Ctx(Result<_Sha3256Ctx, Sha3256Fallback>);
	#[derive(Clone)]
	pub struct Sha3384Ctx(Result<_Sha3384Ctx, Sha3384Fallback>);
	#[derive(Clone)]
	pub struct Sha3512Ctx(Result<_Sha3512Ctx, Sha3512Fallback>);

	macro_rules! fallback_sha3
	{
		($name:ident, $native:ident, $outlen:expr, $fallback:ident) => {
			impl $name
			{
				pub fn new() -> $name {
					$name($native::new().map_err(|_|$fallback::new()))
				}
				pub fn reset(&mut self)
				{
					match &mut self.0 {
						&mut Ok(ref mut x) => x.reset(),
						&mut Err(ref mut x) => x.reset(),
					}
				}
				pub fn input(&mut self, data: &[u8])
				{
					match &mut self.0 {
						&mut Ok(ref mut x) => x.input(data),
						&mut Err(ref mut x) => x.input(data),
					}
				}
				pub fn output(&mut self) -> [u8; $outlen]
				{
					match &mut self.0 {
						&mut Ok(ref mut x) => x.output(),
						&mut Err(ref mut x) => x.output(),
					}
				}
			}
		}
	}

	fallback_sha3!(Sha3256Ctx, _Sha3256Ctx, 32, Sha3256Fallback);
	fallback_sha3!(Sha3384Ctx, _Sha3384Ctx, 48, Sha3384Fallback);
	fallback_sha3!(Sha3512Ctx, _Sha3512Ctx, 64, Sha3512Fallback);
	
	#[derive(Clone)]
	pub struct Sha256(Sha256Ctx);
	#[derive(Clone)]
	pub struct Sha384(Sha384Ctx);
	#[derive(Clone)]
	pub struct Sha512(Sha512Ctx);
	#[derive(Clone)]
	pub struct Sha3256(Sha3256Ctx);
	#[derive(Clone)]
	pub struct Sha3384(Sha3384Ctx);
	#[derive(Clone)]
	pub struct Sha3512(Sha3512Ctx);

	impl Sha256 { pub fn new() -> Sha256 { Sha256(Sha256Ctx::new()) } }
	impl Sha384 { pub fn new() -> Sha384 { Sha384(Sha384Ctx::new()) } }
	impl Sha512 { pub fn new() -> Sha512 { Sha512(Sha512Ctx::new()) } }
	impl Sha3256 { pub fn new() -> Sha3256 { Sha3256(Sha3256Ctx::new()) } }
	impl Sha3384 { pub fn new() -> Sha3384 { Sha3384(Sha3384Ctx::new()) } }
	impl Sha3512 { pub fn new() -> Sha3512 { Sha3512(Sha3512Ctx::new()) } }

	macro_rules! hash_impl
	{
		($name:ident, $func:expr) => {
			impl HashCtxTrait for $name
			{
				fn reset(&mut self) { self.0.reset() }
				fn input(&mut self, data: &[u8]) { self.0.input(data); }
				fn input_sg(&mut self, data: &[&[u8]]) { for i in data.iter() { self.0.input(i); } }
				fn function(&self) -> HashFunc { $func }
				fn output(&self) -> Result<HashOutput, HashFunctionError>
				{
					let sname = self.function().name();
					let mut digest = self.0.clone().output();
					let ret = HashOutput::from2(&digest).map_err(|(x, y)|
						HashFunctionError::OutputLengthTooBig(sname,x, y))?;
					wipe_buffer(&mut digest);
					Ok(ret)
				}
				fn clone_state(&self) -> Box<HashCtxTrait+Send> { Box::new(self.clone()) }
			}
			impl HashFunctionContextExtended for $name {}
		}
	}

	hash_impl!(Sha256, HashFunc::Sha256);
	hash_impl!(Sha384, HashFunc::Sha384);
	hash_impl!(Sha512, HashFunc::Sha512);
	hash_impl!(Sha3256, HashFunc::Sha3256);
	hash_impl!(Sha3384, HashFunc::Sha3384);
	hash_impl!(Sha3512, HashFunc::Sha3512);
}

///Checksum function.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum ChecksumFunction
{
	///SHA-1 (INSECURE).
	Sha1Insecure,
	///SHA-256
	Sha256,
	///SHA-384
	Sha384,
	///SHA-512
	Sha512,
	///SHA3-256
	Sha3256,
	///SHA3-384
	Sha3384,
	///SHA3-512.
	Sha3512,
}

macro_rules! hash_calculate
{
	($ctx:expr, $name:expr, $input:expr) => {{
		let mut ctx = $ctx;
		ctx.input($input);
		let mut digest = ctx.output();
		let ret = HashOutput::from2(&digest).map_err(|(x,y)|HashFunctionError::OutputLengthTooBig(
			$name, x, y));
		wipe_buffer(&mut digest);
		ret
	}}
}

impl ChecksumFunction
{
	///Calculate a checksum value for data `input`.
	///
	///The calculation should not fail (absent bugs).
	pub fn calculate(&self, input: &[u8]) -> Result<HashOutput, HashFunctionError>
	{
		match self {
			&ChecksumFunction::Sha1Insecure => hash_calculate!(nettlef::Sha1Ctx::new(), "SHA-1", input),
			&ChecksumFunction::Sha256 => hash_calculate!(nettlef::Sha256Ctx::new(), "SHA-256", input),
			&ChecksumFunction::Sha384 => hash_calculate!(nettlef::Sha384Ctx::new(), "SHA-384", input),
			&ChecksumFunction::Sha512 => hash_calculate!(nettlef::Sha512Ctx::new(), "SHA-512", input),
			&ChecksumFunction::Sha3256 => hash_calculate!(nettlef::Sha3256Ctx::new(), "SHA3-256", input),
			&ChecksumFunction::Sha3384 => hash_calculate!(nettlef::Sha3384Ctx::new(), "SHA3-384", input),
			&ChecksumFunction::Sha3512 => hash_calculate!(nettlef::Sha3512Ctx::new(), "SHA3-512", input),
		}
	}
}

///The type of checksum output.
///
///Note: The output type can change!
pub type ChecksumOutput = [u8; 32];

///Compute a checksum of data `input`.
///
///The checksum is computed using unspecified strong hash.
pub fn checksum(input: &[u8]) -> ChecksumOutput
{
	let mut ctx = nettlef::Sha256Ctx::new();
	ctx.input(input);
	ctx.output()
}



#[cfg(test)]
fn test_hash_function(func: HashFunction)
{
	let mut ctx = func.make_context();
	assert_eq!(func.input_length(), ctx.input_length());
	assert_eq!(func.output_length(), ctx.output_length());
	assert!(func.input_length() <= MAX_HASH_INPUT);
	assert!(func.output_length() <= MAX_HASH_OUTPUT);
	assert_eq!(ctx.function(), func);
	let emptyhash = ctx.output().unwrap();
	let emptyhash2 = ctx.output().unwrap();
	assert_eq!(emptyhash.as_ref().len(), func.output_length());
	assert_eq!(emptyhash.as_ref(), emptyhash2.as_ref());
	ctx.input("foo".as_bytes());
	ctx.input("bar".as_bytes());
	let foobarhash = ctx.output().unwrap();
	let mut ctx2 = ctx.clone_state();
	ctx.reset();
	ctx.input("foobar".as_bytes());
	let foobarhash2 = ctx.output().unwrap();
	assert_eq!(foobarhash.as_ref(), foobarhash2.as_ref());
	ctx.input("baz".as_bytes());
	ctx2.input("baz".as_bytes());
	let foobarbazhash = ctx.output().unwrap();
	let foobarbazhash2 = ctx2.output().unwrap();
	assert_eq!(foobarbazhash.as_ref(), foobarbazhash2.as_ref());
	ctx.reset();
	let emptyhash3 = ctx.output().unwrap();
	assert_eq!(emptyhash.as_ref(), emptyhash3.as_ref());
}


#[test]
fn test_sha256_hash()
{
	test_hash_function(HashFunction::Sha256);
	assert_eq!(HashFunction::Sha256.input_length(), 64);
	assert_eq!(HashFunction::Sha256.output_length(), 32);
}

#[test]
fn test_sha384_hash()
{
	test_hash_function(HashFunction::Sha384);
	assert_eq!(HashFunction::Sha384.input_length(), 128);
	assert_eq!(HashFunction::Sha384.output_length(), 48);
}

#[test]
fn test_sha512_hash()
{
	test_hash_function(HashFunction::Sha512);
	assert_eq!(HashFunction::Sha512.input_length(), 128);
	assert_eq!(HashFunction::Sha512.output_length(), 64);
}

#[test]
fn test_sha3_256_hash()
{
	test_hash_function(HashFunction::Sha3256);
	assert_eq!(HashFunction::Sha3256.input_length(), 200-2*32);
	assert_eq!(HashFunction::Sha3256.output_length(), 32);
}

#[test]
fn test_sha3_384_hash()
{
	test_hash_function(HashFunction::Sha3384);
	assert_eq!(HashFunction::Sha3384.input_length(), 200-2*48);
	assert_eq!(HashFunction::Sha3384.output_length(), 48);
}

#[test]
fn test_sha3_512_hash()
{
	test_hash_function(HashFunction::Sha3512);
	assert_eq!(HashFunction::Sha3512.input_length(), 200-2*64);
	assert_eq!(HashFunction::Sha3512.output_length(), 64);
}
