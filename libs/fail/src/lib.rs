//!Macros `fail!` and `fail_if!`

#![forbid(unsafe_code)]
#![forbid(missing_docs)]

///Fail the current function with error `$f`.
///
///This is essentially `return Err($f);`, but the error does undergo `From` trait conversion if needed.
#[macro_export]
macro_rules! fail
{
	($f:expr) => {{ Err($f)?; unreachable!() }};
}

///If `$c` is true, fail the current function with error `$f`.
///
///This is essentially `if $c { return Err($f); }`, but the error does undergo `From` trait conversion if needed.
#[macro_export]
macro_rules! fail_if
{
	($c: expr, $f:expr) => { if $c { Err($f)?; } };
}
