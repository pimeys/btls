//!Time routines:
//!
//! * Timestamp type.
//! * Trait for transporting time (including current)
//! * Get current time routine

#![no_std]
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

use core::ops::Add;
use core::ops::Range;

///Timestamp type.
#[derive(Copy,Clone,PartialEq,PartialOrd,Eq,Ord,Debug)]
pub struct Timestamp(i64);

impl Timestamp
{
	///Create new timestamp.
	pub fn new(ts: i64) -> Timestamp { Timestamp(ts) }
	///Get the timestamp as i64.
	pub fn into_inner(self) -> i64 { self.0 }
	///Negative infinity.
	pub fn negative_infinity() -> Timestamp { Timestamp(i64::min_value()) }
	///Positive infinity.
	pub fn positive_infinity() -> Timestamp { Timestamp(i64::max_value()) }
	///First of timestamps.
	pub fn first(self, another: Timestamp) -> Timestamp
	{
		if self < another { self } else { another }
	}
	///Last of timestamps.
	pub fn last(self, another: Timestamp) -> Timestamp
	{
		if self > another { self } else { another }
	}
	///Delta another-self, floored to 0.
	pub fn delta(self, another: Timestamp) -> u64
	{
		if another.0 >= self.0 {
			let base = self.0;
			let baseoff = (!base).wrapping_add(1) as u64;
			baseoff.wrapping_add(another.0 as u64)
		} else {
			0
		}
	}
	///Is this timestamp in specified range.
	pub fn in_range(self, r: &Range<Timestamp>) -> bool
	{
		self >= r.start && self < r.end
	}
}

///Interface: Get timestamp.
pub trait TimeInterface
{
	///Get the relevant timestamp. This may return fixed time or current time.
	fn get_time(&self) -> Timestamp;
}

impl TimeInterface for Timestamp
{
	fn get_time(&self) -> Timestamp { *self }
}

impl Add<i64> for Timestamp
{
	type Output = Timestamp;
	fn add(self, x: i64) -> Timestamp
	{
		Timestamp(self.0 + x)
	}
}

impl Default for Timestamp
{
	fn default() -> Timestamp { Timestamp(0) }
}

///Get current time.
pub struct TimeNow;

impl TimeInterface for TimeNow
{
	fn get_time(&self) -> Timestamp
	{
		extern crate time;
		let now = time::get_time();
		Timestamp(now.sec)
	}
}


#[test]
fn timestamp_sub_normal()
{
	assert_eq!(Timestamp::new(123).delta(Timestamp::new(245)), 122);
}

#[test]
fn timestamp_sub_opposite()
{
	assert_eq!(Timestamp::new(245).delta(Timestamp::new(123)), 0);
}

#[test]
fn timestamp_sub_verylarge()
{
	assert_eq!(Timestamp::new(-0x6000000000000000).delta(Timestamp::new(0x6000000000000000)),
		0xC000000000000000);
}
