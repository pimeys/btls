//!Simple implementation of futures.
//!
//!TL;DR: You don't have to understand this unless you plan on implementing new ways of accessing private keys
//!or selecting certificates.
//!
//!Futures represent result of computation that might not have been done yet. This is handy for representing
//!asynchronous processes.
//!
//!This module has the following things:
//!
//! * `FutureReceiver`: Object that acts as the actual future value.
//! * `FutureSender`: Capability to settle a future value.
//! * `FutureCallback`: A trait for callback to call on settling the future. Useful for chaining futures.
//! * `create_future`: A function that creates a future value and capability to settle it.

#![forbid(unsafe_code)]
#![forbid(missing_docs)]

use std::mem::swap;
use std::sync::{Arc, Mutex};

///A function to call when future has settled.
pub trait FutureCallback<T: Sized+Send>
{
	///The future has settled.
	///
	///# Parameters:
	///
	/// * `self`: Context to execute with.
	/// * `value`: The value sent.
	///
	///# Return value:
	///
	/// * The new value to use.
	fn on_settled(&mut self, value: T) -> T;
}

///A future sender.
///
///This forms the send half of future value.
#[derive(Clone)]
pub struct FutureSender<T: Sized+Send>
{
	value: Arc<Mutex<FutureReceiverInner<T>>>
}

impl<T:Sized+Send> FutureSender<T>
{
	///Cause the connected future receiver to settle.
	///
	///Calling this consumes the sender and causes the associated receiver to settle.
	///
	///# Parameters:
	///
	/// * `self`: The sender to use.
	/// * `val`: The value to settle with.
	pub fn settle(self, val: T)
	{
		match self.value.lock() {
			Ok(mut x) =>  {
				let mut func = None;
				swap(&mut x.func, &mut func);
				if let Some(mut func2) = func {
					x.value = Some(func2.on_settled(val));
				} else {
					x.value = Some(val);
				}
			}
			Err(_) => (),
		}
	}
}

struct FutureReceiverInner<T: Sized+Send>
{
	func: Option<Box<FutureCallback<T>+Send>>,
	value: Option<T>
}

#[derive(Clone)]
enum _FutureReceiver<T: Sized+Send>
{
	Delayed(Arc<Mutex<FutureReceiverInner<T>>>),
	Immediate(T),
}

///A future receiver.
///
///This forms the receive half of future value.
///
///If created via the `From` trait, the value settles immediately to the value FutureReceiver is
///constructed from.
///
///For more information about futures, see [`create_future`].
///
///[`create_future`]: fn.create_future.html
#[derive(Clone)]
pub struct FutureReceiver<T: Sized+Send>(_FutureReceiver<T>);

impl<T:Sized+Send> From<T> for FutureReceiver<T>
{
	fn from(val: T) -> FutureReceiver<T>
	{
		FutureReceiver(_FutureReceiver::Immediate(val))
	}
}

impl<T:Sized+Send> FutureReceiver<T>
{
	///Set function to call when the future settles.
	///
	///# Parameters:
	///
	/// * `self`: The receiver to use.
	/// * `cb`: The callback to use.
	///
	///# Return value:
	///
	/// * None on success, Some(cb) if future has already settled.
	pub fn settled_cb(&mut self, cb: Box<FutureCallback<T>+Send>) -> Option<Box<FutureCallback<T>+Send>>
	{
		match &self.0 {
			&_FutureReceiver::Delayed(ref x) =>  match x.lock() {
				Ok(mut y) => {
					if y.value.is_none() {
						y.func = Some(cb);
						return  None;
					}
				}
				Err(_) => return None,
			},
			&_FutureReceiver::Immediate(_) => ()
		};
		return Some(cb);
	}
	///Has the value settled yet?
	///
	///# Parameters:
	///
	/// * `self`: The receiver to use.
	///
	/// * True if value has settled, false if not.
	pub fn settled(&self) -> bool
	{
		match &self.0 {
			&_FutureReceiver::Delayed(ref x) =>  match x.lock() {
				Ok(y) => y.value.is_some(),
				Err(_) => false,
			},
			&_FutureReceiver::Immediate(_) => true,
		}
	}
	///Read the settled value.
	///
	///This consumes the future receiver and reads the settled value.
	///
	///# Parameters:
	///
	/// * `self`: The receiver to use.
	///
	///# Returns:
	///
	/// * The value.
	///
	///# Failures:
	///
	/// * Fails with the receiver wrapped in `Err()` if the future hasn't settled yet.
	pub fn read(self) -> Result<T, FutureReceiver<T>>
	{
		match self.0 {
			_FutureReceiver::Immediate(x) => return Ok(x),
			_FutureReceiver::Delayed(y) => {
				if let Ok(mut z) = y.lock() {
					let mut x = None;
					swap(&mut z.value, &mut x);
					match x {
						Some(w) => return Ok(w),
						None => (),
					};
				};
				return Err(FutureReceiver(_FutureReceiver::Delayed(y)));
			}
		}
	}
}

///Create a future value.
///
///This function creates a `(sender, receiver)` pair for a future value of type `T`.
///
///# Futures:
///
///You don't need to understand futures with this library unless you are changing the way
///certificate lookup/signing works.
///
///Futures model computation that happens asynchronously, and whose final value may not be known yet.
///When the value is known, the future settles, and the value of computation can then be extracted
//from it.
///
///The computation invoked can also be synchronous: In that case, the value settles during
///invocation of the computation.
///
///Because the values are stored, they need to be `Sized` and because those can cross threads,
///the values need to be `Send`. Because no synchronous mutation is possible, `Sync` is not needed.
///
///In this implementation of futures, one can:
///
/// * Create an unsettled future by using this function. It also returns object that can settle the
///   future (and be consumed in process), by using the `.settle()` method.
/// * Create an immediately settled future by using the [`FutureReceiver::from`] function (from `From`
///   trait).
/// * Check if future has settled by using the `.settled()` value on the receiver.
/// * When the future has settled, read the value using `.read()` value on receiver, consuming the
///   receiver.
///
///# Return value:
///
/// * The future sender.
/// * The future receiver.
///
///[`FutureReceiver::from`]: struct.FutureReceiver.html
pub fn create_future<T:Sized+Send>() -> (FutureSender<T>, FutureReceiver<T>)
{
	let inner = Arc::new(Mutex::new(FutureReceiverInner{value:None, func:None}));
	let sender = FutureSender{value:inner.clone()};
	let receiver = FutureReceiver(_FutureReceiver::Delayed(inner));
	(sender, receiver)
}
