//!Assertion errors.
#![forbid(unsafe_code)]
#![forbid(missing_docs)]

use std::error::Error;
use std::fmt::{Display, Error as FmtError, Formatter};

///Assertion failed error.
#[derive(Clone,Debug,PartialEq,Eq)]
pub struct AssertFailed(pub String);

impl Display for AssertFailed
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		fmt.write_str(&self.0)
	}
}

impl Error for AssertFailed
{
	fn description(&self) -> &str
	{
		&self.0
	}
}

///Create an assertion failure error object, with specified message, without wrapping it in `Err()`.
///
///If one argument is passed, it is taken as the assertion failure error as-is. If multiple arguments are passed,
///the first argument is format string for assertion error, and the remaining are parameters for the format string.
///
///This is intended to be used in `.map_err()` methods, like:
///
///```no-run
///do_something().map_err(|(x,y)|assert_failure!("Foo is too big: {} is over limit {}", x, y))?;
///```
#[macro_export]
macro_rules! assert_failure
{
	($msg:expr) => {{
		use std::borrow::ToOwned;
		use $crate::AssertFailed;
		AssertFailed($msg.to_owned())
	}};
	($msg:expr,$($args:expr),*) => {{
		use $crate::AssertFailed;
		AssertFailed(format!($msg, $($args),*))
	}};
}

///If condition is false, create an assertion failure object and return it out of current function as failure.
///
///Takes a condition and one or more arguments. If the condition is false, the rest of arguments specify the
///message for assertion, as for [`assert_failure`]. The failure is then returned from the current function, by
///wrapping it in `Err()` and then applying the `?` operator.
///
///[`assert_failure`]: macro.assert_failure.html
#[macro_export]
macro_rules! sanity_check
{
	($cond:expr,$($x:expr),*) => {
		if !$cond { Err(assert_failure!($($x),*))?; }
	};
}

///Unconditionally create an assertion failure object and return it out of current function as failure.
///
///Takes one or more arguments. The arguments specify the message for assertion, as for [`assert_failure`]. The
///failure is then returned from the current function, by wrapping it in `Err()` and then applying the `?`
///operator.
///
///This macro never returns.
///
///[`assert_failure`]: macro.assert_failure.html
#[macro_export]
macro_rules! sanity_failed
{
	($msg:expr) => {{
		Err(assert_failure!($msg))?;
		unreachable!();
	}};
	($($x:expr),*) => {{
		Err(assert_failure!($($x),*))?;
		unreachable!();
	}};
}
