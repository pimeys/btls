//!Routines related to the `Ed448` signature algorithm.

#![forbid(unsafe_code)]
#![forbid(missing_docs)]
#![no_std]
extern crate iecclib;
use iecclib::{bigint_add, bigint_load, bigint_mul, Curve};
extern crate ihashlib;
use ihashlib::Hash;
extern crate btls_aux_signature_algo;
use btls_aux_signature_algo::{extract_raw_key, RecognizedSignatureAlgorithm, WrappedRecognizedSignatureAlgorithm};
#[macro_use]
extern crate btls_aux_fail;

///Verify `Ed448` signature `signature` over data `tbs` using key `key`.
///
///If the verification succeds, returns `Ok(())`, otherwise fails with `Err(())`.
pub fn ed448_verify(key: &[u8; 57], tbs: &[u8], signature: &[u8; 114]) -> Result<(), ()>
{
	let h = Hash::lookup("Shake256")?;
	let c = Curve::lookup("Edwards448")?;
	let mut ctx = h.make_size(114)?;
	ctx.update(b"SigEd448\x00\x00");
	ctx.update(&signature[0..57]);
	ctx.update(key);
	ctx.update(tbs);
	let mut _hram = [0; 114];
	let mut hram_raw = [0; 30];
	ctx.finalize(&mut _hram)?;
	bigint_load(&mut hram_raw, &_hram);
	let mut hram = [0; 15];
	c.reduce_partial(&mut hram, &hram_raw)?;
	//Hack: Load and store s to check it is in base form.
	let mut s = [0; 15];
	let mut scopy = [0; 57];
	bigint_load(&mut s, &signature[57..114]);
	c.reduce_full(&mut scopy[0..56], &s)?;			//reduce_full wants 56 octets, not 57!.
	fail_if!(&scopy[..] != &signature[57..114], ());	//s not canonical.

	let mut lhs = c.make_zero();
	let mut ha = c.make_zero();
	let mut rhs = c.make_zero();
	let mut tmp1 = c.make_zero();
	let r = c.make(&signature[0..57])?;		//Does canonical check.
	let a = c.make(key)?;				//Does canonical check.
	//sB = R + hA
	lhs.mul_base(&s)?;
	ha.mul(&hram, &a)?;
	rhs.add(&r, &ha)?;
	//Multiply both sides by 4 and dump.
	tmp1.double(&lhs)?;
	lhs.double(&tmp1)?;
	tmp1.double(&rhs)?;
	rhs.double(&tmp1)?;
	let mut _lhs = [0; 57];
	let mut _rhs = [0; 57];
	lhs.get(&mut _lhs)?;
	rhs.get(&mut _rhs)?;
	//Are equal?
	if &_lhs[..] == &_rhs[..] { Ok(()) } else { Err(()) }
}

///A `Ed448` key pair.
pub struct Ed448KeyPair
{
	private: [u8; 57],
	public: [u8; 57],
}

///Generate a `Ed25519` public key corresponding to private key `private2`.
///
///The argument `private2` should be set to 32 cryptographically random bytes.
pub fn generate_ed25519_pubkey(private2: &[u8; 32]) -> Result<[u8; 32], ()>
{
	let h = Hash::lookup("SHA-512")?;
	let c = Curve::lookup("curve25519")?;
	let mut ctx = h.make();
	ctx.update(private2);
	let mut intermediate = [0; 64];
	ctx.finalize(&mut intermediate)?;
	intermediate[31] &= 127;
	intermediate[31] |= 64;
	intermediate[0] &= !7;
	let mut scalar = [0; 8];
	bigint_load(&mut scalar, &intermediate[0..32]);
	let mut p = c.make_zero();
	p.mul_base(&scalar)?;
	let mut pubcmp = [0; 32];
	p.get(&mut pubcmp)?;
	Ok(pubcmp)
}

///Generate a `Ed448` public key corresponding to private key `private2`.
///
///The argument `private2` should be set to 57 cryptographically random bytes.
pub fn generate_ed448_pubkey(private2: &[u8; 57]) -> Result<[u8; 57], ()>
{
	let h = Hash::lookup("Shake256")?;
	let c = Curve::lookup("Edwards448")?;
	let mut ctx = h.make_size(114)?;
	ctx.update(private2);
	let mut intermediate = [0; 114];
	ctx.finalize(&mut intermediate)?;
	intermediate[56] = 0;
	intermediate[55] |= 128;
	intermediate[0] &= !3;
	let mut scalar = [0; 15];
	bigint_load(&mut scalar, &intermediate[0..56]);
	let mut p = c.make_zero();
	p.mul_base(&scalar)?;
	let mut pubcmp = [0; 57];
	p.get(&mut pubcmp)?;
	Ok(pubcmp)
}

impl Ed448KeyPair
{
	///Load `Ed448` key pair from private key `private` and public key `public`.
	///
	///This routine will fail if:
	///
	/// * Private key is not 57 octets.
	/// * Public key is not 57 octets.
	/// * The private and public key don't match each other.
	pub fn from_bytes(private: &[u8], public: &[u8]) -> Result<Ed448KeyPair, ()>
	{
		fail_if!(private.len() != 57 || public.len() != 57, ());
		let mut private2 = [0; 57];
		private2.copy_from_slice(private);
		let pubcmp = generate_ed448_pubkey(&private2)?;
		fail_if!(&pubcmp[..] != public, ());
		Ok(Ed448KeyPair{private:private2, public:pubcmp})
	}
	///Sign data `data` with keypair, writing the result to `signature`.
	///
	///This routine should never fail (absent a bug).
	pub fn sign(&self, data: &[u8], signature: &mut [u8; 114]) -> Result<(), ()>
	{
		signature[113] = 0;	//This byte is unused.
		let h = Hash::lookup("Shake256")?;
		let c = Curve::lookup("Edwards448")?;
		let mut ctx = h.make_size(114)?;
		ctx.update(&self.private);
		let mut intermediate = [0; 114];
		ctx.finalize(&mut intermediate)?;
		intermediate[56] = 0;
		intermediate[55] |= 128;
		intermediate[0] &= !3;
		ctx.reset_size(114)?;
		ctx.update(b"SigEd448\x00\x00");
		ctx.update(&intermediate[57..114]);
		ctx.update(data);
		let mut small_r = [0; 114];
		let mut small_r_raw = [0; 30];
		ctx.finalize(&mut small_r)?;		//r
		bigint_load(&mut small_r_raw, &small_r);
		let mut small_r = [0; 15];
		c.reduce_partial(&mut small_r, &small_r_raw)?;
		let mut p = c.make_zero();
		p.mul_base(&small_r)?;
		p.get(&mut signature[0..57])?;
		ctx.reset_size(114)?;
		ctx.update(b"SigEd448\x00\x00");
		ctx.update(&signature[0..57]);
		ctx.update(&self.public);
		ctx.update(data);
		let mut _hram = [0; 114];
		let mut hram_raw = [0; 30];
		ctx.finalize(&mut _hram)?;
		bigint_load(&mut hram_raw, &_hram);
		let mut hram = [0; 15];
		c.reduce_partial(&mut hram, &hram_raw)?;
		let mut a = [0; 15];
		let mut s = [0; 15];
		bigint_load(&mut a, &intermediate[0..57]);
		bigint_mul(&mut hram_raw, &hram, &a)?;
		c.reduce_partial(&mut hram, &hram_raw)?;
		bigint_add(&mut s, &small_r, &hram)?;
		c.reduce_full(&mut signature[57..113], &s)?;
		Ok(())
	}
}

///Wrapper for X448 key agreement functions.
pub struct X448Wrapper(&'static Curve);

impl X448Wrapper
{
	///Create a new wrapper.
	///
	///This function fails if X448 key agreement is not actually supported.
	pub fn new() -> Result<X448Wrapper, ()>
	{
		let crv = Curve::lookup("curve448")?;
		Ok(X448Wrapper(crv))
	}
	///Perform X448 key agremement using private key `secret` and peer public key `peerpub`. The result is
	///written to `shared`.
	///
	///Fails if the result is all zeroes.
	pub fn xcurve(&self, shared: &mut [u8; 56], secret: &[u8; 56], peerpub: &[u8]) -> Result<(), ()>
	{
		fail_if!(peerpub.len() != 56, ());	//Check length of shared secret.
		self.0.xcurve(shared, secret, peerpub)
	}
	///Generate a X448 public key corresponding to secret key `secret` and store the result to `public`.
	///
	///This function should not fail (absent a bug).
	pub fn xcurve_base(&self, public: &mut [u8; 56], secret: &[u8; 56]) -> Result<(), ()>
	{
		self.0.xcurve_base(public, secret)
	}
}

///Check if specified signature algorithm `sigalgo` can be validated by `ed448_verify_pkix()` under condition flags
///`flags`.
///
///This returns true if the signature is `Ed448` signature, false otherwise.
pub fn ed448_known_algo(sigalgo: &WrappedRecognizedSignatureAlgorithm, flags: u32) -> bool
{
	sigalgo.unwrap_if_allowed(flags).map(|x|x == RecognizedSignatureAlgorithm::SEd448).unwrap_or(false)
}

///Validate signature `signature` over `tbs` using key `key` (formatted in X.509 SPKI format).
///
///Returns `Ok(())` if the signature verifies, or fails with `Err(())` if the signature is invalid.
///
///`_dummy1` would be the signature algorithm, but this function only ever supports one signature algorithm.
///`_dummy2` would be flags, but this algorithm is legal regardless of flags.
pub fn ed448_verify_pkix(key: &[u8], tbs: &[u8], _dummy1: &WrappedRecognizedSignatureAlgorithm,
	signature: &[u8], _dummy2: u32) -> Result<(), ()>
{
	let key = extract_raw_key(key)?;
	let mut key2 = [0; 57];
	let mut signature2 = [0; 114];
	fail_if!(key.len() != key2.len(), ());
	fail_if!(signature.len() != signature2.len(), ());
	key2.copy_from_slice(key);
	signature2.copy_from_slice(signature);
	ed448_verify(&key2, tbs, &signature2)
}
