//!Ed25519 signing
#![forbid(unsafe_code)]
#![forbid(missing_docs)]
#[macro_use]
extern crate btls_aux_fail;
extern crate btls_aux_securebuf;
extern crate btls_aux_xed25519;
use btls_aux_securebuf::wipe_buffer;
use btls_aux_xed25519::{ed25519_pubkey, ed25519_sign};

use std::fmt::{Display, Error as FmtError, Formatter};


///Error in loading Ed25519 key pair
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum Ed25519KeyLoadingError
{
	///Invalid Ed25519 data length.
	InvalidEd25519DataLength(usize),
	///Invalid Ed25519 keypair (the private and public keys mismatch).
	InvalidEd25519Keypair,
	#[doc(hidden)]
	Hidden__
}

impl Display for Ed25519KeyLoadingError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use Ed25519KeyLoadingError::*;
		match self {
			&InvalidEd25519DataLength(x) => fmt.write_fmt(format_args!("Invalid Ed25519 key length \
				{} (expected 64)", x)),
			&InvalidEd25519Keypair => fmt.write_str("Inconsistent public/private keys in Ed25519 key"),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

///Ed25519 keypair.
pub struct Ed25519KeyPair([u8;64]);

impl Drop for Ed25519KeyPair
{
	fn drop(&mut self)
	{
		wipe_buffer(&mut self.0);
	}
}

impl Ed25519KeyPair
{
	///Sign data using Ed25519.
	///
	///The message `message`is signed with this key, and the output signature (64 bytes) is written into
	///`output`.
	///
	///On failure, returns `()`. Especially wrong signature length causes this function to fail.
	pub fn sign(&self, message: &[u8], output: &mut [u8]) -> Result<(), ()>
	{
		let mut signature = [0;64];
		ed25519_sign(&self.0, message, &mut signature);
		fail_if!(signature.len() != output.len(), ());
		output.copy_from_slice(&signature);
		Ok(())
	}
	///Create a Ed25519 key from internal representation `data`.
	///
	///On success, returns a tuple of Ed25519 keypair, and X.509 SPKI representation of Ed25519 public key.
	///
	///On failure, returns `Ed25519KeyLoadingError` describing the failure.
	pub fn from_bytes(data: &[u8]) -> Result<(Ed25519KeyPair, Vec<u8>), Ed25519KeyLoadingError>
	{
		use Ed25519KeyLoadingError::*;
		fail_if!(data.len() != 64, InvalidEd25519DataLength(data.len()));
		//Presume this is Ed25519 key.
		let privkey = &data[0..32];
		let pubkey = &data[32..64];
		let mut tmp = [0;32];
		let mut tmp2 = [0;32];
		tmp.copy_from_slice(privkey);
		ed25519_pubkey(&mut tmp2, &tmp);
		wipe_buffer(&mut tmp);
		fail_if!(&tmp2[..] != pubkey, InvalidEd25519Keypair);
		let mut output = Ed25519KeyPair([0;64]);
		output.0.copy_from_slice(data);
		let ed25519_header = b"\x30\x2A\x30\x05\x06\x03\x2B\x65\x70\x03\x21\x00";
		let mut out = Vec::new();
		out.extend_from_slice(ed25519_header);
		out.extend_from_slice(&pubkey);
		Ok((output, out))
	}
}
