extern crate gcc;
use gcc::Config;

fn main()
{
	{
		//Try general nettle test.
		let mut config = Config::new();
		config.file("src/ed25519-donna/ed25519.c");
		config.define("ED25519_REFHASH", None);		//SHA-512 is not invoked on lots of data.
		config.define("ED25519_CUSTOMRANDOM", None);	//Use the defined random routine.
		config.define("ED25519_SUFFIX", Some("_xed25519"));	//Less symbol collisions.
		config.compile("libxed25519.a");
	}
}
