//!Canonical S-Expression parser.

#![forbid(unsafe_code)]
#![forbid(missing_docs)]
#![no_std]
use core::str::from_utf8;
extern crate btls_aux_serialization;
use btls_aux_serialization::SlicingExt;
#[macro_use]
extern crate btls_aux_fail;

///A canonical S-Expression iterator.
#[derive(Copy,Clone, Debug)]
pub struct SexprParseStream<'a>
{
	data: &'a [u8],
	ptr: usize,
	depth: usize,
	root_status: u8,
}

///A token in S-Expression.
#[derive(Copy,Clone, PartialEq, Eq, Debug)]
pub enum SexprNode<'a>
{
	///Start of list.
	StartList,
	///End of list.
	EndList,
	///Raw octet string token.
	Data(&'a [u8]),
}

impl<'a> SexprParseStream<'a>
{
	fn next_token(&mut self) -> Result<SexprNode<'a>, bool>
	{
		fail_if!(self.ptr == self.data.len(), false);
		fail_if!(self.root_status == 2, true);
		let ntype = *self.data.get(self.ptr).ok_or(true)?;
		if ntype == 40 { //Start of list.
			if self.root_status == 0 { self.root_status = 1; }
			self.ptr = self.ptr.checked_add(1).ok_or(true)?;
			self.depth = self.depth.checked_add(1).ok_or(true)?;
			return Ok(SexprNode::StartList);
		} else if ntype == 41 { //End of list.
			if self.depth == 1 && self.root_status == 1 { self.root_status = 2; }
			self.ptr = self.ptr.checked_add(1).ok_or(true)?;
			self.depth = self.depth.checked_sub(1).ok_or(true)?;
			return Ok(SexprNode::EndList);
		} else if ntype >= 48 && ntype <= 57 { //Data.
			if self.depth == 0 { self.root_status = 2; }
			let mut len = 0usize;
			let mut nptr = self.ptr;
			loop {
				fail_if!(nptr == self.data.len(), true);
				let ch = *self.data.get(nptr).ok_or(true)?;
				if ch == 58 { break; }	//End on :
				fail_if!(ch < 48 || ch > 57, true);	//Only numbers and : valid.
				let dch = (ch - 48) as usize;
				len = len.checked_mul(10).ok_or(true)?.checked_add(dch).ok_or(true)?;
				nptr = nptr.checked_add(1).ok_or(true)?;
			}
			//Zero leading is only valid for '0'.
			fail_if!(ntype == 48 && nptr.saturating_sub(self.ptr) != 1, true);
			//Advance to start of data.
			nptr = nptr.checked_add(1).ok_or(true)?;
			let eptr = nptr.checked_add(len).ok_or(true)?;
			let data = self.data.slice_np(nptr..eptr).ok_or(true)?;
			self.ptr = eptr;
			return Ok(SexprNode::Data(data))
		} else {
			fail!(true);
		}
	}
}

impl<'a> Iterator for SexprParseStream<'a>
{
	type Item = Result<SexprNode<'a>, ()>;
	fn next(&mut self) -> Option<Result<SexprNode<'a>, ()>>
	{
		match self.next_token() {
			Ok(x) => Some(Ok(x)),
			Err(true) => Some(Err(())),
			Err(false) => None
		}
	}
}

impl<'a> SexprParseStream<'a>
{
	///Create a new S-Expression iterator for S-Expression `data`.
	pub fn new(data: &'a [u8]) -> SexprParseStream<'a>
	{
		SexprParseStream {
			data: data,
			ptr: 0,
			depth: 0,
			root_status: 0
		}
	}
	///Read start of list, and the first item in the list as octet string.
	///
	///Fails with `Err(())` if next two tokens are not start of list and raw octet string respectively.
	pub fn read_typed(&mut self) -> Result<&'a [u8], ()>
	{
		fail_if!(self.next().unwrap_or(Err(()))? != SexprNode::StartList, ());
		let ltype = if let SexprNode::Data(x) = self.next().unwrap_or(Err(()))? { x } else { fail!(()); };
		Ok(ltype)
	}
	///Read start of list, and the first item in the list as text string.
	///
	///Fails with `Err(())` if next two tokens are not start of list and raw octet string that is valid UTF-8,
	///respectively.
	pub fn read_typed_str(&mut self) -> Result<&'a str, ()>
	{
		from_utf8(self.read_typed()?).map_err(|_|())
	}
	///Read a raw octet string.
	///
	///Fails with `Err(())` if next token is raw octet string.
	pub fn next_data(&mut self) -> Result<&'a [u8], ()>
	{
		match self.next().unwrap_or(Err(())) { Ok(SexprNode::Data(x)) => Ok(x), _ => fail!(()) }
	}
	///Read end of list.
	///
	///Fails with `Err(())` if the next token is not end of list.
	pub fn assert_eol(&mut self) -> Result<(), ()>
	{
		match self.next().unwrap_or(Err(())) { Ok(SexprNode::EndList) => Ok(()), _ => fail!(()) }
	}
	///Read a text string.
	///
	///Fails with `Err(())` if next token is raw octet string that is valid UTF-8.
	pub fn next_str(&mut self) -> Result<&'a str, ()>
	{
		from_utf8(self.next_data()?).map_err(|_|())
	}
	///Read a list containing a pair of raw octet strings.
	///
	///Fails with `Err(())` if the next four tokens are not start of list, raw octets, raw octets and end of list
	///in that order.
	pub fn read_pair(&mut self) -> Result<(&'a [u8], &'a [u8]), ()>
	{
		if self.next().unwrap_or(Err(()))? != SexprNode::StartList { fail!(()); }
		let first = if let SexprNode::Data(x) = self.next().unwrap_or(Err(()))? { x } else { fail!(()); };
		let second = if let SexprNode::Data(x) = self.next().unwrap_or(Err(()))? { x } else { fail!(()); };
		if self.next().unwrap_or(Err(()))? != SexprNode::EndList { fail!(()); }
		Ok((first, second))
	}
}

#[test]
fn test_read_typed()
{
	let input = "(3:foo4:quux(6:barqux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.read_typed(), Ok(&b"foo"[..]));
	assert_eq!(strm.next_data(), Ok(&b"quux"[..]));
	assert_eq!(strm.read_typed(), Ok(&b"barqux"[..]));
	assert_eq!(strm.next_data(), Ok(&b"zot"[..]));
	strm.assert_eol().unwrap();
	strm.assert_eol().unwrap();
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_typed_str()
{
	let input = "(3:foo4:quux(6:barqux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.read_typed_str(), Ok("foo"));
	assert_eq!(strm.next_str(), Ok("quux"));
	assert_eq!(strm.read_typed_str(), Ok("barqux"));
	assert_eq!(strm.next_str(), Ok("zot"));
	strm.assert_eol().unwrap();
	strm.assert_eol().unwrap();
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_pair()
{
	let input = "((3:foo6:barqux)(4:quux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Ok((&b"foo"[..], &b"barqux"[..])));
	assert_eq!(strm.read_pair(), Ok((&b"quux"[..], &b"zot"[..])));
	assert_eq!(strm.next(), Some(Ok(SexprNode::EndList)));
	assert_eq!(strm.next(), None);
}

#[test]
fn test_read_pair_1elem()
{
	let input = "((3:foo))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}

#[test]
fn test_read_pair_2elem()
{
	let input = "((3:foo6:barqux))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Ok((&b"foo"[..], &b"barqux"[..])));
}

#[test]
fn test_read_pair_3elem()
{
	let input = "((3:foo6:barqux3:zot))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}

#[test]
fn test_read_pair_overlen1()
{
	let input = "((32:foo6:barqux))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}


#[test]
fn test_read_pair_overlen2()
{
	let input = "((3:foo66:barqux))";
	let mut strm = SexprParseStream::new(input.as_bytes());
	assert_eq!(strm.next(), Some(Ok(SexprNode::StartList)));
	assert_eq!(strm.read_pair(), Err(()));
}
