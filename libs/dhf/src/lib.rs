//!Routines related to Diffie-Hellman Functions in TLS.

#![forbid(unsafe_code)]
#![forbid(missing_docs)]
use std::fmt::{Debug, Display, Error as FmtError, Formatter};
extern crate btls_aux_nettle;
extern crate btls_aux_random;
extern crate btls_aux_securebuf;
use btls_aux_securebuf::wipe_buffer;
pub use btls_aux_random::secure_random;
#[macro_use]
extern crate btls_aux_fail;

macro_rules! make_bytes_wrapper
{
	($name:ident, $maxsize:expr) => {
		impl Clone for $name {
			fn clone(&self) -> Self
			{
				$name(self.0, self.1)
			}
		}
		impl Drop for $name { fn drop(&mut self) { wipe_buffer(&mut self.0); } }
		impl $name
		{
			///Convert a octet slice into type.
			///
			///If the octet slice is larger than the maximum size, fails with `Err((size, limit))`, where
			///`size` is the size of slice passed and `limit` is the maximum number of octets allowed.
			pub fn from2(y: &[u8]) -> Result<$name, (usize, usize)>
			{
				if y.len() > $maxsize { return Err((y.len(), $maxsize)); }
				let mut x = $name([0;$maxsize], y.len());
				(&mut x.0[..y.len()]).copy_from_slice(y);
				Ok(x)
			}
		}
		impl Debug for $name
		{
			fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
			{
				if self.1 > self.0.len() {
					fmt.write_fmt(format_args!("<Invalid data: {:?}, length={}>", &self.0[..],
						self.1))
				} else {
					fmt.write_fmt(format_args!("{:?}", &self.0[..self.1]))
				}
			}
		}
		impl AsRef<[u8]> for $name
		{
			fn as_ref(&self) -> &[u8]
			{
				//We outright assume length is valid.
				&self.0[..self.1]
			}
		}
	}
}

///TLS group identifier for `NSA P-256` group
pub const GRP_P256: u16 = 23;
///TLS group identifier for `NSA P-384` group
pub const GRP_P384: u16 = 24;
///TLS group identifier for `NSA P-521` group
pub const GRP_P521: u16 = 25;
///TLS group identifier for X25519 pseudo-group
pub const GRP_X25519: u16 = 29;
///TLS group identifier for X448 pseudo-group
pub const GRP_X448: u16 = 30;

///Error in key agreement.
#[derive(Clone,Debug,PartialEq,Eq)]
pub enum KeyAgreementError
{
	///Attempted to reuse DHE keypair.
	AttemptedDheReuse,
	///Peer public key length invalid.
	PeerPublicKeyLengthInvalid,
	///Peer public key invalid.
	PeerPublicKeyInvalid,
	///Can't export public key
	CantExportPublicKey,
	///Curve not supported.
	CurveNotSupported,
	///Can't generate DHE keypair
	CantGenerateDheKey,
	///X448 not supported.
	X448NotSupported,
	///Dummy public key.
	DummyPublicKey,
	///DH output too big (actual, limit).
	OutputLengthTooBig(&'static str, usize, usize),
	///Public key too big (actual, limit).
	PubkeyLengthTooBig(&'static str, usize, usize),
	#[doc(hidden)]
	Hidden__
}


impl Display for KeyAgreementError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::KeyAgreementError::*;
		match self {
			&AttemptedDheReuse => fmt.write_str("Tried to reuse DHE keypair"),
			&PeerPublicKeyLengthInvalid => fmt.write_str("Peer DHE public key length invalid"),
			&PeerPublicKeyInvalid => fmt.write_str("Peer DHE public key invalid"),
			&CurveNotSupported => fmt.write_str("Curve not supported"),
			&CantExportPublicKey => fmt.write_str("Can't export DHE public key"),
			&CantGenerateDheKey => fmt.write_str("Internal error: Failed to generate DH key"),
			&X448NotSupported => fmt.write_str("X448 not supported"),
			&DummyPublicKey => fmt.write_str("Dummy public key"),
			&OutputLengthTooBig(name,  x, y) => fmt.write_fmt(format_args!("DH output length too big \
				({}, limit {}) for {}", x, y, name)),
			&PubkeyLengthTooBig(name, x, y) => fmt.write_fmt(format_args!("DH public key length too big \
				({}, limit {}) for {}", x, y, name)),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

///The maximum possible size of DHF output.
pub const MAX_DHF_OUTPUT: usize = 66;	//Assume DHF outputs are below 528 bits.
///The maximum possible size of DHF public key.
pub const MAX_DHF_PUBKEY: usize = 136;	//Assume DHF public keys are below 1088 bits.

///Shared secret output from Diffie-Hellman Function.
pub struct DhfOutput([u8; MAX_DHF_OUTPUT], usize);
make_bytes_wrapper!(DhfOutput, MAX_DHF_OUTPUT);
///Public key output from Diffie-Hellman Function.
pub struct DhfPublicKey([u8; MAX_DHF_PUBKEY], usize);
make_bytes_wrapper!(DhfPublicKey, MAX_DHF_PUBKEY);

///A Diffie-Hellman Function.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum Dhf
{
	///`ECDH NSA P-256`, TLS style.
	NsaP256,
	///`ECDH NSA P-384`, TLS style.
	NsaP384,
	///`ECDH NSA P-521`, TLS style.
	NsaP521,
	///X25519.
	X25519,
	///X448.
	X448,
}

mod x448
{
	use super::secure_random;
	use super::{Dhf, DhfOutput, DhfPublicKey, DiffieHellmanKey, KeyAgreementError};

	extern crate btls_aux_ed448;
	use self::btls_aux_ed448::X448Wrapper;

	pub struct DhfX448(bool, [u8; 56], X448Wrapper);

	impl DhfX448
	{
		pub fn new() -> Result<DhfX448, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			let mut mat = [0; 56];
			secure_random(&mut mat);
			let crv = X448Wrapper::new().map_err(|_|X448NotSupported)?;
			Ok(DhfX448(false, mat, crv))
		}
	}

	impl DiffieHellmanKey for DhfX448
	{
		fn pubkey(&self) -> Result<DhfPublicKey, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			if self.0 {
				//Take the value.
				return Ok(DhfPublicKey::from2(&self.1[..]).map_err(|(x, y)|PubkeyLengthTooBig("X448",
					x, y))?);
			};
			let mut mat = [0; 56];
			self.2.xcurve_base(&mut mat, &self.1).map_err(|_|CantExportPublicKey)?;
			Ok(DhfPublicKey::from2(&mat[..]).map_err(|(x, y)|PubkeyLengthTooBig("X448", x, y))?)
		}
		fn agree(&mut self, peer_pub: &[u8]) -> Result<DhfOutput, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			let mut mat = [0; 56];
			let mut tmp = [0; 56];
			fail_if!(self.0, AttemptedDheReuse);
			self.2.xcurve(&mut mat, &self.1, peer_pub).map_err(|_|PeerPublicKeyInvalid)?;
			//Compute the public key here.
			self.2.xcurve_base(&mut tmp, &self.1).map_err(|_|CantExportPublicKey)?;
			self.1 = tmp;
			self.0 = true;
			Ok(DhfOutput::from2(&mat[..]).map_err(|(x,y)|OutputLengthTooBig("X448", x, y))?)
		}
		fn tls_id(&self) -> u16 { self.group().to_tls_id() }
		fn group(&self) -> Dhf { Dhf::X448 }
	}
}

mod x25519
{
	use super::secure_random;
	use super::{Dhf, DhfOutput, DhfPublicKey, DiffieHellmanKey, KeyAgreementError};

	extern crate btls_aux_xed25519;
	use self::btls_aux_xed25519::{x25519_agree, x25519_generate};
	use btls_aux_securebuf::wipe_buffer;

	pub struct DhfX25519(bool, [u8; 32], [u8; 32]);

	impl DhfX25519
	{
		pub fn new() -> Result<DhfX25519, KeyAgreementError>
		{
			let mut tmp = DhfX25519(false, [0;32], [0;32]);
			secure_random(&mut tmp.1);
			x25519_generate(&mut tmp.2, &tmp.1);
			Ok(tmp)
		}
	}

	impl DiffieHellmanKey for DhfX25519
	{
		fn pubkey(&self) -> Result<DhfPublicKey, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			Ok(DhfPublicKey::from2(&self.2[..]).map_err(|(x, y)|PubkeyLengthTooBig("X25519", x, y))?)
		}
		fn agree(&mut self, peer_pub: &[u8]) -> Result<DhfOutput, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			let mut ppub = [0; 32];
			let mut mat = [0; 32];
			fail_if!(ppub.len() != peer_pub.len(), PeerPublicKeyLengthInvalid);
			ppub.copy_from_slice(peer_pub);
			fail_if!(self.0, AttemptedDheReuse);
			x25519_agree(&mut mat, &self.1, &ppub);
			let mut syndrome = 0;
			for i in mat.iter() { syndrome |= *i; }
			fail_if!(syndrome == 0, PeerPublicKeyInvalid);
			wipe_buffer(&mut self.1);
			self.0 = true;
			Ok(DhfOutput::from2(&mat[..]).map_err(|(x,y)|OutputLengthTooBig("X25519", x, y))?)
		}
		fn tls_id(&self) -> u16 { self.group().to_tls_id() }
		fn group(&self) -> Dhf { Dhf::X25519 }
	}
}

mod nettle
{
	use super::{Dhf, DhfOutput, DhfPublicKey, DiffieHellmanKey, KeyAgreementError, MAX_DHF_PUBKEY};
	use btls_aux_nettle::{EccCurve, NsaEcdhKey};
	use btls_aux_securebuf::wipe_buffer;
	pub struct DhfNsaNettle(Option<NsaEcdhKey>, DhfPublicKey, Dhf);

	impl DhfNsaNettle
	{
		pub fn new(crvid: u32, func: Dhf) -> Result<DhfNsaNettle, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			let mut tmp = [0; MAX_DHF_PUBKEY];	//Large enough for NIST P-521.
			let crv = EccCurve::new(crvid).map_err(|_|CurveNotSupported)?;
			let (key, publen) = NsaEcdhKey::new(crv, &mut tmp).map_err(|_|CantGenerateDheKey)?;
			let pubkey = DhfPublicKey::from2(&tmp[..publen]).map_err(|(x,y)|PubkeyLengthTooBig(
				func.as_string(), x, y))?;
			Ok(DhfNsaNettle(Some(key), pubkey, func))
		}
	}

	impl DiffieHellmanKey for DhfNsaNettle
	{
		fn pubkey(&self) -> Result<DhfPublicKey, KeyAgreementError>
		{
			Ok(self.1.clone())
		}
		fn agree(&mut self, peer_pub: &[u8]) -> Result<DhfOutput, KeyAgreementError>
		{
			use super::KeyAgreementError::*;
			let mut tmp = [0; MAX_DHF_PUBKEY];		//Large enough for NIST P-521.
			let xlen = if let &Some(ref x) = &self.0 {
				x.agree(&mut tmp, peer_pub).map_err(|_|PeerPublicKeyInvalid)?
			} else {
				fail!(AttemptedDheReuse);
			};
			fail_if!(xlen > peer_pub.len(), CantExportPublicKey);
			self.0 = None;		//Destroy the private key.
			let matlen = (xlen - 1) / 2;	//Only the x component.
			let out = {
				let mat = &tmp[1..][..matlen];
				DhfOutput::from2(mat).map_err(|(x,y)|OutputLengthTooBig(self.2.as_string(),
					x, y))?
			};
			wipe_buffer(&mut tmp);
			Ok(out)
		}
		fn tls_id(&self) -> u16 { self.group().to_tls_id() }
		fn group(&self) -> Dhf { self.2 }
	}
}

impl Dhf
{
	///Construct `Dhf` enumeration value corresponding to TLS group id `id`.
	///
	///If the specified group `dhf` is known, returns `Some(dhf)`. Otherwise returns `None`.
	pub fn by_tls_id(id: u16) -> Option<Dhf>
	{
		match id {
			GRP_P256 => Some(Dhf::NsaP256),
			GRP_P384 => Some(Dhf::NsaP384),
			GRP_P521 => Some(Dhf::NsaP384),
			GRP_X25519 => Some(Dhf::X25519),
			GRP_X448 => Some(Dhf::X448),
			_  => None
		}
	}
	///Generate a new keypair for the group.
	///
	///If something goes wrong in generating the keypair, fails with appropriate error.
	pub fn generate_key(&self) -> Result<Box<DiffieHellmanKey+Send>, KeyAgreementError>
	{
		Ok(match self {
			&Dhf::NsaP256 => Box::new(nettle::DhfNsaNettle::new(0, *self)?),
			&Dhf::NsaP384 => Box::new(nettle::DhfNsaNettle::new(1, *self)?),
			&Dhf::NsaP521 => Box::new(nettle::DhfNsaNettle::new(2, *self)?),
			&Dhf::X25519  => Box::new(x25519::DhfX25519::new()?),
			&Dhf::X448    => Box::new(x448::DhfX448::new()?),
		})
	}
	///Return the TLS group ID corresponding to this DHF.
	pub fn to_tls_id(&self) -> u16
	{
		match self {
			&Dhf::NsaP256 => GRP_P256,
			&Dhf::NsaP384 => GRP_P384,
			&Dhf::NsaP521 => GRP_P521,
			&Dhf::X25519  => GRP_X25519,
			&Dhf::X448    => GRP_X448,
		}
	}
	///Return a bitmask corresponding to this DHF.
	pub fn dhfalgo_const(&self) -> u64
	{
		1u64 << match self {
			&Dhf::NsaP256 => 0,
			&Dhf::NsaP384 => 1,
			&Dhf::X25519  => 2,
			&Dhf::X448    => 3,
			&Dhf::NsaP521 => 4,
		}
	}
	///Return the name of this DHF as string.
	pub fn as_string(&self) -> &'static str
	{
		match self {
			&Dhf::NsaP256 => "NSA P-256",
			&Dhf::NsaP384 => "NSA P-384",
			&Dhf::NsaP521 => "NSA P-521",
			&Dhf::X25519  => "X25519",
			&Dhf::X448    => "X448",
		}
	}
}

///Diffie-Hellman Function key pair.
pub trait DiffieHellmanKey
{
	///Extract the public key from key pair.
	///
	///Public key can be extracted even from invalidated keypair.
	///
	///If something goes wrong, fails with appropriate error.
	///
	///The public key is in TLS uncompressed format.
	fn pubkey(&self) -> Result<DhfPublicKey, KeyAgreementError>;
	///Perform key agreement with public key `peer_pub`. The keypair is invalidated in process.
	///
	///If something goes wrong, fails with appropriate error. Especially the following trigger failures:
	///
	/// * All-zero output from `X25519` or `X448`.
	/// * Trying to perform agreement with invalidated keypair.
	///
	///The shared key is in whatever format the DH shared value in TLS is.
	fn agree(&mut self, peer_pub: &[u8]) -> Result<DhfOutput, KeyAgreementError>;
	///Get the TLS group id corresponding to the key.
	fn tls_id(&self) -> u16;
	///Get the Diffie-Hellman Function handle corresponding to the key.
	fn group(&self) -> Dhf;
}

#[cfg(test)]
fn test_key_agreement(func: Dhf)
{
	//This will also test lengths are correct.
	let mut alice = func.generate_key().unwrap();
	let mut bob = func.generate_key().unwrap();
	let alicepub = alice.pubkey().unwrap();
	let bobpub = bob.pubkey().unwrap();
	assert!(alicepub.as_ref().len() <= MAX_DHF_PUBKEY);
	assert!(bobpub.as_ref().len() <= MAX_DHF_PUBKEY);
	let ab = alice.agree(bobpub.as_ref()).unwrap();
	let ba = bob.agree(alicepub.as_ref()).unwrap();
	assert!(ab.as_ref().len() <= MAX_DHF_OUTPUT);
	assert!(ba.as_ref().len() <= MAX_DHF_OUTPUT);
	alice.agree(bobpub.as_ref()).unwrap_err();
	assert_eq!(ab.as_ref(), ba.as_ref());
}

#[cfg(test)]
fn test_key_agreement_invalid(func: Dhf)
{
	let mut alice = func.generate_key().unwrap();
	let mallorypub = [4, 2];
	//This better fail.
	alice.agree(&mallorypub).unwrap_err();
}

#[test]
fn x25519_key_agreement()
{
	test_key_agreement(Dhf::X25519);
}

#[test]
fn x448_key_agreement()
{
	test_key_agreement(Dhf::X448);
}

#[test]
fn nsa_p256_key_agreement()
{
	test_key_agreement(Dhf::NsaP256);
}

#[test]
fn nsa_p384_key_agreement()
{
	test_key_agreement(Dhf::NsaP384);
}

#[test]
fn nsa_p521_key_agreement()
{
	test_key_agreement(Dhf::NsaP521);
}

#[test]
fn x25519_key_agreement_invalid()
{
	test_key_agreement_invalid(Dhf::X25519);
}

#[test]
fn x448_key_agreement_invalid()
{
	test_key_agreement_invalid(Dhf::X448);
}

#[test]
fn nsa_p256_key_agreement_invalid()
{
	test_key_agreement_invalid(Dhf::NsaP256);
}

#[test]
fn nsa_p384_key_agreement_invalid()
{
	test_key_agreement_invalid(Dhf::NsaP384);
}


#[test]
fn nsa_p521_key_agreement_invalid()
{
	test_key_agreement_invalid(Dhf::NsaP521);
}
