use std::env::set_current_dir;
use std::env::current_dir;
use std::process::Command;
use std::fs::File;
use std::fs::create_dir_all;
use std::io::Read;
use std::io::Write;

fn copy_file(src: &str, dest: &str)
{
	let mut _src = File::open(src).unwrap();
	let mut _dest = File::create(dest).unwrap();
	let mut content = Vec::new();
	_src.read_to_end(&mut content).unwrap();
	_dest.write_all(&mut content).unwrap();
}

fn main()
{
	set_current_dir("src/chacha-opt").unwrap();
	Command::new("./configure").args(&["--pic"]).status().unwrap();
	Command::new("make").status().unwrap();
	set_current_dir("../poly1305-opt").unwrap();
	Command::new("./configure").args(&["--pic"]).status().unwrap();
	Command::new("make").status().unwrap();
	set_current_dir("../..").unwrap();
	create_dir_all(".libs").unwrap();
	copy_file("src/chacha-opt/bin/chacha.lib",".libs/libchacha.a");
	copy_file("src/poly1305-opt/bin/poly1305.lib",".libs/libpoly1305.a");
	let mut libsdir = current_dir().unwrap();
	libsdir.push(".libs");
	println!("cargo:rustc-link-search=native={}", libsdir.display());
	println!("cargo:rustc-link-lib=static=chacha");
	println!("cargo:rustc-link-lib=static=poly1305");
}
