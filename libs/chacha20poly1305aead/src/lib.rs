//!Chacha20-Poly1305-AEAD
#![forbid(missing_docs)]
#[macro_use]
extern crate btls_aux_fail;
extern crate btls_aux_securebuf;
use btls_aux_securebuf::wipe_buffer;

use std::sync::{Once, ONCE_INIT};
use std::mem::size_of;

//The first is key, the second is standby buffer. We need 64 bytes for padding, 256 for Chacha20 and 320 for
//poly1305.
///Key for Chacha20-Poly1305-AEAD encryption/decryption.
pub struct Chacha20Poly1305(Vec<u8>, usize);

impl Drop for Chacha20Poly1305
{
	fn drop(&mut self)
	{
		wipe_buffer(&mut self.0);
	}
}

impl Chacha20Poly1305
{
	///Create a new Chacha20-Poly1305-AEAD encryption/decryption context. The 32-byte key is passed as `key`.
	///
	///On success, returns the new context.
	///
	///On failure, returns `()`. Especially `key` not being 32 bytes causes a failure, but failing due to
	///internal error in Chacha20 or Poly1305 implementations is a possibility.
	pub fn new(key: &[u8]) -> Result<Chacha20Poly1305, ()>
	{
		fail_if!(key.len() != 32, ());
		static START: Once = ONCE_INIT;
		static mut POST_OK: bool = false;
		START.call_once(|| {
			if unsafe{chacha_startup()} != 0 { return; }
			if unsafe{poly1305_startup()} != 0 { return; }
			unsafe{POST_OK = true;}
		});
		fail_if!(unsafe{!POST_OK}, ());
		let mut bufferspace = Vec::with_capacity(706);
		bufferspace.resize(706, 0);
		//Reserve first 64 bytes for key, and align state to 64 byte boundary.
		let bufferoff = 64 + (64 - bufferspace.as_ptr() as usize % 64) % 64;
		(&mut bufferspace[..32]).copy_from_slice(key);
		Ok(Chacha20Poly1305(bufferspace, bufferoff))
	}
}

impl Chacha20Poly1305
{
	//Returns pointers to chacha20 and poly1305 state buffers.
	fn get_buffers(&mut self) -> (*mut u8, *mut u8)
	{
		let x = &mut self.0[self.1..];
		let (x, y) = x.split_at_mut(256);
		(x.as_mut_ptr(), y.as_mut_ptr())
	}
	//Assumes key is 32 bytes, nonce is 12.
	fn initialize_state(state: &mut [u8], key: &[u8], nonce: &[u8])
	{
		for i in 48..256 { state[i] = 0; }
		(&mut state[..32]).copy_from_slice(key);
		(&mut state[36..48]).copy_from_slice(nonce);
		if u32::from_le(0x12345678) == 0x12345678 {
			state[48] = 20;		//Little-endian.
		} else {
			state[47+size_of::<usize>()] = 20;	//Big-endian.
		}
	}
	fn encrypt_decrypt_raw(chacha_state: *mut u8, in_out: &mut [u8])
	{
		//We can't have any pending input. However, we can leave pending output, so call finish to extract
		//the tail.
		unsafe {
			let r = chacha_update(chacha_state, in_out.as_ptr(), in_out.as_mut_ptr(), in_out.len());
			chacha_final(chacha_state, (&mut in_out[r..]).as_mut_ptr());
		}
	}
	//Assumes key is all zeroes at call!
	fn compute_poly_key(chacha_state: *mut u8, key: &mut [u8;64])
	{
		//Outputs multiple of 64 bytes are always synchronously emitted. Additionally, we can't have any
		//pending input.
		unsafe {chacha_update(chacha_state, key.as_ptr(), key.as_mut_ptr(), 64)};
	}
	///Encrypt a piece of data with the AEAD.
	///
	///The plaintext is passed in the first part of `in_out`. The last `pad` bytes of `in_out` are not
	///considered part of plaintext. `pad` has to be at least 16.
	///
	///The nonce used for encryption is in `nonce`, and it has to be 12 bytes. The nonce MUST be unique for all
	///calls to encrypt with the same key, or ALL SECURITY IS LOST. Additionally, the data in `ad` is
	///authenticated.
	///
	///On success, the method returns number of bytes of ciphertext produced, and stored in `in_out`.
	///
	///On failure, this returns `()`. Especially `pad` being less than 16, or bigger than `in_out` causes a
	///failure. Also, fails if `nonce` is not 12 bytes or if data authentication fails.
	pub fn encrypt(&mut self, nonce: &[u8], ad: &[u8], in_out: &mut [u8], pad: usize) -> Result<usize, ()>
	{
		fail_if!(nonce.len() != 12, ());
		fail_if!(pad < 16, ());
		fail_if!(pad > in_out.len(), ());
		{
			let (key, x) = self.0.split_at_mut(64);
			let (_, cs) = x.split_at_mut(self.1-64);
			Self::initialize_state(cs, &key[..32], nonce);
		}
		let reallen = in_out.len() - pad;
		let (chacha_state, poly_state) = self.get_buffers();
		let hint = ((ad.len() + 15) / 16 + (reallen + 15) / 16 + 1) * 16;
		let mut polykey = [0u8; 64];
		let mut tmp = [0u8; 16];

		Self::compute_poly_key(chacha_state, &mut polykey);
		unsafe{poly1305_init_ext(poly_state, polykey.as_ptr(), hint)};
		unsafe{poly1305_update(poly_state, ad.as_ptr(), ad.len())};
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), (16 - ad.len()) % 16)};
		Self::encrypt_decrypt_raw(chacha_state, &mut in_out[..reallen]);
		unsafe{poly1305_update(poly_state, in_out.as_ptr(), reallen)};
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), (16 - reallen % 16) % 16)};
		for i in 0..8 {
			tmp[0+i] = (ad.len() >> 8 * i) as u8;
			tmp[8+i] = (reallen >> 8 * i) as u8;
		}
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), 16)};
		unsafe{poly1305_finish(poly_state, tmp.as_mut_ptr())};
		(&mut in_out[reallen..][..16]).copy_from_slice(&tmp);
		Ok(reallen+16)
	}
	///Decrypt a piece of data with the AEAD.
	///
	///The ciphertext is passed in as `in_out`.
	///
	///The nonce used for decryption is in `nonce`, and must match the one passed to the corresponding
	///encryption, or the decryption fails. Additionally, the data in `ad` is compared to the one passed to
	///the encryption function. Again, mismatch causes decryption to fail.
	///
	///On success, the method returns number of bytes of plaintext produced, and stored in `in_out`.
	///
	///On failure, this returns `()`. Especially `nonce` not being 12 bytes. Also fails if data 
	///authentication fails.
	pub fn decrypt(&mut self, nonce: &[u8], ad: &[u8], in_out: &mut [u8]) -> Result<usize, ()>
	{
		fail_if!(nonce.len() != 12, ());
		fail_if!(in_out.len() < 16, ());
		{
			let (key, x) = self.0.split_at_mut(64);
			let (_, cs) = x.split_at_mut(self.1-64);
			Self::initialize_state(cs, &key[..32], nonce);
		}
		let reallen = in_out.len() - 16;
		let (chacha_state, poly_state) = self.get_buffers();
		let hint = ((ad.len() + 15) / 16 + (reallen + 15) / 16 + 1) * 16;
		let mut polykey = [0u8; 64];
		let mut tmp = [0u8; 16];

		Self::compute_poly_key(chacha_state, &mut polykey);
		unsafe{poly1305_init_ext(poly_state, polykey.as_ptr(), hint)};
		unsafe{poly1305_update(poly_state, ad.as_ptr(), ad.len())};
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), (16 - ad.len()) % 16)};
		unsafe{poly1305_update(poly_state, in_out.as_ptr(), reallen)};
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), (16 - reallen % 16) % 16)};
		for i in 0..8 {
			tmp[0+i] = (ad.len() >> 8 * i) as u8;
			tmp[8+i] = (reallen >> 8 * i) as u8;
		}
		unsafe{poly1305_update(poly_state, tmp.as_ptr(), 16)};
		unsafe{poly1305_finish(poly_state, tmp.as_mut_ptr())};
		let mut syndrome = 0;
		{
			let mtag = &in_out[reallen..];
			for i in 0..16 { syndrome |= mtag[i] ^ tmp[i]; }
		}
		fail_if!(syndrome != 0, ());	//Bad tag.

		Self::encrypt_decrypt_raw(chacha_state, &mut in_out[..reallen]);
		Ok(reallen)
	}
}

extern
{
	fn chacha_startup() -> i32;
	fn chacha_update(S: *mut u8, input: *const u8, output: *mut u8, inlen: usize) -> usize;
	fn chacha_final(S: *mut u8, out: *mut u8) -> usize;
	fn poly1305_startup() -> i32;
	fn poly1305_init_ext(S: *mut u8, key: *const u8, bytes_hint: usize);
	fn poly1305_update(S: *mut u8, input: *const u8, inlen: usize);
	fn poly1305_finish(S: *mut u8, mac: *mut u8);
}

#[test]
fn simple_encrypt_decrypt()
{
	let ad = "foobar";
	let msg = "Hello, World!";
	let iv = "0123456789AB";
	let key = "ABCDEFGHIJKLMNOPQRSTUVWXYZ012345";
	let mut buf = [0;29];
	(&mut buf[..13]).copy_from_slice(msg.as_bytes());
	let mut ctx = Chacha20Poly1305::new(key.as_bytes()).unwrap();
	assert_eq!(ctx.encrypt(iv.as_bytes(), ad.as_bytes(), &mut buf, 16), Ok(29));
	assert_eq!(ctx.decrypt(iv.as_bytes(), ad.as_bytes(), &mut buf), Ok(13));
}
