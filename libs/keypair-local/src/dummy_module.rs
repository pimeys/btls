//!Dummy implementation of module key.
use std::fmt::{Display, Error as FmtError, Formatter};

use btls_aux_futures::FutureReceiver;

///Error from module.
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum ModuleError
{
	///Modules are not supported.
	NotSupported,
	#[doc(hidden)]
	Hidden__
}

impl Display for ModuleError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::ModuleError::*;
		match self {
			&NotSupported => fmt.write_str("Module keys are not supported"),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

///Dummy implementation of module key.
pub struct ModuleKey(());

impl ModuleKey
{
	///This always fails.
	pub fn new(_data: &[u8]) -> Result<ModuleKey, ModuleError>
	{
		Err(ModuleError::NotSupported)
	}
	//No need for more elaborate definitions. ModuleKey is assumed unconstructable.
	pub fn sign(&self, _data: &[u8], _algorithm: u16) -> FutureReceiver<Result<Vec<u8>, ()>>
	{
		unreachable!();
	}
	pub fn get_schemes(&self) -> Vec<u16>
	{
		unreachable!();
	}
	pub fn get_public_key(&self) -> Vec<u8>
	{
		unreachable!();
	}
	pub fn get_key_type(&self) -> &'static str
	{
		unreachable!();
	}
}
