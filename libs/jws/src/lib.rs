//!JOSE JWS signing.
//!
//!This module contains functions to use keypairs presented as `KeyPair` object to sign JWS structures.

#![forbid(unsafe_code)]
#![forbid(missing_docs)]

use std::borrow::ToOwned;
use std::collections::BTreeMap;
use std::mem::swap;
use std::ops::Deref;

extern crate btls_aux_futures;
extern crate btls_aux_hash;
extern crate btls_aux_keyconvert;
extern crate btls_aux_keypair;
extern crate btls_aux_keypair_local;
extern crate btls_aux_serialization;
extern crate btls_aux_signature_algo;
use btls_aux_futures::{create_future, FutureCallback, FutureReceiver, FutureSender};
use btls_aux_keyconvert::EcdsaCurve;
use btls_aux_keypair::KeyPair;
use btls_aux_serialization::{ASN1_INTEGER, ASN1_SEQUENCE, Source};
use btls_aux_signature_algo::{extract_key_algorithm, extract_raw_key, KeyAlgorithm, SIG_ECDSA_SHA256,
	SIG_ECDSA_SHA384, SIG_ECDSA_SHA512, SIG_ED25519, SIG_ED448, SIG_RSA_PKCS1_SHA256, SIG_RSA_PKCS1_SHA384,
	SIG_RSA_PKCS1_SHA512, SIG_RSA_PSS_SHA256, SIG_RSA_PSS_SHA384, SIG_RSA_PSS_SHA512};

///A node in JSON tree.
#[derive(Clone,Debug)]
pub enum JsonNode
{
	///A JSON Null value.
	Null,
	///A JSON number value (only integers are supported).
	Number(i64),
	///A JSON string value.
	String(String),
	///A JSON boolean value.
	Boolean(bool),
	///A JSON array value.
	Array(Vec<JsonNode>),
	///A JSON dictionar/map/object value.
	Dictionary(BTreeMap<String, JsonNode>),
}

impl JsonNode
{
	fn _serialize_str(x: &str, output: &mut String)
	{
		output.push('\"');
		for i in x.chars() {
			//Only 0x00-0x1F, 0x22 and 0x5C have to be escaped.
			if (i as u32) < 0x20 {
				output.push_str(&format!("\\u00{:02x}", i as u32));
			} else if i == '\"' {
				output.push_str("\\\"");
			} else if i == '\\' {
				output.push_str("\\\\");
			} else {
				output.push(i);
			}
		}
		output.push('\"');
	}
	fn _serialize(&self, output: &mut String)
	{
		match self {
			&JsonNode::Null => output.push_str("null"),
			&JsonNode::Number(ref x) => output.push_str(&format!("{}", x)),
			&JsonNode::String(ref x) => Self::_serialize_str(x, output),
			&JsonNode::Boolean(true) => output.push_str("true"),
			&JsonNode::Boolean(false) => output.push_str("false"),
			&JsonNode::Array(ref x) => {
				output.push('[');
				let mut first = true;
				for i in x.iter() {
					if !first { output.push_str(","); }
					first = false;
					i._serialize(output);
				}
				output.push(']');
			},
			&JsonNode::Dictionary(ref x) => {
				output.push('{');
				let mut first = true;
				for i in x.iter() {
					if !first { output.push_str(","); }
					first = false;
					Self::_serialize_str(i.0, output);
					output.push(':');
					(i.1)._serialize(output);
				}
				output.push('}');
			},
		}
	}
	///Serialize the node and its subnodes into a text string.
	///
	///# Parameters:
	///
	/// * `self`: The node to serialize.
	///
	///# Return value:
	///
	/// * The serialization.
	pub fn serialize(&self) -> String
	{
		let mut s = String::new();
		self._serialize(&mut s);
		s
	}
	///Serialize the node and its subnodes into a octet string.
	///
	///# Parameters:
	///
	/// * `self`: The node to serialize.
	///
	///# Return value:
	///
	/// * The serialization.
	pub fn serialize_bytes(&self) -> Vec<u8>
	{
		self.serialize().into_bytes()
	}
}

fn base64url(input: &[u8]) -> String
{
	const BASE64URL: [char; 64] = [
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
		'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
		'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
		'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '_'
	];
	let mut phase = 0;
	let mut value = 0u32;
	let mut out = String::new();
	for i in input.iter() {
		phase = phase + 1;
		value = value * 256 + (*i as u32);
		if phase == 3 {
			phase = 0;
			out.push(BASE64URL[((value >> 18) & 0x3F) as usize]);
			out.push(BASE64URL[((value >> 12) & 0x3F) as usize]);
			out.push(BASE64URL[((value >> 6) & 0x3F) as usize]);
			out.push(BASE64URL[(value & 0x3F) as usize]);
			value = 0;
		}
	}
	if phase == 1 {
		value *= 65536;
		out.push(BASE64URL[((value >> 18) & 0x3F) as usize]);
		out.push(BASE64URL[((value >> 12) & 0x3F) as usize]);
	} else if phase == 2 {
		value *= 256;
		out.push(BASE64URL[((value >> 18) & 0x3F) as usize]);
		out.push(BASE64URL[((value >> 12) & 0x3F) as usize]);
		out.push(BASE64URL[((value >> 6) & 0x3F) as usize]);
	};
	out
}

fn adapt_asn1_integer(input: &[u8]) -> Vec<u8>
{
	//Strip off leading zero, if any.
	let input = if input.len() > 0 && input[0] == 0 {
		&input[1..]
	} else {
		input
	};
	let mut tmp = Vec::new();
	if input.len() <= 32 {
		for _ in input.len()..32 { tmp.push(0); }
	} else if input.len() <= 48 {
		for _ in input.len()..48 { tmp.push(0); }
	} else if input.len() <= 66 {
		for _ in input.len()..66 { tmp.push(0); }
	};
	tmp.extend_from_slice(&input);
	tmp
}

///The signed JWS.
#[derive(Clone,Debug)]
pub struct SignedJws
{
	///The protected JWS header, as base64url string.
	header: String,
	///The signed payload, as base64url string.
	payload: String,
	///The signature, as base64url string.
	signature: String,
}

impl SignedJws
{
	///Emit the signed JWS as compact serialization.
	pub fn to_compact(&self) -> String
	{
		let mut x = String::new();
		x.push_str(&self.header);
		x.push('.');
		x.push_str(&self.payload);
		x.push('.');
		x.push_str(&self.signature);
		x
	}
	///Emit signed JWS as JSON serialization.
	pub fn to_json(&self) -> String
	{
		let mut root = BTreeMap::new();
		root.insert("protected".to_owned(), JsonNode::String(self.header.clone()));
		root.insert("payload".to_owned(), JsonNode::String(self.payload.clone()));
		root.insert("signature".to_owned(), JsonNode::String(self.signature.clone()));
		JsonNode::Dictionary(root).serialize()
	}
}

struct JwsSignAdapter
{
	algo: u16,
	header: String,
	payload: String,
	sender: Option<FutureSender<Result<SignedJws, ()>>>
}

impl JwsSignAdapter
{
	fn adapt(&self, val: Vec<u8>) -> Result<SignedJws, ()>
	{
		let signature = if self.algo == 0x403 || self.algo == 0x503 || self.algo == 0x603 {
			//This is ECDSA. Handle specially.
			let mut src = Source::new(&val);
			let mut srctop = src.read_asn1_value(ASN1_SEQUENCE, |_|())?.value;
			let r = adapt_asn1_integer(srctop.read_asn1_value(ASN1_INTEGER, |_|())?.raw_p);
			let s = adapt_asn1_integer(srctop.read_asn1_value(ASN1_INTEGER, |_|())?.raw_p);
			let mut _x = Vec::new();
			_x.extend_from_slice(&r);
			_x.extend_from_slice(&s);
			base64url(&_x)
		} else {
			base64url(&val)
		};
		Ok(SignedJws{header: self.header.clone(), payload: self.payload.clone(), signature: signature})
	}
}

impl FutureCallback<Result<Vec<u8>, ()>> for JwsSignAdapter
{
	fn on_settled(&mut self, val: Result<Vec<u8>, ()>) -> Result<Vec<u8>, ()>
	{
		let mut tmp = None;
		swap(&mut self.sender, &mut tmp);
		tmp.map(|resender|resender.settle(val.and_then(|x|self.adapt(x))));
		Err(())	//Nobody ever reads this.
	}
}

///Sign a JWS and return to future resolving to the signature.
///
///If the `prot_header` parameter does not already contain a `alg` parameter, that is automatically inserted.
///
///# Parameters:
///
/// * `prot_header`: The JWS protected header. Must be a dictionary.
/// * `payload`: The payload to sign.
/// * `keypair`: The keypair to sign with.
/// * `algorithm`: The algorithm to use. One of the SIG_* constants (TLS signature scheme numbers).
///
///# Return value:
///
/// * Future resolving to the signature when available.
pub fn sign_jws<K:KeyPair>(prot_header: &JsonNode, payload: &[u8], keypair: &K, algorithm: u16) ->
	FutureReceiver<Result<SignedJws, ()>>
{
	let prot_header = if let &JsonNode::Dictionary(ref x) = prot_header {
		let mut arr = BTreeMap::new();
		let mut has_alg = false;
		for i in x.iter() {
			if i.0 == "alg" { has_alg = true; }
			arr.insert((i.0).clone(), (i.1).clone());
		}
		if !has_alg {
			let alg = match algorithm {
				SIG_RSA_PKCS1_SHA256 => "RS256",
				SIG_RSA_PKCS1_SHA384 => "RS384",
				SIG_RSA_PKCS1_SHA512 => "RS512",
				SIG_RSA_PSS_SHA256 => "PS256",
				SIG_RSA_PSS_SHA384 => "PS384",
				SIG_RSA_PSS_SHA512 => "PS512",
				SIG_ECDSA_SHA256 => "ES256",
				SIG_ECDSA_SHA384 => "ES384",
				SIG_ECDSA_SHA512 => "ES512",
				SIG_ED25519 => "EdDSA",		//Determined by key.
				SIG_ED448 => "EdDSA",		//Determined by key.
				_ => return FutureReceiver::from(Err(()))
			};
			arr.insert("alg".to_owned(), JsonNode::String(alg.to_owned()));
		}
		JsonNode::Dictionary(arr)
	} else {
		return FutureReceiver::from(Err(()));
	};
	let (sender, receiver) = create_future::<Result<SignedJws, ()>>();
	let header = base64url(prot_header.serialize().as_bytes());
	let payload = base64url(payload);
	let mut tbs = header.clone();
	tbs.push('.');	//.
	tbs.push_str(&payload);
	let ctx = JwsSignAdapter{algo:algorithm, header: header.clone(), payload: payload.clone(),
		sender: Some(sender)};
	let mut will_be_signature = keypair.sign(tbs.as_bytes(), algorithm);
	if let Some(mut ctx) = will_be_signature.settled_cb(Box::new(ctx)) {
		//Ok, the signing is already complete, Forward the value.
		let _ = match will_be_signature.read() {
			Ok(x) => ctx.on_settled(x),
			_ => return FutureReceiver::from(Err(())),
		};
	}
	//Will be chained here when ready.
	receiver
}

///Extract JWK from `KeyPair`
///
///# Parameters:
///
/// * `keypair`: The keypair to get JWK from.
///
///# Return value:
///
/// * On success, `Ok(jwk)`.
/// * On failure, `Err(())`.
///
///# Notes:
///
/// * The extracted JWK is always in canonical form one can compute JWK thumbprint from.
pub fn extract_jwk<K: KeyPair>(keypair: &K) -> Result<JsonNode, ()>
{
	let mut jwk_map = BTreeMap::new();
	let pubkey = keypair.get_public_key().0;
	//The pubkey is in X.509 SPKI form.
	let algo = extract_key_algorithm(pubkey.deref())?;
	let keydata = extract_raw_key(pubkey.deref())?;
	match algo {
		KeyAlgorithm::Rsa|KeyAlgorithm::RsaPss => {
			let mut keydata = Source::new(keydata);
			let mut keydata = keydata.read_asn1_value(ASN1_SEQUENCE, |_|())?.value;
			let rsa_n = keydata.read_asn1_value(ASN1_INTEGER, |_|())?.raw_p;
			let rsa_e = keydata.read_asn1_value(ASN1_INTEGER, |_|())?.raw_p;
			//Strip initial zero bytes, in case ASN.1 notation has those.
			let rsa_n = if rsa_n.len() > 0 && rsa_n[0] == 0 { &rsa_n[1..] } else { rsa_n };
			let rsa_e = if rsa_e.len() > 0 && rsa_e[0] == 0 { &rsa_e[1..] } else { rsa_e };
			jwk_map.insert("kty".to_owned(), JsonNode::String("RSA".to_owned()));
			jwk_map.insert("n".to_owned(), JsonNode::String(base64url(rsa_n)));
			jwk_map.insert("e".to_owned(), JsonNode::String(base64url(rsa_e)));
		},
		KeyAlgorithm::EcdsaP256 => make_jwk_ec(&mut jwk_map, EcdsaCurve::NsaP256, keydata)?,
		KeyAlgorithm::EcdsaP384 => make_jwk_ec(&mut jwk_map, EcdsaCurve::NsaP384, keydata)?,
		KeyAlgorithm::EcdsaP521 => make_jwk_ec(&mut jwk_map, EcdsaCurve::NsaP521, keydata)?,
		KeyAlgorithm::Ed25519 => make_jwk_okp(&mut jwk_map, "Ed25519", keydata),
		KeyAlgorithm::Ed448 => make_jwk_okp(&mut jwk_map, "Ed448", keydata),
	}
	Ok(JsonNode::Dictionary(jwk_map))
}

fn make_jwk_ec(jwk_map: &mut BTreeMap<String, JsonNode>, crv: EcdsaCurve, keydata: &[u8]) -> Result<(), ()>
{
	let _crv = crv.get_jwk_curve_name();
	let xlen = crv.get_component_bytes();
	jwk_map.insert("kty".to_owned(), JsonNode::String("EC".to_owned()));
	jwk_map.insert("crv".to_owned(), JsonNode::String(_crv.to_owned()));
	if keydata.len() != 2 * xlen + 1 { return Err(()); }
	let (_, x) = keydata.split_at(1);
	let (x, y) = x.split_at(xlen);
	jwk_map.insert("x".to_owned(), JsonNode::String(base64url(x)));
	jwk_map.insert("y".to_owned(), JsonNode::String(base64url(y)));
	Ok(())
}

fn make_jwk_okp(jwk_map: &mut BTreeMap<String, JsonNode>, ktype: &str, keydata: &[u8])
{
	jwk_map.insert("kty".to_owned(), JsonNode::String("OKP".to_owned()));
	jwk_map.insert("crv".to_owned(), JsonNode::String(ktype.to_owned()));
	jwk_map.insert("x".to_owned(), JsonNode::String(base64url(keydata)));
}

#[test]
fn rfc8037_key()
{
	//This is the example from RFC8037.
	use self::btls_aux_hash::checksum;
	use btls_aux_keypair_local::LocalKeyPair;
	let key = include_bytes!("ed25519-testkey.txt");
	let mut key = &key[..];
	let key = LocalKeyPair::new(&mut key, "test key").unwrap();
	let pubkey = extract_jwk(&key).unwrap();
	let thumb = base64url(&checksum(&pubkey.serialize_bytes()));
	assert_eq!(&thumb, "kPrK_qmxVWaYVA9wwBF6Iuo3vVzz7TxHCTwXBygrS4k");
}

#[test]
fn rfc8037_sig()
{
	use btls_aux_keypair_local::LocalKeyPair;
	let key = include_bytes!("ed25519-testkey.txt");
	let mut key = &key[..];
	let key = LocalKeyPair::new(&mut key, "test key").unwrap();
	let sig: SignedJws = match sign_jws(&JsonNode::Dictionary(BTreeMap::new()),
		"Example of Ed25519 signing".as_bytes(), &key, SIG_ED25519).read() {
		Ok(Ok(x)) => x,
		_ => { assert!(false); unreachable!(); }
	};
	assert_eq!(sig.to_compact(), "eyJhbGciOiJFZERTQSJ9.RXhhbXBsZSBvZiBFZDI1NTE5IHNpZ25pbmc.hgyY0il_MGCj\
		P0JzlnLWG1PPOt7-09PGcvMg3AIbQR6dWbhijcNR4ki4iylGjg5BhVsPt9g7sVvpAr_MuM0KAg");
}
