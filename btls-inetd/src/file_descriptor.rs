use std::os::unix::io::RawFd;
use std::sync::Arc;
use std::sync::atomic::{AtomicUsize};
use std::marker::PhantomData;
use std::net::{SocketAddr, SocketAddrV4, SocketAddrV6, Ipv4Addr, Ipv6Addr};
use std::io::{Error as IoError, ErrorKind as IoErrorKind, Read as IoRead, Result as IoResult, Write as IoWrite};
use std::mem::{transmute, size_of_val, size_of, zeroed};
use mio::{Evented, Poll, PollOpt, Ready, Token};
use mio::unix::EventedFd;
use libc::{close, c_void, fcntl, read, write, socket, shutdown, SHUT_WR, F_GETFL, O_NONBLOCK, F_SETFL, socklen_t,
	getsockopt, SOL_SOCKET, SO_ERROR, sockaddr, sockaddr_in, sockaddr_in6, connect, EINPROGRESS, AF_INET,
	AF_INET6, getsockname, accept, bind, listen, c_int, in_addr, in6_addr, pipe as libc_pipe, dup2,
	FD_CLOEXEC, F_GETFD, F_SETFD};
use ::{increment_count, decrement_count};

pub struct MutableAddress<'a>
{
	addr: *mut sockaddr,
	addrlen: *mut socklen_t,
	dummy: PhantomData<&'a mut sockaddr>,	//This logically owns a mutable sockaddr.
	dummy1: PhantomData<&'a mut socklen_t>	//This logically owns a mutable socket length.
}

pub struct SockaddrStorage
{
	addr: [u8;128],			//Assume addresses fit into 128 bytes.
	addrlen: socklen_t,
}

impl SockaddrStorage
{
	fn new() -> SockaddrStorage
	{
		SockaddrStorage{addr: [0;128], addrlen: 128}
	}
	fn to_storage<'a>(&'a mut self) -> MutableAddress<'a>
	{
		self.addrlen = size_of_val(&self.addr) as socklen_t;
		MutableAddress {
			addr: unsafe { transmute::<_, *mut sockaddr>(&self.addr) },
			addrlen: unsafe { transmute::<_, *mut socklen_t>(&self.addrlen) },
			dummy: PhantomData,
			dummy1: PhantomData,
		}
	}
	fn to_socket_addr(&self) -> Option<SocketAddr>
	{
		RawAddress::get_source(self).map(|x|x.to_socket_addr())
	}
}

pub struct Address<'a>
{
	addr: *const sockaddr,
	addrlen: socklen_t,
	dummy: PhantomData<&'a sockaddr>	//This logically owns a sockaddr.
}

pub trait ConnectAddress: Sized
{
	fn get_destination<'a>(&'a self) -> Address<'a>;
	fn get_source(addr: &SockaddrStorage) -> Option<Self>;
}

enum RawAddress
{
	V4(sockaddr_in),
	V6(sockaddr_in6),
}

impl RawAddress
{
	fn from_socket_addr(x: &SocketAddr) -> RawAddress
	{
		match x {
			&SocketAddr::V4(ref x) => {
				let mut tmp: sockaddr_in = unsafe{zeroed()};
				let octets = x.ip().octets();
				tmp.sin_family = AF_INET as u16;
				tmp.sin_port = x.port().to_be();
				tmp.sin_addr = unsafe{transmute::<_,in_addr>(octets)};
				RawAddress::V4(tmp)
			},
			&SocketAddr::V6(ref x) => {
				let mut tmp: sockaddr_in6 = unsafe{zeroed()};
				let segments = x.ip().segments();
				let mut addr = [0u8; 16];
				for i in 0..16 { addr[i] = (segments[i>>1] >> (8 - 8 * (i % 2))) as u8; }
				tmp.sin6_family = AF_INET6 as u16;
				tmp.sin6_port = x.port().to_be();
				tmp.sin6_addr = unsafe{transmute::<_,in6_addr>(addr)};
				tmp.sin6_flowinfo = x.flowinfo().to_be();
				tmp.sin6_scope_id = x.scope_id().to_be();
				RawAddress::V6(tmp)
			},
		}
	}
	fn to_socket_addr(&self) -> SocketAddr
	{
		match self {
			&RawAddress::V4(ref x) => SocketAddr::V4(SocketAddrV4::new(
				{
					let addrp = u32::from_be(x.sin_addr.s_addr);
					Ipv4Addr::new((addrp >> 24) as u8, (addrp >> 16) as u8, (addrp >> 8) as u8,
						addrp as u8)
				},
				u16::from_be(x.sin_port)
			)),
			&RawAddress::V6(ref x) => SocketAddr::V6(SocketAddrV6::new(
				{
					let _addr = &x.sin6_addr.s6_addr;
					let mut addr = [0u16;8];
					for i in 0..16 { addr[i>>1] |= (_addr[i] as u16) << (8 - 8 * (i & 1)); }
					Ipv6Addr::new(addr[0], addr[1], addr[2], addr[3], addr[4], addr[5],
						addr[6], addr[7])
				},
				u16::from_be(x.sin6_port),
				u32::from_be(x.sin6_flowinfo),
				u32::from_be(x.sin6_scope_id),
			))
		}
	}
}

impl ConnectAddress for RawAddress
{
	fn get_destination<'a>(&'a self) -> Address<'a>
	{
		match self {
			&RawAddress::V4(ref x) => x.get_destination(),
			&RawAddress::V6(ref x) => x.get_destination(),
		}
	}
	fn get_source(addr: &SockaddrStorage) -> Option<Self>
	{
		if let Some(x) = sockaddr_in::get_source(addr) { Some(RawAddress::V4(x)) }
		else if let Some(x) = sockaddr_in6::get_source(addr) { Some(RawAddress::V6(x)) }
		else { None }
	}
}

impl ConnectAddress for sockaddr_in
{
	fn get_destination<'a>(&'a self) -> Address<'a>
	{
		Address {
			addr: unsafe { transmute::<_, *const sockaddr>(self) },
			addrlen: size_of_val(self) as socklen_t,
			dummy: PhantomData
		}
	}
	fn get_source(addr: &SockaddrStorage) -> Option<Self>
	{
		//If size is wrong, the type is wrong too.
		if addr.addrlen != size_of::<Self>() as socklen_t { return None; }
		unsafe {
			let tmp = transmute::<_, &Self>(&addr.addr);
			if tmp.sin_family != AF_INET as u16 { return None };
			let tmp2 = *tmp;
			Some(tmp2)
		}
	}
}

impl ConnectAddress for sockaddr_in6
{
	fn get_destination<'a>(&'a self) -> Address<'a>
	{
		Address {
			addr: unsafe { transmute::<_, *const sockaddr>(self) },
			addrlen: size_of_val(self) as socklen_t,
			dummy: PhantomData
		}
	}
	fn get_source(addr: &SockaddrStorage) -> Option<Self>
	{
		//If size is wrong, the type is wrong too.
		if addr.addrlen != size_of::<Self>() as socklen_t { return None; }
		unsafe {
			let tmp = transmute::<_, &Self>(&addr.addr);
			if tmp.sin6_family != AF_INET6 as u16 { return None };
			let tmp2 = *tmp;
			Some(tmp2)
		}
	}
}

#[derive(Debug)]
struct FileDescriptorInner(RawFd, Arc<AtomicUsize>);

impl Drop for FileDescriptorInner
{
	fn drop(&mut self)
	{
		unsafe { close(self.0); }
		decrement_count(&self.1);
	}
}

impl PartialEq for FileDescriptorInner
{
	fn eq(&self, other: &FileDescriptorInner) -> bool {
		self.0 == other.0
	}
}

impl Eq for FileDescriptorInner {}

#[derive(Clone,Debug)]
pub struct FileDescriptor(Option<Arc<FileDescriptorInner>>);

impl FileDescriptor
{
	pub fn blank() -> FileDescriptor
	{
		FileDescriptor(None)
	}
	pub fn new(fd: RawFd, count: Arc<AtomicUsize>) -> FileDescriptor
	{
		if fd < 0 { return FileDescriptor::blank(); }
		increment_count(&count);
		FileDescriptor(Some(Arc::new(FileDescriptorInner(fd, count))))
	}
	//First is read end, second is write.
	pub fn pipe(count: Arc<AtomicUsize>) -> IoResult<(FileDescriptor, FileDescriptor)>
	{
		let mut buf: [RawFd;2] = [0, 0];
		match unsafe{libc_pipe(buf.as_mut_ptr())} {
			x if x >= 0 => (),
			_ => return Err(IoError::last_os_error())
		};
		Ok((FileDescriptor::new(buf[0], count.clone()), FileDescriptor::new(buf[1], count.clone())))
	}
	pub fn dup2(&self, target: RawFd) -> IoResult<FileDescriptor>
	{
		//The target is most probably open, so don't increment fd counter.
		let (fd, counter) = match &self.0 {
			&Some(ref x) => (x.0, x.1.clone()),
			&None => return Err(IoError::new(IoErrorKind::Other, "Attempted to dup2 on dummy fd"))
		};
		match unsafe { dup2(fd, target) } {
			x if x >= 0 => Ok(FileDescriptor(Some(Arc::new(FileDescriptorInner(target, counter))))),
			_ => return Err(IoError::last_os_error())
		}
	}
	pub fn socket(domain: i32, stype: i32, protocol: i32, count: Arc<AtomicUsize>) ->
		IoResult<FileDescriptor>
	{
		match unsafe{socket(domain, stype, protocol)} {
			x if x >= 0 => Ok(FileDescriptor::new(x, count)),
			_ => Err(IoError::last_os_error())
		}
	}
	pub fn shutdown_write(&self)
	{
		let fd = self.as_raw_fd();
		if fd < 0 { return; }
		unsafe { shutdown(fd, SHUT_WR); }
	}
	pub fn set_nonblock(&self) -> IoResult<()>
	{
		let fd = self.as_raw_fd();
		if fd < 0 {
			return Err(IoError::new(IoErrorKind::Other, "Attempted to set noblocking mode on dummy fd"));
		}
		let mut flags = match unsafe{fcntl(fd, F_GETFL)} {
			x if x >= 0 => x,
			_ => return Err(IoError::last_os_error())
		};
		flags |= O_NONBLOCK;
		match unsafe{fcntl(fd, F_SETFL, flags)} {
			x if x >= 0 => (),
			_ => return Err(IoError::last_os_error())
		};
		Ok(())
	}
	pub fn set_close_exec(&self, state: bool) -> IoResult<()>
	{
		let fd = self.as_raw_fd();
		if fd < 0 {
			return Err(IoError::new(IoErrorKind::Other, "Attempted to set close-on-exec on dummy fd"));
		}
		let mut flags = match unsafe{fcntl(fd, F_GETFD)} {
			x if x >= 0 => x,
			_ => return Err(IoError::last_os_error())
		};
		if state { flags |= FD_CLOEXEC } else { flags &= !FD_CLOEXEC };
		match unsafe{fcntl(fd, F_SETFD, flags)} {
			x if x >= 0 => (),
			_ => return Err(IoError::last_os_error())
		};
		Ok(())
	}
	pub fn so_error(&self) -> IoResult<IoResult<()>>
	{
		let mut code = 0i32;
		let mut len = 4 as socklen_t;
		let fd = self.as_raw_fd();
		if fd < 0 { return Err(IoError::new(IoErrorKind::Other, "Attempted to get SO_ERROR on dummy fd")); }
		match unsafe{getsockopt(fd, SOL_SOCKET, SO_ERROR, transmute::<_,*mut c_void>(&mut code),
			&mut len as *mut _)} {
			x if x >= 0 => (),
			_ => return Err(IoError::last_os_error())
		}
		Ok(if code != 0 { Err(IoError::from_raw_os_error(code)) } else { Ok(()) })
	}
	pub fn connect(&self, addr: &SocketAddr) -> IoResult<bool>
	{
		let addr = RawAddress::from_socket_addr(addr);
		let fd = self.as_raw_fd();
		if fd < 0 { return Err(IoError::new(IoErrorKind::Other, "Attempted to connect on dummy fd")); }
		let dest = addr.get_destination();
		match unsafe{connect(fd, dest.addr, dest.addrlen)} {
			x if x >= 0 => (),
			_ => {
				//The connect errored.
				let serr = IoError::last_os_error();
				if serr.raw_os_error() == Some(EINPROGRESS) {
					return Ok(false);
				} else {
					return Err(serr)
				}
			}
		}
		Ok(true)
	}
	pub fn bind(&self, addr: &SocketAddr) -> IoResult<()>
	{
		let addr = RawAddress::from_socket_addr(addr);
		let fd = self.as_raw_fd();
		if fd < 0 { return Err(IoError::new(IoErrorKind::Other, "Attempted to bind on dummy fd")); }
		let dest = addr.get_destination();
		match unsafe{bind(fd, dest.addr, dest.addrlen)} {
			x if x >= 0 => (),
			_ => return Err(IoError::last_os_error())
		}
		Ok(())
	}
	pub fn listen(&self, backlog: c_int) -> IoResult<()>
	{
		let fd = self.as_raw_fd();
		if fd < 0 { return Err(IoError::new(IoErrorKind::Other, "Attempted to listen on dummy fd")); }
		match unsafe{listen(fd, backlog)} {
			x if x >= 0 => (),
			_ => return Err(IoError::last_os_error())
		}
		Ok(())
	}
	pub fn local_addr(&self) -> IoResult<Option<SocketAddr>>
	{
		let mut addr = SockaddrStorage::new();
		let fd = self.as_raw_fd();
		if fd < 0 {
			return Err(IoError::new(IoErrorKind::Other, "Attempted to get local address on dummy fd"));
		}
		{
			let tmp = addr.to_storage();
			if unsafe{getsockname(fd, tmp.addr, tmp.addrlen)} < 0 {
				return Err(IoError::last_os_error());
			}
		}
		Ok(addr.to_socket_addr())
	}
	pub fn accept(&self) -> IoResult<(FileDescriptor, Option<SocketAddr>)>
	{
		let mut addr = SockaddrStorage::new();
		let (fd, counter) = match &self.0 {
			&Some(ref x) => (x.0, x.1.clone()),
			&None => return Err(IoError::new(IoErrorKind::Other, "Attempted to accept on dummy fd"))
		};
		let rnewfd = {
			let tmp = addr.to_storage();
			match unsafe{accept(fd, tmp.addr, tmp.addrlen)} {
				x if x >= 0 => x,
				_ => return Err(IoError::last_os_error())
			}
		};
		Ok((FileDescriptor::new(rnewfd, counter), addr.to_socket_addr()))
	}
	pub fn as_raw_fd(&self) -> RawFd
	{
		match &self.0 {
			&Some(ref x) => x.0,
			&None => -1
		}
	}
}

impl PartialEq for FileDescriptor
{
	fn eq(&self, other: &FileDescriptor) -> bool {
		match (&self.0, &other.0) {
			(&Some(ref x), &Some(ref y)) => x.0 == y.0,
			(&None, &None) => true,
			_ => false,
		}
	}
}

impl Eq for FileDescriptor {}

impl IoRead for FileDescriptor
{
	fn read(&mut self, buf: &mut [u8]) -> IoResult<usize>
	{
		let fd = self.as_raw_fd();
		if fd < 0 { return Err(IoError::new(IoErrorKind::Other, "Attempted to read on dummy fd")); }
		let ret = unsafe{read(fd, buf.as_mut_ptr() as *mut c_void, buf.len())};
		if ret < 0 {
			return Err(IoError::last_os_error());
		}
		return Ok(ret as usize);
	}
}

impl IoWrite for FileDescriptor
{
	fn write(&mut self, buf: &[u8]) -> IoResult<usize>
	{
		let fd = self.as_raw_fd();
		if fd < 0 { return Err(IoError::new(IoErrorKind::Other, "Attempted to write on dummy fd")); }
		let ret = unsafe{write(fd, buf.as_ptr() as *const c_void, buf.len())};
		if ret < 0 {
			return Err(IoError::last_os_error());
		}
		return Ok(ret as usize);
	}
	fn flush(&mut self) -> IoResult<()>
	{
		Ok(())		//NOP.
	}
}

impl Evented for FileDescriptor
{
	fn register(&self, poll: &Poll, token: Token, interest: Ready, opts: PollOpt) -> IoResult<()>
	{
		let fd = self.as_raw_fd();
		if fd < 0 { return Err(IoError::new(IoErrorKind::Other, "Attempted to register dummy fd")); }
		let x = EventedFd(&fd);
		x.register(poll, token, interest, opts)
	}
	fn reregister(&self, poll: &Poll, token: Token, interest: Ready, opts: PollOpt) -> IoResult<()>
	{
		let fd = self.as_raw_fd();
		if fd < 0 { return Err(IoError::new(IoErrorKind::Other, "Attempted to reregister dummy fd")); }
		let x = EventedFd(&fd);
		x.reregister(poll, token, interest, opts)
	}
	fn deregister(&self, poll: &Poll) -> IoResult<()>
	{
		let fd = self.as_raw_fd();
		if fd < 0 { return Err(IoError::new(IoErrorKind::Other, "Attempted to deregister dummy fd")); }
		let x = EventedFd(&fd);
		x.deregister(poll)
	}
}
