use std::collections::BTreeMap;
use std::ops::Deref;
use std::sync::{Arc, Mutex};

struct _AllocatedToken
{
	parent: TokenAllocatorPool,
	token: usize,
}

impl Drop for _AllocatedToken
{
	fn drop(&mut self)
	{
		self.parent.release(self.token);
	}
}

///Allocated token.
#[derive(Clone)]
pub struct AllocatedToken(Arc<_AllocatedToken>);

impl AllocatedToken
{
	pub fn value(&self) -> usize
	{
		self.0.deref().token
	}
}

struct _TokenAllocatorPool
{
	first: usize,
	last: usize,
	first_free: usize,
	free_ones: BTreeMap<usize, ()>,
}

impl _TokenAllocatorPool
{
	fn new(begin: usize, count: usize) -> _TokenAllocatorPool
	{
		_TokenAllocatorPool {
			first: begin,
			last: begin.saturating_add(count),
			first_free: begin,
			free_ones: BTreeMap::new(),
		}
	}
	fn allocate(&mut self) -> Result<usize, ()>
	{
		//First try allocating a free one.
		let first_free_one = self.free_ones.iter().next().map(|x|*x.0);
		if let Some(x) = first_free_one {
			self.free_ones.remove(&x);
			return Ok(x);
		}
		//No free ones. Try bumping first_free.
		if self.first_free < self.last {
			let nt = self.first_free;
			self.first_free += 1;
			return Ok(nt);
		}
		//No tokens remain.
		Err(())
	}
	fn release(&mut self, token: usize)
	{
		if token < self.first || token >= self.last { return; }
		//If we released token just below first_free, decrement first_free.
		if self.first_free + 1 == token {
			self.first_free -= 1;
			//Scan for free ones to decremen first_free with.
			loop {
				if self.first_free == self.first { break; }
				let candidate = self.first_free - 1;
				if self.free_ones.contains_key(&candidate) {
					self.free_ones.remove(&candidate);
					self.first_free = candidate;
				} else {
					break;	//No more to merge.
				}
			}
			//Ok.
		} else {
			//Push straight to free_ones.
			self.free_ones.insert(token, ());
		}
	}
}

///Token allocator pool.
///
///Allocates tokens out of specified range.
#[derive(Clone)]
pub struct TokenAllocatorPool(Arc<Mutex<_TokenAllocatorPool>>);

impl TokenAllocatorPool
{
	///Creates a new allocator pool controlling the specified range.
	///
	///# Parameters:
	///
	/// * `begin`: The first token number in range.
	/// * `count`: Number of tokens on range.
	///
	///# Returns:
	///
	/// * The new pool controlling the range `[begin,begin+count)`.
	pub fn new(begin: usize, count: usize) -> TokenAllocatorPool
	{
		TokenAllocatorPool(Arc::new(Mutex::new(_TokenAllocatorPool::new(begin, count))))
	}
	///Allocate a token.
	///
	///# Parameters:
	///
	/// * `self`: The allocator to allocate from.
	///
	///# Returns:
	///
	/// * The allocated token.
	///
	///# Failures:
	///
	/// * If there are no more tokens available, fails with `()`.
	pub fn allocate(&mut self) -> Result<AllocatedToken, ()>
	{
		let tnum = match self.0.lock().map_err(|_|()).and_then(|mut x|x.allocate()) {
			Ok(x) => x,
			Err(_) => return Err(())
		};
		let _token = _AllocatedToken{
			parent: self.clone(),
			token: tnum
		};
		Ok(AllocatedToken(Arc::new(_token)))
	}
	fn release(&mut self, token: usize)
	{
		match self.0.lock() {
			Ok(mut x) => x.release(token),
			Err(_) => return
		}
	}
}
