use ::stdlib::{Display, Formatter, FmtError};

use btls_aux_serialization::{ASN1_GENERALIZED_TIME, ASN1_UTC_TIME, Asn1Tag, Source};
use btls_aux_time::Timestamp;


///Error in parsing timestamp.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum TimestampParseError
{
	///The date is invalid.
	///
	///This error occurs if a date is invalid (invalid day in month, invalid month or invalid year).
	///
	/// * The 1st parameter is the year.
	/// * The 2nd parameter is the month.
	/// * The 3rd parameter is the day.
	InvalidDate(i64, usize, i64),
	///The time is invalid.
	///
	///This error occurs if time is invalid (invalid hour number, invalid minute number or invalid second
	///number).
	///
	/// * The 1st parameter is the hour.
	/// * The 2nd parameter is the minute.
	/// * The 3rd parameter is the second.
	InvalidTime(u32, u32, u32),
	///Unrecognized time format.
	///
	///This error occurs if timestamp isn't either UTCTime nor GeneralizedTime.
	UnrecognizedFormat,
	///UTCTime notation is bad.
	///
	///This error occurs if UTCTime value can not be parsed into its components.
	BadUtcTime,
	///GeneralizedTime notation is bad.
	///
	///This error occurs if GeneralizedTime value can not be parsed into its components.
	BadGeneralizedTime,
	#[doc(hidden)]
	Hidden__
}

impl Display for TimestampParseError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::TimestampParseError::*;
		match self {
			&InvalidDate(year, month, day) => fmt.write_fmt(format_args!("Invalid date {}-{}-{}", year,
				month, day)),
			&InvalidTime(hour, minute, second) => fmt.write_fmt(format_args!("Invalid time {}:{}:{}",
				hour, minute, second)),
			&UnrecognizedFormat => fmt.write_str("Unrecognized time format"),
			&BadUtcTime => fmt.write_str("Bad UtcTime notation"),
			&BadGeneralizedTime => fmt.write_str("Bad GeneralizedTime notation"),
			&Hidden__ => fmt.write_str("Hidden__")
		}
	}
}

//floor(a/400).
fn floordiv400(a: i64) -> i64
{
	if a < 0 {
		//No way the bound can be reached.
		a.saturating_sub(399) / 400
	} else {
		a / 400
	}
}

//Get day num for date (not correct for year 1752 or earlier, but we won't care).
fn get_day_num(year: i64, month: usize, day: i64) -> Result<i64, TimestampParseError>
{
	use self::TimestampParseError::*;
	//Just use wrapping ops here, the values shouldn't even be near overflowing. Just in case, limit year to
	//10^11.
	//See "cal 9 1752" for this mess. Also, we don't handle years before 1CE.
	fail_if!((year == 1752 && month == 9 && day >= 3 && day <= 13) || year <= 0 || year >= 100000000000,
		InvalidDate(year, month, day));
	//Break down year to supercycle (400 years), cycle (4 years) and year in cycle. Arrange 1970 to be
	//supercycle 0, cycle 0, year in cycle 0.
	let supercycle = floordiv400(year.wrapping_sub(1970));
	let cycle = ((year.wrapping_add(30)) % 400) / 4;
	let cyear = (year.wrapping_add(2)) % 4;
	//Cycles last 1461 days (1 leap year, in year 2 in cycle), except cycles 32, 57 and 82, which are only
	//1460 days (no leap years).
	let mut delta = 0i64;
	if cycle > 32 { delta = delta.wrapping_sub(1); }
	if cycle > 57 { delta = delta.wrapping_sub(1); }
	if cycle > 82 { delta = delta.wrapping_sub(1); }
	if year < 1752 || (year == 1752 && month < 9) || (year == 1752 && month == 9 && day < 14) {
		delta = delta.wrapping_add(11);		//Gap of 11 days in dates!
	}
	if cycle != 32 && cycle != 57 && cycle != 82 {
		if (cyear == 2 && month >= 3) || cyear > 2 { delta = delta.wrapping_add(1); }
	}
	let slop = if cyear == 2 && month == 2 { 1 } else { 0 };

	const MAX_DAY: [i64; 12] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	const FIRST_MONTH_DAY: [i64; 13] = [0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365];
	let monthm1 = month.wrapping_sub(1);
	let max_day = MAX_DAY.get(monthm1).map(|x|*x).unwrap_or(0);
	let first_month_day = FIRST_MONTH_DAY.get(monthm1).map(|x|*x).unwrap_or(0);
	//max_day is at most 31, so the addition won't even nearly overflow.
	fail_if!(year < 1 || day < 1 || day > max_day.saturating_add(slop), InvalidDate(year, month, day));
	//This can't overflow, because it would take year greater than +-Y100G...
	Ok(supercycle.wrapping_mul(146097).wrapping_add(cycle.wrapping_mul(1461)).wrapping_add(cyear.wrapping_mul(
		365)).wrapping_add(delta).wrapping_add(first_month_day).wrapping_add(day).wrapping_sub(1))
}

//Get a digit. The index needs to be in-range!
fn get_digit(arr: &[u8], index: usize) -> Result<u32, ()>
{
	let x = arr[index];
	match x.checked_sub(0x30) {
		Some(y) if y < 10 => Ok(y as u32),
		_ => Err(())
	}
}

fn read_utc_timestmap(payload: &[u8]) -> Result<(u32, u32, u32, u32, u32, u32), ()>
{
	fail_if!(payload.len() != 13 || payload.get(12).map(|x|*x).ok_or(())? != 90, ());
	//These won't come even near overflowing.
	let ybase = if get_digit(payload, 0)? >= 5 { 1900u32 } else { 2000u32 };
	let year = get_digit(payload, 0)?*10+get_digit(payload, 1)?;
	let month = get_digit(payload, 2)?*10+get_digit(payload, 3)?;
	let day = get_digit(payload, 4)?*10+get_digit(payload, 5)?;
	let hour = get_digit(payload, 6)?*10+get_digit(payload, 7)?;
	let minute = get_digit(payload, 8)?*10+get_digit(payload, 9)?;
	let second = get_digit(payload, 10)?*10+get_digit(payload, 11)?;
	Ok((ybase.wrapping_add(year), month, day, hour, minute, second))
}

fn read_general_timestmap(payload: &[u8]) -> Result<(u32, u32, u32, u32, u32, u32), ()>
{
	fail_if!(payload.len() != 15 || payload.get(14).map(|x|*x).ok_or(())? != 90, ());
	//These won't come even near overflowing.
	let century = get_digit(payload, 0)?*10+get_digit(payload, 1)?;
	let year = get_digit(payload, 2)?*10+get_digit(payload, 3)?;
	let month = get_digit(payload, 4)?*10+get_digit(payload, 5)?;
	let day = get_digit(payload, 6)?*10+get_digit(payload, 7)?;
	let hour = get_digit(payload, 8)?*10+get_digit(payload, 9)?;
	let minute = get_digit(payload, 10)?*10+get_digit(payload, 11)?;
	let second = get_digit(payload, 12)?*10+get_digit(payload, 13)?;
	Ok((century*100+year, month, day, hour, minute, second))
}

///Interpret an ASN.1 timestamp
///
///# Parameters:
///
/// * `tag`: The ASN.1 tag for the timestamp (can be either UTCTime or GeneralizedTime).
/// * `payload`: The contents of the ASN.1 tag.
///
///# Return value:
///
/// * The parsed timestamp, in seconds since UNIX epoch.
///
///# Failures:
///
/// * If the timestamp is invalid, fails with an error.
pub fn interpret_timestamp<'a>(tag: Asn1Tag, payload: Source<'a>) -> Result<Timestamp, TimestampParseError>
{
	use self::TimestampParseError::*;
	let payload = payload.as_slice();
	let (year, month, day, hour, minute, second) = if tag == ASN1_UTC_TIME {
		//UTCTime: YYMMDDHHMMSS"Z"
		read_utc_timestmap(payload).map_err(|_|BadUtcTime)?
	} else if tag == ASN1_GENERALIZED_TIME {
		//GeneralizedTime: YYYYMMDDHHMMSS"Z"
		read_general_timestmap(payload).map_err(|_|BadGeneralizedTime)?
	} else { fail!(UnrecognizedFormat); };
	fail_if!(year < 1 || year > 9999 || month > 12 || day > 31, InvalidDate(year as i64, month as usize,
		day as i64));
	//23:59:60 might be valid.
	fail_if!(hour > 23 || minute > 59 || second > 60, InvalidTime(hour, minute, second));
	fail_if!((hour != 23 || minute != 59) && second == 60, InvalidTime(hour, minute, second));
	//These won't come even near overflowing, since max year is 9999 and min is 1.
	let time_of_day = (hour as i64).wrapping_mul(3600).wrapping_add((minute as i64).wrapping_mul(60)).
		wrapping_add(second as i64);
	let daynum = get_day_num(year as i64, month as usize, day as i64)?;
	Ok(Timestamp::new(daynum.wrapping_mul(86400).wrapping_add(time_of_day)))
}

///Decode timestamp to human-readable format.
///
///# Praameters:
///
/// * `ts`: The timestamp to decode (seconds since UNIX epoch).
/// * `utc`: If true, use UTC. Otherwise use local timezone.
///
///# Returns:
///
/// * The timestamp as string.
pub struct DecodeTsForUser(pub Timestamp, pub bool);


impl Display for DecodeTsForUser
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		extern crate time;
		let x = time::Timespec{sec:self.0.into_inner(), nsec:0};
		let (x, flag) = if self.1 { (time::at_utc(x), "Z") } else { (time::at(x), "J") };
		fmt.write_fmt(format_args!("{:04}{:02}{:02}T{:02}{:02}{:02}{}", x.tm_year.saturating_add(1900),
			x.tm_mon.saturating_add(1), x.tm_mday, x.tm_hour, x.tm_min, x.tm_sec, flag))
	}
}

#[test]
fn check_daynum()
{
	assert_eq!(get_day_num(2016, 8, 4).unwrap(), 17017);
	assert_eq!(get_day_num(370, 1, 1).unwrap(), -3*146097-146086);
	assert_eq!(get_day_num(770, 1, 1).unwrap(), -2*146097-146086);
	assert_eq!(get_day_num(1170, 1, 1).unwrap(), -146097-146086);
	assert_eq!(get_day_num(1570, 1, 1).unwrap(), -146086);
	assert_eq!(get_day_num(1970, 1, 1).unwrap(), 0);
	assert_eq!(get_day_num(2370, 1, 1).unwrap(), 146097);
	assert_eq!(get_day_num(1969, 12, 31).unwrap(), -1);
	assert_eq!(get_day_num(2370, 1, 1).unwrap()-1, get_day_num(2369, 12, 31).unwrap());
	assert_eq!(get_day_num(1999, 1, 1).unwrap()+59, get_day_num(1999, 3, 1).unwrap());
	assert_eq!(get_day_num(2000, 1, 1).unwrap()+60, get_day_num(2000, 3, 1).unwrap());
	assert_eq!(get_day_num(2100, 1, 1).unwrap()+59, get_day_num(2100, 3, 1).unwrap());

	//See "cal 9 1752".
	assert_eq!(get_day_num(1752, 9, 2).unwrap()+1, get_day_num(1752, 9, 14).unwrap());
	get_day_num(1752, 9, 3).unwrap_err();
	get_day_num(1752, 9, 13).unwrap_err();

	get_day_num(2000, 0, 1).unwrap_err();
	get_day_num(2000, 1, 32).unwrap_err();
	get_day_num(2000, 2, 30).unwrap_err();
	get_day_num(1999, 2, 29).unwrap_err();
	get_day_num(2000, 3, 32).unwrap_err();
	get_day_num(2000, 4, 31).unwrap_err();
	get_day_num(2000, 5, 32).unwrap_err();
	get_day_num(2000, 6, 31).unwrap_err();
	get_day_num(2000, 7, 32).unwrap_err();
	get_day_num(2000, 8, 32).unwrap_err();
	get_day_num(2000, 9, 31).unwrap_err();
	get_day_num(2000, 10, 32).unwrap_err();
	get_day_num(2000, 11, 31).unwrap_err();
	get_day_num(2000, 12, 32).unwrap_err();
	get_day_num(2000, 13, 1).unwrap_err();
	get_day_num(0, 1, 1).unwrap_err();
	assert_eq!(get_day_num(2000, 1, 31).unwrap()+29, get_day_num(2000, 2, 29).unwrap());
	get_day_num(1999, 2, 28).unwrap();
	assert_eq!(get_day_num(2000, 3, 31).unwrap()+30, get_day_num(2000, 4, 30).unwrap());
	assert_eq!(get_day_num(2000, 5, 31).unwrap()+30, get_day_num(2000, 6, 30).unwrap());
	assert_eq!(get_day_num(2000, 7, 31).unwrap()+31, get_day_num(2000, 8, 31).unwrap());
	assert_eq!(get_day_num(2000, 9, 30).unwrap()+31, get_day_num(2000, 10, 31).unwrap());
	assert_eq!(get_day_num(2000, 11, 30).unwrap()+31, get_day_num(2000, 12, 31).unwrap());
}


#[test]
fn timestamp()
{
	interpret_timestamp(ASN1_UTC_TIME, Source::new("".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_UTC_TIME, Source::new("ERR".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_UTC_TIME, Source::new("000101000000".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_UTC_TIME, Source::new("000101000000Y".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_UTC_TIME, Source::new("000101000000+0000".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("ERR".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("20000101000000".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("20000101000000Y".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("20000101000000+0000".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("20000101000000.000Z".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("200001010000Z".as_bytes())).
		unwrap_err();
	interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("2000010100Z".as_bytes())).
		unwrap_err();
	assert_eq!(interpret_timestamp(ASN1_UTC_TIME, Source::new("700101000000Z".
		as_bytes())).unwrap(), Timestamp::new(0));
	assert_eq!(interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("19700101000000Z".
		as_bytes())).unwrap(), Timestamp::new(0));
	assert_eq!(interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("20700101000000Z".
		as_bytes())).unwrap(), Timestamp::new(3155760000));
	assert!(interpret_timestamp(ASN1_UTC_TIME, Source::new("491231235959Z".
		as_bytes())).unwrap() > Timestamp::new(0));
	assert!(interpret_timestamp(ASN1_UTC_TIME, Source::new("500101000000Z".
		as_bytes())).unwrap() < Timestamp::new(0));
	assert_eq!(interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("20160804002129Z".
		as_bytes())).unwrap(), Timestamp::new(1470270089));
	assert_eq!(interpret_timestamp(ASN1_UTC_TIME, Source::new("160804002129Z".
		as_bytes())).unwrap(), Timestamp::new(1470270089));
	assert_eq!(interpret_timestamp(ASN1_UTC_TIME, Source::new("160804042129Z".
		as_bytes())).unwrap(), Timestamp::new(1470270089 + 4 * 3600));
}

#[test]
fn timestamp_utc_boundaries()
{
	assert!(interpret_timestamp(ASN1_UTC_TIME, Source::new("491231235959Z".as_bytes())).unwrap() ==
		interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("20491231235959Z".as_bytes())).unwrap());
	assert!(interpret_timestamp(ASN1_UTC_TIME, Source::new("500101000000Z".as_bytes())).unwrap() ==
		interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new("19500101000000Z".as_bytes())).unwrap());
}

#[test]
fn ts_userprint()
{
	assert_eq!(format!("{}", DecodeTsForUser(Timestamp::new(1470270089), true)), "20160804T002129Z");
}

#[test]
fn ts_leap_second()
{
	assert_eq!(interpret_timestamp(ASN1_UTC_TIME, Source::new("161231235960Z".as_bytes())).unwrap(),
		Timestamp::new(1483228800));
	assert!(interpret_timestamp(ASN1_UTC_TIME, Source::new("161231225960Z".as_bytes())).is_err());
	assert!(interpret_timestamp(ASN1_UTC_TIME, Source::new("161231235860Z".as_bytes())).is_err());
	assert!(interpret_timestamp(ASN1_UTC_TIME, Source::new("161231225860Z".as_bytes())).is_err());
	assert!(interpret_timestamp(ASN1_UTC_TIME, Source::new("161231235961Z".as_bytes())).is_err());
	assert!(interpret_timestamp(ASN1_UTC_TIME, Source::new("161231236000Z".as_bytes())).is_err());
	assert!(interpret_timestamp(ASN1_UTC_TIME, Source::new("161231240000Z".as_bytes())).is_err());
}


#[test]
fn timestamp_infinities()
{
	let neg_inf = "00010101000000Z".as_bytes();
	let pos_inf = "99991231235959Z".as_bytes();
	//Day -719151: 1st January 1CE
	//Day 2932897: 1st January 10000CE
	let neg_inf_day = -719151;	//Day of 1st January 1CE
	let pos_inf_day = 2932897;	//Day of 1st December 10000CE
	assert_eq!(interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new(neg_inf)).unwrap(),
		Timestamp::new(neg_inf_day * 86400));
	assert_eq!(interpret_timestamp(ASN1_GENERALIZED_TIME, Source::new(pos_inf)).unwrap(),
		Timestamp::new(pos_inf_day * 86400 - 1));
}
