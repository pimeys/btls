use btls::ClientConfiguration;
use btls::client_certificates::{HostSpecificPin, LogHashFunction, TrustAnchor, TrustedLog};
use btls::logging::StderrLog;

use std::env::var;
use std::io::{Read as IoRead, Write as IoWrite, stderr};
use std::panic::UnwindSafe;

#[path="./session.rs"]
mod session;
use self::session::{handle_peer};
pub use self::session::{TryClone};

pub fn do_it<Socktype:IoRead+IoWrite+TryClone+Send+UnwindSafe+'static>(sock: Socktype, reference: Option<String>,
	trust_anchors: Vec<TrustAnchor>, pins: Vec<HostSpecificPin>)
{
	let google_aviator = include_bytes!("google-aviator.ctkey");
	let google_pilot = include_bytes!("google-pilot.ctkey");
	let google_rocketeer = include_bytes!("google-rocketeer.ctkey");
	let digicert = include_bytes!("digicert.ctkey");

	let google_aviator = TrustedLog {
		name: "Google Aviator".to_owned(),
		expiry: None,
		key: (&google_aviator[..]).to_owned(),
		v2_hash: LogHashFunction::Sha256,
		v2_id: Vec::new(),	//No V2 id.
	};
	let google_pilot = TrustedLog {
		name: "Google Pilot".to_owned(),
		expiry: None,
		key: (&google_pilot[..]).to_owned(),
		v2_hash: LogHashFunction::Sha256,
		v2_id: Vec::new(),	//No V2 id.
	};
	let google_rocketeer = TrustedLog {
		name: "Google Rocketeer".to_owned(),
		expiry: None,
		key: (&google_rocketeer[..]).to_owned(),
		v2_hash: LogHashFunction::Sha256,
		v2_id: Vec::new(),	//No V2 id.
	};
	let digicert = TrustedLog {
		name: "Digicert".to_owned(),
		expiry: None,
		key: (&digicert[..]).to_owned(),
		v2_hash: LogHashFunction::Sha256,
		v2_id: Vec::new(),	//No V2 id.
	};
	let nocheck = &reference.clone().unwrap() == "no-checking.invalid";
	if trust_anchors.len() == 0 && pins.len() == 0 && !nocheck {
		panic!("Need a target, reference and at least one trust anchor");
	}
	let mut client_config = ClientConfiguration::from(&trust_anchors[..]);
	if let Ok(settings) = var("BTLS_SETTINGS") {
		client_config.config_flags(&settings, |error|{writeln!(stderr(), "{}", error).unwrap();});
	}
	client_config.set_debug(!0, Box::new(StderrLog));
	client_config.add_ct_log(&google_aviator);
	client_config.add_ct_log(&google_pilot);
	client_config.add_ct_log(&google_rocketeer);
	client_config.add_ct_log(&digicert);
	let client_session = if nocheck {
		client_config.make_session_insecure_nocheck().unwrap()
	} else {
		client_config.make_session_pinned(&reference.clone().unwrap(), &pins).unwrap()
	};
	handle_peer(client_session, sock, "Client");
}
