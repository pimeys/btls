pub use super::common::TryClone;

use std::io::{Error as IoError, ErrorKind as IoErrorKind, Read as IoRead, Result as IoResult, Write as IoWrite};
use std::net::{SocketAddr, TcpListener,TcpStream};

pub enum TransportListener
{
	Tcp(TcpListener),
	Unix(UnixListenerWrapper),
}

pub enum TransportStream
{
	Tcp(TcpStream),
	Unix(UnixStreamWrapper),
}

#[derive(Clone)]
pub enum TransportAddress
{
	Tcp(SocketAddr),
	Unix(UnixSocketAddressWrapper),
}

#[cfg(not(unix))]
mod unixwrapper
{
	use super::TryClone;

	use std::io::{Error as IoError, ErrorKind as IoErrorKind, Read as IoRead, Result as IoResult,
		Write as IoWrite};
	use std::path::Path;

	//Dummy versions of UnixStream and UnixListener.
	#[derive(Copy,Clone)]
	enum Void {}
	pub struct UnixStreamWrapper(Void);
	pub struct UnixListenerWrapper(Void);
	#[derive(Clone)]
	pub struct UnixSocketAddressWrapper(Void);

	impl TryClone for UnixStreamWrapper
	{
		fn try_clone_trait(&self) -> IoResult<Self>
		{
			Ok(UnixStreamWrapper(self.0))
		}
	}

	impl IoRead for UnixStreamWrapper
	{
		fn read(&mut self, buf: &mut [u8]) -> IoResult<usize>
		{
			Ok(buf.len())
		}
	}

	impl IoWrite for UnixStreamWrapper
	{
		fn write(&mut self, buf: &[u8]) -> IoResult<usize>
		{
			Ok(buf.len())
		}
		fn flush(&mut self) -> IoResult<()>
		{
			Ok(())
		}
	}

	impl UnixStreamWrapper
	{
		pub fn connect<P: AsRef<Path>>(_path: P) -> IoResult<UnixStreamWrapper>
		{
			Err(IoError::new(IoErrorKind::Other, "Unix domain sockets not supported on Windows"))
		}
	}

	impl UnixListenerWrapper
	{
		pub fn bind<P: AsRef<Path>>(_path: P) -> IoResult<UnixListenerWrapper>
		{
			Err(IoError::new(IoErrorKind::Other, "Unix domain sockets not supported on Windows"))
		}
		pub fn accept(&self) -> IoResult<(UnixStreamWrapper, UnixSocketAddressWrapper)>
		{
			Ok((UnixStreamWrapper(self.0), UnixSocketAddressWrapper(self.0)))
		}
	}
}

#[cfg(unix)]
mod unixwrapper
{
	use super::TryClone;

	use std::io::{Read as IoRead, Result as IoResult, Write as IoWrite};
	use std::os::unix::net::{SocketAddr, UnixListener, UnixStream};
	use std::path::Path;

	pub struct UnixStreamWrapper(UnixStream);
	pub struct UnixListenerWrapper(UnixListener);
	#[derive(Clone)]
	pub struct UnixSocketAddressWrapper(SocketAddr);

	impl TryClone for UnixStreamWrapper
	{
		fn try_clone_trait(&self) -> IoResult<Self>
		{
			self.0.try_clone().map(|x|UnixStreamWrapper(x))
		}
	}

	impl IoRead for UnixStreamWrapper
	{
		fn read(&mut self, buf: &mut [u8]) -> IoResult<usize>
		{
			self.0.read(buf)
		}
	}

	impl IoWrite for UnixStreamWrapper
	{
		fn write(&mut self, buf: &[u8]) -> IoResult<usize>
		{
			self.0.write(buf)
		}
		fn flush(&mut self) -> IoResult<()>
		{
			self.0.flush()
		}
	}

	impl UnixStreamWrapper
	{
		pub fn connect<P: AsRef<Path>>(path: P) -> IoResult<UnixStreamWrapper>
		{
			UnixStream::connect(path).map(|x|UnixStreamWrapper(x))
		}
	}

	impl UnixListenerWrapper
	{
		pub fn bind<P: AsRef<Path>>(path: P) -> IoResult<UnixListenerWrapper>
		{
			UnixListener::bind(path).map(|x|UnixListenerWrapper(x))
		}
		pub fn accept(&self) -> IoResult<(UnixStreamWrapper, UnixSocketAddressWrapper)>
		{
			self.0.accept().map(|x|(UnixStreamWrapper(x.0), UnixSocketAddressWrapper(x.1)))
		}
	}
}

pub use self::unixwrapper::{UnixListenerWrapper, UnixSocketAddressWrapper, UnixStreamWrapper};

impl TryClone for TransportStream
{
	fn try_clone_trait(&self) -> IoResult<Self>
	{
		match self {
			&TransportStream::Tcp(ref x) => x.try_clone().map(|x|TransportStream::Tcp(x)),
			&TransportStream::Unix(ref x) => x.try_clone_trait().map(|x|TransportStream::Unix(x)),
		}
	}
}

impl IoRead for TransportStream
{
	fn read(&mut self, buf: &mut [u8]) -> IoResult<usize>
	{
		match self {
			&mut TransportStream::Tcp(ref mut x) => x.read(buf),
			&mut TransportStream::Unix(ref mut x) => x.read(buf),
		}
	}
}

impl IoWrite for TransportStream
{
	fn write(&mut self, buf: &[u8]) -> IoResult<usize>
	{
		match self {
			&mut TransportStream::Tcp(ref mut x) => x.write(buf),
			&mut TransportStream::Unix(ref mut x) => x.write(buf),
		}
	}
	fn flush(&mut self) -> IoResult<()>
	{
		match self {
			&mut TransportStream::Tcp(ref mut x) => x.flush(),
			&mut TransportStream::Unix(ref mut x) => x.flush(),
		}
	}
}

impl TransportStream
{
	pub fn connect(name: &str) -> IoResult<(TransportStream, String)>
	{
		let mut refname: Option<String> = None;
		let name = if name.contains("#") {
			let idx = name.rfind("#").unwrap();
			refname = Some((&name[idx+1..]).to_owned());
			&name[..idx]
		} else {
			name
		};
		if name.contains(":") && !name.contains("/") {
			//Assme TCP.
			if refname.is_none() {
				//Just take the part before ':'.
				refname = Some(name.rsplitn(2, ":").nth(1).unwrap().to_owned());
			}
			return TcpStream::connect(name).map(|x|(TransportStream::Tcp(x), refname.unwrap()));
		} else {
			if refname.is_none() {
				return Err(IoError::new(IoErrorKind::Other,
					"Unix domain sockets need reference name"))
			}
			return UnixStreamWrapper::connect(name).map(|x|(TransportStream::Unix(x), refname.unwrap()));
		}
	}
}

impl TransportListener
{
	pub fn bind(name: &str) -> IoResult<TransportListener>
	{
		if name.contains(":") && !name.contains("/") {
			//Assme TCP.
			return TcpListener::bind(name).map(|x|TransportListener::Tcp(x));
		} else {
			//Assme Unix.
			return UnixListenerWrapper::bind(name).map(|x|TransportListener::Unix(x));
		}
	}
	pub fn accept(&self) -> IoResult<(TransportStream, TransportAddress)>
	{
		match self {
			&TransportListener::Tcp(ref x) => x.accept().map(|x|(TransportStream::Tcp(x.0),
				TransportAddress::Tcp(x.1))),
			&TransportListener::Unix(ref x) => x.accept().map(|x|(TransportStream::Unix(x.0),
				TransportAddress::Unix(x.1))),
		}
	}
}
