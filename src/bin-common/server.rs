use btls::ServerConfiguration;
use btls::logging::StderrLog;
use btls::server_certificates::CertificateLookup;

use std::env::var;
use std::io::{Write as IoWrite, stderr};

#[path="./session.rs"]
mod session;
use self::session::handle_peer;
pub use self::session::{TryClone};
use super::transport::TransportListener;

pub fn do_it<Lookup:CertificateLookup+Send+Sync+'static>(acceptsock: TransportListener, store: Lookup)
{
	let mut server_config = ServerConfiguration::from(&store as &CertificateLookup);
	if let Ok(settings) = var("BTLS_SETTINGS") {
		server_config.config_flags(&settings, |error|{writeln!(stderr(), "{}", error).unwrap();});
	}
	server_config.set_debug(!0, Box::new(StderrLog));
	if let Ok(acmepath) = var("ACME_CHALLENGE_PATH") {
		server_config.set_acme_dir(&acmepath);
	}
	server_config.create_acme_challenge("test1.acme.invalid", None).unwrap();
	server_config.create_acme_challenge("test2.acme.invalid", Some("response.acme.invalid")).unwrap();
	writeln!(stderr(), "Waiting for connection...").unwrap();
	let mut sock = None;
	while sock.is_none() {
		let (sock2, _) = match acceptsock.accept() {
			Ok(x) => x,
			Err(x) => {
				writeln!(stderr(), "Can't accept connection: {}", x).unwrap();
				continue;
			}
		};
		sock = Some(sock2);
	}
	let sock = sock.unwrap();
	writeln!(stderr(), "Received connection...").unwrap();
	let server_session = server_config.make_session().unwrap();
	handle_peer(server_session, sock, "Server");
}
