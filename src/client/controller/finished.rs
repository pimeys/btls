use super::{HandshakeState2, Tls12ShowtimeFields, Tls13ShowtimeFields};
use super::changecipherspec::Tls12ServerChangeCipherSpecFields;
use ::common::{Debugger, emit_handshake, HandshakeHash, HandshakeMessage, HashMessages, HSTYPE_CERTIFICATE,
	HSTYPE_FINISHED, NamesContainer, TlsFailure};
use ::record::{SessionBase, SessionController, Tls12MasterSecret, Tls13MasterSecret, Tls13TrafficSecretIn,
	Tls13TrafficSecretOut};
use ::stdlib::Vec;

use btls_aux_serialization::Sink;

pub struct Tls12ClientFinishedFields
{
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub master_secret: Tls12MasterSecret,		//The master secret.
	pub sig_verified: bool,				//Exchange Signature verified.
	pub hs_hash: HandshakeHash,
}

pub struct Tls12ServerFinishedFields
{
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub master_secret: Tls12MasterSecret,		//The master secret.
	pub sig_verified: bool,				//Exchange Signature verified.
	pub hs_hash: HandshakeHash,
}

pub struct Tls13ServerFinishedFields
{
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub client_hs_write: Tls13TrafficSecretOut,
	pub master_secret: Tls13MasterSecret,
	pub requested_certificate: bool,		//Certificate has been requested.
	pub server_hs_write: Tls13TrafficSecretIn,
	pub sig_verified: bool,			//Exchange Signature verified.
	pub hs_hash: HandshakeHash,
}

pub struct Tls13ClientFinishedFields
{
	pub client_app_write: Tls13TrafficSecretOut,
	pub client_hs_write: Tls13TrafficSecretOut,
	pub server_app_write: Tls13TrafficSecretIn,
	pub hs_hash: HandshakeHash,
}

impl Tls13ServerFinishedFields
{
	//Handle received Finished message.
	pub fn rx_finished<'a>(self, controller: &mut SessionController, msg: &[u8], names: &mut NamesContainer,
		debug: Debugger, raw_msg: HandshakeMessage<'a>) -> Result<HandshakeState2, TlsFailure>
	{
		//The finished message just consists of the MAC, so no need to parse it.
		self.server_hs_write.check_finished(&self.hs_hash.checkpoint()?, msg, debug.clone())?;
		if !self.bypass_certificate_validation { names.release_server_names(); }
		sanity_check!(self.sig_verified, "Interlock missing signature verification");

		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug.clone())?;

		let hs_hash = hs_hash;
		let app_hash = hs_hash.checkpoint()?;
		let server_app_write = self.master_secret.traffic_secret_in(&app_hash, debug.clone())?;
		let client_app_write = self.master_secret.traffic_secret_out(&app_hash, debug.clone())?;
		let extractor = self.master_secret.extractor(&app_hash, debug.clone())?;
		controller.set_deprotector(server_app_write.to_deprotector(debug.clone())?);
		controller.set_extractor(extractor);
		debug!(HANDSHAKE_EVENTS debug, "Received Finished, downstream encryption changing keys");
		let inner = Tls13ClientFinishedFields {
			client_app_write: client_app_write,
			client_hs_write: self.client_hs_write,
			server_app_write: server_app_write,
			hs_hash: hs_hash,
		};
		Ok(if self.requested_certificate {
			HandshakeState2::Tls13ClientCertificate(inner)
		} else {
			HandshakeState2::Tls13ClientFinished(inner)
		})
	}
}

impl Tls13ClientFinishedFields
{
	//Transmit a certificate message.
	pub fn tx_certificate(mut self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut content = Vec::new();
		//In TLS 1.3, certificates have context field.
		//This is only used for in-handshake auth, and there context needs to be empty.
		try_buffer!(content.write_u8(0));
		//Just put an empty certificate list (we always refuse authentication).
		try_buffer!(content.write_u24(0));

		emit_handshake(debug, HSTYPE_CERTIFICATE, &content, base, &mut self.hs_hash, state)?;
		Ok(HandshakeState2::Tls13ClientFinished(self))
	}
	//Transmit a Finished message.
	pub fn tx_finished(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		let htype = HSTYPE_FINISHED;
		let mac = self.client_hs_write.generate_finished(&hs_hash.checkpoint()?, debug.clone())?;
		emit_handshake(debug.clone(), htype, mac.as_ref(), base, &mut hs_hash, state)?;
		base.change_protector(self.client_app_write.to_protector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Sent Finished, upstream encryption \
			changing keys");
		//hs_hash at this point is the final handshake hash, e.g. for RMS.
		Ok(HandshakeState2::Tls13Showtime(Tls13ShowtimeFields {
			client_app_write: self.client_app_write,
			rekey_in_progress: false,
			server_app_write: self.server_app_write,
		}))
	}
}

impl Tls12ClientFinishedFields
{
	//Transmit a Finished message.
	pub fn tx_finished(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		let htype = HSTYPE_FINISHED;
		let mac = self.master_secret.generate_finished(&hs_hash.checkpoint()?, debug.clone())?;
		emit_handshake(debug.clone(), htype, mac.as_ref(), base, &mut hs_hash, state)?;
		base.change_extractor(self.master_secret.get_exporter(debug)?);
		Ok(HandshakeState2::Tls12ServerChangeCipherSpec(Tls12ServerChangeCipherSpecFields{
			bypass_certificate_validation:self.bypass_certificate_validation,
			master_secret: self.master_secret,
			sig_verified:self.sig_verified,
			hs_hash: hs_hash,
		}))
	}
}

impl Tls12ServerFinishedFields
{
	//Handle received Finished message.
	pub fn rx_finished<'a>(self, _controller: &mut SessionController, msg: &[u8], names: &mut NamesContainer,
		debug: Debugger, raw_msg: HandshakeMessage<'a>) -> Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		//The finished message just consists of the MAC, so no need to parse it.
		self.master_secret.check_finished(&hs_hash.checkpoint()?, debug.clone(), msg)?;
		if !self.bypass_certificate_validation { names.release_server_names(); }
		sanity_check!(self.sig_verified, "Interlock missing signature verification");

		//The final handshake hash.
		hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls12Showtime(Tls12ShowtimeFields{
		}))
	}
}
