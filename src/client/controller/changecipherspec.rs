use super::HandshakeState2;
use ::client::controller::finished::{Tls12ServerFinishedFields, Tls12ClientFinishedFields};
use ::common::{Debugger, HandshakeHash, TlsFailure};
use ::record::{SessionBase, SessionController, Tls12MasterSecret};

pub struct Tls12ClientChangeCipherSpecFields
{
	pub sig_verified: bool,				//Exchange Signature verified.
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub master_secret: Tls12MasterSecret,		//The master secret.
	pub hs_hash: HandshakeHash,
}

pub struct Tls12ServerChangeCipherSpecFields
{
	pub sig_verified: bool,				//Exchange Signature verified.
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub master_secret: Tls12MasterSecret,		//The master secret.
	pub hs_hash: HandshakeHash,
}

impl Tls12ClientChangeCipherSpecFields
{
	//Transmit a ChangeCipherSpec message.
	pub fn tx_ccs(self, base: &mut SessionBase, debug: Debugger) -> Result<HandshakeState2, TlsFailure>
	{
		base.send_message_fragment(20, &[1]);
		base.change_protector(self.master_secret.get_protector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Sent ChangeCipherSpec, upstream encryption enabled");
		Ok(HandshakeState2::Tls12ClientFinished(Tls12ClientFinishedFields{
			bypass_certificate_validation: self.bypass_certificate_validation,
			sig_verified: self.sig_verified,
			master_secret: self.master_secret,
			hs_hash: self.hs_hash,
		}))
	}
}

impl Tls12ServerChangeCipherSpecFields
{
	//Handle received ChangeCipherSpec message.
	pub fn rx_change_cipher_spec(self, controller: &mut SessionController, debug: Debugger)
		-> Result<HandshakeState2, TlsFailure>
	{
		//Not real handshake message, thus not added to the hash.
		controller.set_deprotector(self.master_secret.get_deprotector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Received ChangeCipherSpec, downstream encryption enabled");
		Ok(HandshakeState2::Tls12ServerFinished(Tls12ServerFinishedFields{
			bypass_certificate_validation:self.bypass_certificate_validation,
			sig_verified:self.sig_verified,
			master_secret: self.master_secret,
			hs_hash: self.hs_hash,
		}))
	}
}
