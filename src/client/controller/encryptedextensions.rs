use super::HandshakeState2;
use ::client::ClientConfiguration;
use ::client::controller::certificate::Tls13CertificateFields;
use ::client_certificates::HostSpecificPin;
use ::common::{Debugger, HandshakeHash, HandshakeMessage, HashMessages, NamesContainer, TlsFailure};
use ::messages::TlsEncryptedExtensions;
use ::record::{SessionController, Tls13MasterSecret, Tls13TrafficSecretIn, Tls13TrafficSecretOut};
use ::stdlib::{String, ToOwned, Vec};

pub struct Tls13EncryptedExtensionsFields
{
	pub requested_sni: String,			//The SNI that has been requested.
	pub pins: Vec<HostSpecificPin>,			//Host-specific pins
	pub features: u32,				//Features supported.
	pub killist_empty: bool,			//Certificate killist forced empty.
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub server_hs_write: Tls13TrafficSecretIn,
	pub client_hs_write: Tls13TrafficSecretOut,
	pub master_secret: Tls13MasterSecret,
	pub hs_hash: HandshakeHash,
}

impl Tls13EncryptedExtensionsFields
{
	//Handle received EncryptedExtensions message.
	pub fn rx_encrypted_extensions<'a,'b>(self, controller: &mut SessionController,
		eemsg: TlsEncryptedExtensions<'a>, names: &mut NamesContainer, debug: Debugger,
		config: &ClientConfiguration, raw_msg: HandshakeMessage<'b>) -> Result<HandshakeState2, TlsFailure>
	{
		//ALPN: Record it. And present or not, it is known.
		if let Some(alpn) = eemsg.alpn {
			let alpn_candidates = &config.supported_alpn;
			fail_if!(alpn_candidates.iter().filter(|x|*x==&alpn).next().is_none(),
				TlsFailure::BogusAlpnProtocol);
			debug!(HANDSHAKE_EVENTS debug, "Application Layer Protocol is '{}'", alpn);
			names.set_alpn(alpn.to_owned());
		}
		if let Some(limit) = eemsg.max_record_size {
			debug!(HANDSHAKE_EVENTS debug, "Maximum fragment size supported by peer is {}", limit);
			controller.set_max_fragment_length(limit);
		}
		//ALPN is available, if any.
		names.alpn_known();

		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls13Certificate(false, Tls13CertificateFields{
			requested_sni: self.requested_sni,
			pins: self.pins,
			features: self.features,
			killist_empty: self.killist_empty,
			bypass_certificate_validation: self.bypass_certificate_validation,
			client_hs_write: self.client_hs_write,
			server_hs_write: self.server_hs_write,
			master_secret: self.master_secret,
			hs_hash: hs_hash,
		}))
	}
}
