use super::HandshakeState2;
use super::finished::Tls13ServerFinishedFields;
use super::serverkeyexchange::Tls12ServerKeyExchangeFields;
use ::certificate::{CFLAG_MUST_CT, CFLAG_MUST_STAPLE, CFLAG_MUST_TLS13, extract_spki, OcspDumpedScts,
	OcspValidationResult, ParsedCertificate, validate_chain, validate_ee_spki, validate_scts};
use ::client::ClientConfiguration;
use ::client_certificates::HostSpecificPin;
use ::common::{CryptoTracker, Debugger, format_tbs_tls13, HandshakeHash, HandshakeMessage, HashMessages,
	NamesContainer, TlsFailure};
use ::features::{FLAGS0_ENABLE_TLS13, FLAGS0_REQUIRE_CT, FLAGS0_REQUIRE_OCSP};
use ::logging::bytes_as_hex_block;
use ::messages::{Tls12Certificate, Tls13Certificate, Tls13CertificateRequest, TlsCertificateVerify};
use ::record::{SessionController, Tls13MasterSecret, Tls13TrafficSecretIn, Tls13TrafficSecretOut};
use ::stdlib::{Arc, Box, BTreeMap, from_utf8, String, ToOwned, Vec};
use ::utils::subslice_to_range;

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::DiffieHellmanKey;
use btls_aux_hash::HashFunction;
use btls_aux_signature_algo::VERIFY_FLAG_NO_RSA_PKCS1;
use btls_aux_time::TimeNow;

struct DoValidateServerCertChainParams<'a>
{
	dont_validate_ocsp: &'a mut bool,
	bypass_certificate_validation: bool,
	requested_sni: &'a str,
	features: u32,
	use_spki_only: bool,
	pins: &'a mut Vec<HostSpecificPin>,
	server_spki: &'a mut Vec<u8>,
	killist_empty: bool,
	require_ocsp_unsat: &'a mut bool,
	require_sct_unsat: &'a mut bool,
}

//Handle main chain validation.
fn do_validate_server_cert_chain<'b,'a>(eecert: &'b [u8], icerts: &[&[u8]], names: &mut NamesContainer,
	debug: Debugger, params: DoValidateServerCertChainParams<'a>, config: &ClientConfiguration) ->
	Result<Option<ParsedCertificate<'b>>, TlsFailure>
{
	let mut features = params.features;

	if params.bypass_certificate_validation {
		//There is no certificate chain, so skip OCSP.
		*params.dont_validate_ocsp = true;
		if params.use_spki_only {
			*params.server_spki = eecert.to_owned();
		} else {
			//There is no names list available in this mode.
			let eecert_p = ParsedCertificate::from(eecert).map_err(|x|TlsFailure::EECertParseError(x))?;
			*params.server_spki = eecert_p.pubkey.to_owned();
		}
		return Ok(None);
	}
	//If TLS 1.3 is disabled, consider requirement for TLS 1.3 met.
	if (config.flags[0] & FLAGS0_ENABLE_TLS13) != 0 { features |= CFLAG_MUST_TLS13; }
	//If use_spki_only is active, just validate the server key.
	let eecert_p = if params.use_spki_only {
		//OCSP can't be done with RPK.
		*params.dont_validate_ocsp = true;
		match validate_ee_spki(eecert, params.pins) {
			Ok(_) => (),
			Err(_) => fail!(TlsFailure::ServerKeyNotWhitelisted)
		};
		*params.server_spki = eecert.to_owned();
		None
	} else {
		let empty_map = BTreeMap::new();
		let killist = if params.killist_empty { &empty_map } else { &config.killist };
		match validate_chain(eecert, &icerts, params.requested_sni, &TimeNow, features,
			&config.trust_anchors, params.pins, killist, params.require_ocsp_unsat,
			params.require_sct_unsat) {
			//true means pinned to SPKI, in this case, no OCSP can be done.
			Ok(x) => *params.dont_validate_ocsp = x,
			Err(ref x) => fail!(TlsFailure::CertificateValidationFailed(x.clone()))
		};
		//Certificate is OK, extract SPKI and names out of it. Note that we will not provode the server
		//name list before server Finished is recieved and checked.
		let eecert_p = ParsedCertificate::from(eecert).map_err(|x|TlsFailure::EECertParseError(x))?;
		*params.server_spki = eecert_p.pubkey.to_owned();
		names.set_server_names(Arc::new(eecert_p.dnsnames.iter().filter_map(|x|from_utf8(x).ok()).map(|x|
			x.to_owned()).collect()));
		Some(eecert_p)
	};
	debug!(HANDSHAKE_EVENTS debug, "Validated server certificate/key");
	if *params.dont_validate_ocsp {
		debug!(HANDSHAKE_EVENTS debug, "Chain validation was bypassed: Server key is explicitly trusted");
	}
	//Pins and killist are not needed anymore.
	params.pins.clear();
	Ok(eecert_p)
}

pub struct Tls12CertificateFields
{
	pub requested_sni: String,				//The SNI that has been requested.
	pub pins: Vec<HostSpecificPin>,				//Host-specific pins
	pub tls13_shares: Vec<Box<DiffieHellmanKey+Send>>,	//The key shares (also abused for TLS 1.2).
	pub staple_scts: Vec<(bool, Vec<u8>)>,			//Stapled SCTs. The flag is set for precerts.
	pub killist_empty: bool,				//Certificate killist forced empty.
	pub use_spki_only: bool,				//The certificate is raw SPKI.
	pub maybe_send_status: bool,				//Maybe send certificate status.
	pub bypass_certificate_validation: bool,		//Certificate validation was bypassed.
	pub features: u32,					//Features supported.
	pub client_random: [u8; 32],				//The client random.
	pub server_random: [u8; 32],				//The server random.
	pub hs_hash: HandshakeHash,
	pub ems_enabled: bool,
	pub protection: ProtectorType,
	pub prf: HashFunction,
}

pub struct Tls13CertificateFields
{
	pub requested_sni: String,			//The SNI that has been requested.
	pub pins: Vec<HostSpecificPin>,			//Host-specific pins
	pub features: u32,				//Features supported.
	pub killist_empty: bool,			//Certificate killist forced empty.
	pub bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	pub server_hs_write: Tls13TrafficSecretIn,
	pub client_hs_write: Tls13TrafficSecretOut,
	pub master_secret: Tls13MasterSecret,
	pub hs_hash: HandshakeHash,
}

pub struct Tls13CertificateVerifyFields
{
	server_spki: Vec<u8>,			//The SPKI of the server.
	bypass_certificate_validation: bool,	//Certificate validation was bypassed.
	requested_certificate: bool,		//Certificate has been requested.
	server_hs_write: Tls13TrafficSecretIn,
	client_hs_write: Tls13TrafficSecretOut,
	master_secret: Tls13MasterSecret,
	pub hs_hash: HandshakeHash,
}

impl Tls13CertificateFields
{
	//Handle TLS 1.3 CertificateRequest message
	pub fn rx_certificate_request<'a>(mut self, pmsg: Tls13CertificateRequest, debug: Debugger,
		raw_msg: HandshakeMessage<'a>) ->
		Result<HandshakeState2, TlsFailure>
	{
		//We are only interested about the context.
		fail_if!(pmsg.context.len() > 0, TlsFailure::CrContextMustBeEmpty);
		debug!(HANDSHAKE_EVENTS debug, "Unimplemented: Received certificate request. \
			Will NAK.");

		self.hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls13Certificate(true, Tls13CertificateFields{
			requested_sni: self.requested_sni,
			pins: self.pins,
			features: self.features,
			killist_empty: self.killist_empty,
			bypass_certificate_validation: self.bypass_certificate_validation,
			client_hs_write: self.client_hs_write,
			server_hs_write: self.server_hs_write,
			master_secret: self.master_secret,
			hs_hash: self.hs_hash,
		}))
	}
	//Handle TLS 1.3 Certificate message.
	pub fn rx_certificate<'a, 'b>(self, certmsg: Tls13Certificate<'a>, names: &mut NamesContainer,
		debug: Debugger, config: &ClientConfiguration, certificate_reqd: bool, raw_msg: HandshakeMessage<'b>,
		crypto_algs: &mut CryptoTracker) -> Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug.clone())?;

		let mut server_spki = Vec::new();
		let mut pins = self.pins;
		let mut dont_validate_ocsp = false;
		let mut features = self.features;
		let mut staple_scts = Vec::new();
		let require_ct = (config.flags[0] & FLAGS0_REQUIRE_CT) != 0;
		let require_ocsp = (config.flags[0] & FLAGS0_REQUIRE_OCSP) != 0;
		let mut require_ocsp_unsat = require_ocsp;
		let mut require_sct_unsat = require_ct;
		//Extract the EE certificate. This has to be present.
		fail_if!(certmsg.chain.len() == 0, TlsFailure::ServerChainEmpty);
		let eecert = certmsg.chain[0];
		fail_if!(eecert.len() == 0, TlsFailure::EeCertificateEmpty);
		//Read intermediate certs. There can be any amount of these (including 0.) There is at least one
		//certificate, so slice is valid.
		let icerts = &certmsg.chain[1..];

		//Collect the stapled SCTs from the TLS extension (in TLS 1.3, these are inside Certificate message).
		for i in certmsg.stapled_scts {
			debug!(HANDSHAKE_EVENTS debug, "Received SCT#{} using TLS extension.", staple_scts.len());
			staple_scts.push((false, i.to_owned()));
		}
		//Collect the stapled SCTs from OCSP response if any.
		if let Some(ocsp) = certmsg.stapled_ocsp {
			let sctlist = OcspDumpedScts::from(ocsp).map_err(|x|TlsFailure::CantExtractSct13(x))?;
			for i in sctlist.list {
				debug!(HANDSHAKE_EVENTS debug, "Received SCT#{} using OCSP.", staple_scts.len());
				staple_scts.push((false, i.to_owned()));
			}
		}
		//If OCSP staples were sent, satisfy CFLAG_MUST_STAPLE.
		//Always satisfy CFLAG_MUST_CT, because we need to check that later.

		if certmsg.stapled_ocsp.is_some() { features |= CFLAG_MUST_STAPLE; }
		features |= CFLAG_MUST_CT;

		let use_spki_only = certmsg.is_rpk;
		let parsed_eecert = do_validate_server_cert_chain(eecert, icerts, names, debug.clone(),
			DoValidateServerCertChainParams{dont_validate_ocsp: &mut dont_validate_ocsp,
			bypass_certificate_validation: self.bypass_certificate_validation, requested_sni:
			&self.requested_sni, features: features, use_spki_only: use_spki_only, pins: &mut pins,
			server_spki: &mut server_spki, killist_empty: self.killist_empty, require_ocsp_unsat:
			&mut require_ocsp_unsat, require_sct_unsat: &mut require_sct_unsat}, config)?;
		if self.bypass_certificate_validation {
			return Ok(HandshakeState2::Tls13CertificateVerify(Tls13CertificateVerifyFields {
				server_spki: server_spki,
				bypass_certificate_validation: true,			//Always set.
				requested_certificate: certificate_reqd,		//Preserved.
				client_hs_write: self.client_hs_write,
				server_hs_write: self.server_hs_write,
				master_secret: self.master_secret,
				hs_hash: hs_hash,
			}));
		}

		//Collect SCTs from certificate.
		if let &Some(ref pcert) = &parsed_eecert {
			for i in pcert.scts.iter() {
				debug!(HANDSHAKE_EVENTS debug, "Received SCT#{} using certificate.", staple_scts.
					len());
				staple_scts.push((true, (*i).to_owned()));
			}
		}
		let eecert_issuer = match &parsed_eecert {
			&Some(ref x) => Some(x.issuer),
			&None => None
		};

		//Check SCTs.
		if !dont_validate_ocsp {	//dont_validate_ocsp covers use_spki_only.
			if staple_scts.len() > 0 {
				let mut got_one = false;
				//Grab the issuer key, if any is available.
				let key = grab_issuer_key!(icerts, eecert_issuer, config.trust_anchors);
				if let Some(k) = key {
					validate_scts(eecert, k, &mut staple_scts, &mut require_sct_unsat,
						&mut got_one, &config.trusted_logs, debug.clone()).
						map_err(|x|TlsFailure::SctValidationFailed(x))?;
				}
				if got_one { crypto_algs.ct_validated(); }
			}
		} else {
			require_sct_unsat = false;	//Don't require SCTs.
			if require_ct || staple_scts.len() > 0 {
				debug!(HANDSHAKE_EVENTS debug, "Not checking SCT, since chain validation was \
					bypassed");
			}
		}

		//Determine if certificate is short-lived.
		let short_lived_cert = if let &Some(ref ep) = &parsed_eecert {
			ep.not_before.delta(ep.not_after) <= config.ocsp_maxvalid
		} else {
			false
		};

		//On TLS 1.3, validate staple if any.
		if short_lived_cert && !dont_validate_ocsp {
			require_ocsp_unsat = false;	//OCSP is deemed OK.
			debug!(HANDSHAKE_EVENTS debug, "Not checking OCSP, since certificate is short-lived");
			crypto_algs.ocsp_shortlived();
		} else if let (Some(ocsp), false) = (certmsg.stapled_ocsp, dont_validate_ocsp) {
			fail_if!(certmsg.stapled_ocsp_type != 1, TlsFailure::BogusCsOcspType(
				certmsg.stapled_ocsp_type));
			let issuer_key = grab_issuer_key!(icerts, eecert_issuer, config.trust_anchors);
			let issuer_key = issuer_key.ok_or(TlsFailure::OcspIncompleteCertificateChain)?;
			//Grab issuer and serial. We can assume valid EE certificate if we reach here
			//(OCSP validation to be performed).
			let eecertp = parsed_eecert.ok_or(TlsFailure::OcspIncompleteCertificateChain)?;
			let issuer = eecertp.issuer;
			let serial = eecertp.serial_number;
			let results = OcspValidationResult::from(ocsp, issuer, serial, issuer_key, &TimeNow).
				map_err(|x|TlsFailure::InvalidOcsp(x))?;
			let too_long = results.response_lifetime > config.ocsp_maxvalid;
			if too_long {
				debug!(HANDSHAKE_EVENTS debug, "Not considering OCSP valid due to excessive \
					lifetime");
				if require_ocsp_unsat {
					//This certificate needs OCSP, but the response isn't valid because of the
					//too long duration.
					fail!(TlsFailure::OcspValidityTooLong(results.response_lifetime,
						config.ocsp_maxvalid));
				}
			} else {
				require_ocsp_unsat = false;	//OCSP checked.
				debug!(HANDSHAKE_EVENTS debug, "Validated stapled OCSP response");
				crypto_algs.ocsp_validated();
			}
		} else if dont_validate_ocsp {
			require_ocsp_unsat = false;	//Don't require OCSP.
			debug!(HANDSHAKE_EVENTS debug, "Not checking OCSP, since chain \
				validation was bypassed");
		} else {
			debug!(HANDSHAKE_EVENTS debug, "No stapled OCSP response");
		}
		//We have now validated CT and OCSP if we are going to do so.
		fail_if!(require_sct_unsat, TlsFailure::CertRequiresSct);
		fail_if!(require_ocsp_unsat, TlsFailure::CertRequiresOcsp);

		Ok(HandshakeState2::Tls13CertificateVerify(Tls13CertificateVerifyFields {
			server_spki: server_spki,
			bypass_certificate_validation: self.bypass_certificate_validation,
			requested_certificate: certificate_reqd,
			client_hs_write: self.client_hs_write,
			server_hs_write: self.server_hs_write,
			master_secret: self.master_secret,
			hs_hash: hs_hash,
		}))
	}
}

impl Tls13CertificateVerifyFields
{
	//Handle TLS 1.3 CertificateVerify message.
	pub fn rx_certificate_verify<'a,'b>(self, _controller: &mut SessionController,
		cvmsg: TlsCertificateVerify<'a>, debug: Debugger, raw_msg: HandshakeMessage,
		crypto_algs: &mut CryptoTracker) -> Result<HandshakeState2, TlsFailure>
	{
		//Format TBS and verify signature.
		let cv_hash = self.hs_hash.checkpoint()?;
		let tbs = format_tbs_tls13(cv_hash, true)?;
		debug!(CRYPTO_CALCS debug, "Server TBS:\n{}", bytes_as_hex_block(&tbs, ">"));
		cvmsg.signature.verify(&self.server_spki, &tbs, VERIFY_FLAG_NO_RSA_PKCS1).map_err(|_|
			TlsFailure::CvSignatureFailed)?;
		crypto_algs.set_signature(cvmsg.signature.algorithm);
		debug!(HANDSHAKE_EVENTS debug, "Server key exchange signature ({:?}) OK",
			cvmsg.signature.algorithm);

		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug)?;
		Ok(HandshakeState2::Tls13ServerFinished(Tls13ServerFinishedFields{
			bypass_certificate_validation: self.bypass_certificate_validation,
			sig_verified: true,
			requested_certificate: self.requested_certificate,
			client_hs_write: self.client_hs_write,
			server_hs_write: self.server_hs_write,
			master_secret: self.master_secret,
			hs_hash: hs_hash,
		}))
	}
}

impl Tls12CertificateFields
{
	pub fn rx_certificate<'a,'b>(self, certmsg: Tls12Certificate<'a>, names: &mut NamesContainer,
		debug: Debugger, config: &ClientConfiguration, raw_msg: HandshakeMessage<'b>) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		hs_hash.add(raw_msg, debug.clone())?;

		let mut staple_ee_cert = Vec::new();
		let mut staple_issuer_key = Vec::new();
		let mut dont_validate_ocsp = false;
		let mut features = self.features;
		let mut pins = self.pins;
		let mut server_spki = Vec::new();
		let mut require_ocsp_unsat = (config.flags[0] & FLAGS0_REQUIRE_OCSP) != 0;
		let mut require_sct_unsat = (config.flags[0] & FLAGS0_REQUIRE_CT) != 0;
		let mut staple_scts = self.staple_scts;
		let mut staple_issuer = 0..0;
		let mut staple_serial = 0..0;
		//Extract the EE certificate. This has to be present.
		fail_if!(certmsg.chain.len() == 0, TlsFailure::ServerChainEmpty);
		let eecert = certmsg.chain[0];
		fail_if!(eecert.len() == 0, TlsFailure::EeCertificateEmpty);
		//Read intermediate certs. There can be any amount of these (including 0). There is at least one
		//certificate, so slice is valid.
		let icerts = &certmsg.chain[1..];

		//Hack: Since we don't know the OCSP status yet, set the feature flag just if we have maybe send
		//OCSP. We check later if OCSP was actually sent.
		//Do that also unconditionally for SCT.
		if self.maybe_send_status { features |= CFLAG_MUST_STAPLE; }
		features |= CFLAG_MUST_CT;
		let eecert_parsed = do_validate_server_cert_chain(eecert, icerts, names, debug.clone(),
			DoValidateServerCertChainParams{dont_validate_ocsp: &mut dont_validate_ocsp,
			bypass_certificate_validation: self.bypass_certificate_validation, requested_sni:
			&self.requested_sni, features: features, use_spki_only: self.use_spki_only, pins: &mut pins,
			server_spki: &mut server_spki, killist_empty: self.killist_empty, require_ocsp_unsat:
			&mut require_ocsp_unsat, require_sct_unsat: &mut require_sct_unsat}, config)?;

		//Determine if shortlived.
		let short_lived_cert = if let &Some(ref ep) = &eecert_parsed {
			ep.not_before.delta(ep.not_after) <= config.ocsp_maxvalid
		} else {
			false
		};

		if self.bypass_certificate_validation {
			return Ok(HandshakeState2::Tls12ServerKeyExchange(self.maybe_send_status,
				Tls12ServerKeyExchangeFields {
				tls13_shares: self.tls13_shares,
				server_spki: server_spki,
				staple_scts: Vec::new(),			//No SCTs to validate.
				staple_ee_cert: Vec::new(),			//Nothing to validate.
				staple_issuer_key: Vec::new(),			//Nothing to validate.
				staple_issuer: 0..0,				//Nothing to validate.
				staple_serial: 0..0,				//Nothing to validate.
				dont_validate_ocsp: true,			//Never validate.
				use_spki_only: true,				//Can Assume this.
				require_ocsp_unsat: false,			//Don't require OCSP.
				bypass_certificate_validation: true,		//Bypassed.
				require_sct_unsat: false,			//Don't require SCT.
				client_random: self.client_random,
				server_random: self.server_random,
				hs_hash: hs_hash,
				ems_enabled: self.ems_enabled,
				protection: self.protection,
				prf: self.prf,
				short_lived_cert: false,			//Normal processing here.
			}));
		}

		//Save the receceived SCTs, and OCSP params.
		if let &Some(ref pcert) = &eecert_parsed {
			staple_issuer = subslice_to_range(eecert, pcert.issuer.0).map_err(|_|
				assert_failure!("Certificate issuer not from certificate???"))?;
			staple_serial = subslice_to_range(eecert, pcert.serial_number).map_err(|_|
				assert_failure!("Certificate serial not from certificate???"))?;
			for i in pcert.scts.iter() {
				debug!(HANDSHAKE_EVENTS debug, "Received SCT#{} using certificate.",
					staple_scts.len());
				staple_scts.push((true, (*i).to_owned()));
			}
		}

		//Save info for OCSP validation if sent. Also do this if we might need to validate SCTs.
		if self.maybe_send_status || staple_scts.len() > 0 {
			//Save EE cert and its issuer.
			staple_ee_cert = eecert.to_owned();
			let issuer_key = grab_issuer_key!(icerts, eecert_parsed.map(|x|x.issuer),
				config.trust_anchors);
			if let Some(isskey) = issuer_key { staple_issuer_key = isskey.to_owned(); }
		}

		//If dont_validate_ocsp is set, we never require SCT or OCSP.
		require_ocsp_unsat &= !dont_validate_ocsp;
		require_sct_unsat &= !dont_validate_ocsp;

		Ok(HandshakeState2::Tls12ServerKeyExchange(self.maybe_send_status, Tls12ServerKeyExchangeFields {
			tls13_shares: self.tls13_shares,
			server_spki: server_spki,
			staple_scts: staple_scts,
			staple_ee_cert: staple_ee_cert,
			staple_issuer_key: staple_issuer_key,
			staple_issuer: staple_issuer,
			staple_serial: staple_serial,
			dont_validate_ocsp: dont_validate_ocsp,
			use_spki_only: self.use_spki_only,
			bypass_certificate_validation: self.bypass_certificate_validation,
			require_ocsp_unsat: require_ocsp_unsat,
			require_sct_unsat: require_sct_unsat,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
			ems_enabled: self.ems_enabled,
			protection: self.protection,
			prf: self.prf,
			short_lived_cert: short_lived_cert,
		}))
	}
}
