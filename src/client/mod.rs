use ::{AbortReason, RxAppdataHandler, Session, TlsConnectionInfo, TxIrqHandler};
use ::certificate::TrustAnchorValue;
use ::client_certificates::{HostSpecificPin, LogHashFunction, TrustAnchor, TrustedLog};
use ::common::{builtin_killist, TlsFailure};
use ::features::{ALLOW_BAD_CRYPTO_NAME, CONFIG_PAGES, ConfigFlagsError, ConfigFlagsErrorKind, ConfigFlagsValue,
	ConfigFlagsEntry, DEFAULT_FLAGS, lookup_flag, set_combine_flags_set, set_combine_flags_clear,
	split_config_str};
use ::logging::Logging;
use ::record::{ControlHandler, SessionBase, SessionController};
use ::stdlib::{Arc, Box, BTreeMap, Cow, Deref, DerefMut, FromStr, IoError, IoErrorKind, IoRead, IoResult, IoWrite,
	Mutex, String, ToOwned, Vec};
use ::system::{File, Path};

use btls_aux_keyconvert::{Base64Decoder, decode_pem, looks_like_pem};
use btls_aux_serialization::{ASN1_SEQUENCE, LengthTrait, Sink, Source};

mod controller;
use self::controller::HandshakeController;

//Write a TLS vector of specified length of length, call callback to populate it, returning an error if length limit
//is exceeded
fn write_sub_fn<S:Sink,F,L:LengthTrait>(to: &mut S, name: &str, marker: L, mut cb: F) -> Result<(), TlsFailure>
	where F: FnMut(&mut S) -> Result<(), TlsFailure>
{
	let mut result = Ok(());
	to.vector_fn(marker, |ctx|{
		result = cb(ctx);
		if result.is_err() { Err(()) } else { Ok(()) }
	}).map_err(|_|assert_failure!("Element {} too big to send", name))?;
	result
}

///Client configuration.
///
///This structure contains various parts of the client configuration.
///
///# Setting trust anchors and creating configuration object (required).
///
///You can set up trust anchors and create the configuration like:
///
///```no-run
///let mut talist = Vec::new();
///talist.push(TrustAnchor::from_certificate_file("ca1.crt")?);
///talist.push(TrustAnchor::from_certificate_file("ca2.crt")?);
///let mut config = ClientConfiguration::from(&talist[..]);
///```
///
///If you want to "burn" the key into executable image:
///
///```no-run
///let mut talist = Vec::new();
///let cert = include_bytes!("../certificate/test3.der");
///talist.push(TrustAnchor::from_certificate(&mut &cert[..])?);
///let mut config = ClientConfiguration::from(&talist[..]);
///```
///
///
///Alternatively, if you are using host-specific pinning with trust anchors, you can also use it as follows:
///
///```no-run
///let mut config = ClientConfiguration::new();
///```
///
///This is handy if you are e.g. implementing DANE (RFC6698) validation (but don't use it for HPKP (RFC7469), see
///below).
///
///# Setting supported ALPNs (optional):
///
///You can set up supported ALPNs like follows:
///
///```no-run
///config.add_alpn("h2");
///config.add_alpn("http/1.1");
///```
///
///# Setting require EMS (optional):
///
///You can set EMS to be required as follows:
///
///```no-run
///config.set_flags(0, FLAGS0_REQUIRE_EMS);
///```
///
///This is useful if you are going to use exporters, since those won't work without EMS.
///
///Note: Negotiating TLS 1.3 is taken as sufficient to meet the EMS requirement.
///
///# Setting TLS version range (optional):
///
///Enabling TLS 1.3 support:
///
///```no-run
///config.set_flags(0, FLAGS0_ENABLE_TLS13);
///```
///
///Requiring TLS 1.3
///
///```no-run
///config.set_flags(0, FLAGS0_ENABLE_TLS13);
///config.clear_flags(0, FLAGS0_ENABLE_TLS12);
///```
///
///# Certificate Transparency (optional):
///
///Adding a log:
///
///```no-run
///let ct_log = TrustedLog{...};
///config.add_ct_log(&ct_log);
///```
///
///Requiring Certificate Transparency.
///
///```no-run
///config.set_flags(0, FLAGS0_REQUIRE_CT);
///```
///
///# Making session
///
///When you have it all set up, you can get client session using `.make_session()`.
///
///```no-run
///let mut session = config.make_session(hostname);
///```
///
///Or if you want to specify host-specific pins (mandatory if you used `.new()`):
///
///```no-run
///const SHAHASH1: [u8; 32] = [66;32/*...*/];
///const SHAHASH2: [u8; 32] = [77;32/*...*/];
///let pin1 = HostSpecificPin::trust_server_key_by_sha256(&SHAHASH1, false, true);
///let pin2 = HostSpecificPin::trust_server_key_by_sha256(&SHAHASH2, false, true);
///let pins = [pin1, pin2];
///let mut session = config.make_session_pinned(hostname, &pins[..]);
///```
///
///This is useful for implementing DANE (RFC6698) or HPKP (RFC7469). Or if you want to "just test" without "valid"
///CA certificate.
#[derive(Clone)]
pub struct ClientConfiguration
{
	//Trust anchor set.
	trust_anchors: BTreeMap<Vec<u8>, TrustAnchorValue>,
	//Supported ALPN's, in order of decreasing perference.
	supported_alpn: Vec<String>,
	//Flags pages.
	flags: [u64; CONFIG_PAGES],
	//Logging config.
	log: Option<(u64, Arc<Box<Logging+Send+Sync>>)>,
	//Certificate Killist.
	killist: BTreeMap<[u8; 32], ()>,
	//Allow SPKI mode.
	no_server_rpk: bool,
	//Trusted logs.
	trusted_logs: Vec<TrustedLog>,
	//OCSP maximum validitiy.
	ocsp_maxvalid: u64,
}

fn convert_trust_anchors(list: &[TrustAnchor]) -> BTreeMap<Vec<u8>, TrustAnchorValue>
{
	let mut ret = BTreeMap::new();
	for i in list {
		let (name, content) = i.to_internal_form();
		ret.insert(name, content);
	}
	ret
}


impl<'a> From<&'a [TrustAnchor]> for ClientConfiguration
{
	fn from(anchors: &'a [TrustAnchor]) -> ClientConfiguration
	{
		ClientConfiguration {
			trust_anchors: convert_trust_anchors(anchors),
			supported_alpn: Vec::new(),
			flags: DEFAULT_FLAGS,
			log: None,
			killist: builtin_killist(),
			no_server_rpk: false,
			trusted_logs: Vec::new(),
			ocsp_maxvalid: 7 * 60 * 60 * 24,	//7 days.
		}
	}
}

impl ClientConfiguration
{
	///Return a default client configuration with empty trust anchor list.
	///
	///Note that in order to successfully create a session out of this, you need to use `.make_session_pinned()`
	///specifying at least one pin with TA flag set (`trust_server_key_by_sha256` and `from_tlsa_raw` (when
	///first byte is 2 or 3) constructors both set the TA flag).
	pub fn new() -> ClientConfiguration
	{
		ClientConfiguration {
			trust_anchors: BTreeMap::new(),
			supported_alpn: Vec::new(),
			flags: DEFAULT_FLAGS,
			log: None,
			killist: builtin_killist(),
			no_server_rpk: false,
			trusted_logs: Vec::new(),
			ocsp_maxvalid: 7 * 60 * 60 * 24,	//7 days.
		}
	}
	///Add a supported ALPN `alpn` to client configuration.
	///
	///Note that the ALPNs are given in order of decreasing preference, if multiple are specified, the order
	///given is the order the values are sent in.
	pub fn add_alpn(&mut self, alpn: &str)
	{
		self.supported_alpn.push(alpn.to_owned());
	}
	///Set the configuration flags on page `page` that are set in mask `mask`.
	///
	///The `mask` should be bitwise or of various `FLAGS*` values.
	///
	///For example, setting `page` to `0` and `mask` to `FLAGS0_REQUIRE_EMS | FLAGS0_ASSUME_HW_AES` turns on
	///the `require Extended Master Secret` and `assume hardware AES support` flags.
	pub fn set_flags(&mut self, page: usize, mask: u64)
	{
		if page < CONFIG_PAGES { self.flags[page] = set_combine_flags_set(page, mask, self.flags[page]); }
	}
	///Clear the configuration flags on page `page` that are clear in mask `mask`.
	///
	///The `mask` should be bitwise or of various `FLAGS*` values.
	///
	///For example, setting `page` to `0` and `mask` to `FLAGS0_REQUIRE_EMS | FLAGS0_ASSUME_HW_AES` turns off
	///the `require Extended Master Secret` and `assume hardware AES support` flags.
	pub fn clear_flags(&mut self, page: usize, mask: u64)
	{
		if page < CONFIG_PAGES { self.flags[page] = set_combine_flags_clear(page, mask, self.flags[page]); }
	}
	//Handle blacklist entry.
	fn handle_blacklist<F>(&mut self, value: &str, mut error_cb: &mut F, entry: ConfigFlagsEntry)
		where F: FnMut(ConfigFlagsError)
	{
		let mut tmp = [0u8; 32];
		let mut valid = true;
		valid &= value.len() == 64;
		for i in value.chars().enumerate() {
			let val = match i.1 {
				'0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4,
				'5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9,
				'a' => 10, 'b' => 11, 'c' => 12, 'd' => 13, 'e' => 14, 'f' => 15,
				'A' => 10, 'B' => 11, 'C' => 12, 'D' => 13, 'E' => 14, 'F' => 15,
				_ => { valid = false; 0 }
			} as u8;
			if let Some(ptr) = tmp.get_mut(i.0>>1) {
				*ptr |= val << (4 - 4 * (i.0 & 1));
			}
		}
		if valid {
			self.killist.insert(tmp, ());
		} else {
			error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(
				"The arguemnt needs to be 64 hexdigits")})
		}
	}
	//Handle trust anchor entry.
	fn handle_trustanchor<F>(&mut self, value: &str, mut error_cb: &mut F, entry: ConfigFlagsEntry)
		where F: FnMut(ConfigFlagsError)
	{
		let p: &Path = value.as_ref();
		let mut f = match File::open(p) {
			Ok(x) => x,
			Err(x) => return error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(
				&format!("{}", x))})
		};
		let mut content = Vec::new();
		match f.read_to_end(&mut content) {
			Ok(x) => x,
			Err(x) => return error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(
				&format!("{}", x))})
		};
		let data: Cow<[u8]> = if looks_like_pem(&content, " CERTIFICATE") {
			Cow::Owned(match decode_pem(&content, " CERTIFICATE", true) {
				Ok(x) => x,
				Err(x) => return error_cb(ConfigFlagsError{entry:entry,
					kind:ConfigFlagsErrorKind::Error(&format!("Internal error: Can't decode \
					PEM?: {}", x))})
			})
		} else {
			Cow::Borrowed(&content)
		};
		let mut source = Source::new(&data);
		while !source.at_end() {
			let marker = source.slice_marker();
			match source.read_asn1_value(ASN1_SEQUENCE, |x|x) {
				Ok(_) => (),
				Err(x) => return error_cb(ConfigFlagsError{entry:entry,
					kind:ConfigFlagsErrorKind::Error(&format!("Parse error: {}", x))})
			};
			let mut data = marker.commit(&mut source);
			let t = match TrustAnchor::from_certificate(&mut data) {
				Ok(x) => x,
				Err(x) => { error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(
					&format!("Can't read cert: {}", x))}); continue }
			};
			let (name, content) = t.to_internal_form();
			self.trust_anchors.insert(name, content);
		}
	}
	//Handle ctlog entry.
	fn handle_ctlog<F>(&mut self, value: &str, mut error_cb: &mut F, entry: ConfigFlagsEntry)
		where F: FnMut(ConfigFlagsError)
	{
		let p: &Path = value.as_ref();
		let mut f = match File::open(p) {
			Ok(x) => x,
			Err(x) => return error_cb(ConfigFlagsError{entry:entry,
				kind:ConfigFlagsErrorKind::Error(&format!("{}", x))})
		};
		let mut content = String::new();
		match f.read_to_string(&mut content) {
			Ok(x) => x,
			Err(x) => return error_cb(ConfigFlagsError{entry:entry,
				kind:ConfigFlagsErrorKind::Error(&format!("{}", x))})
		};
		for line in content.split('\n').enumerate() {
			if line.1.len() == 0 { continue; }
			let version = line.1.split(':').next();
			let (key, id, hash, expiry, name) = if version == Some("v1") {
				let mut itr = line.1.splitn(4, ':');
				let _ = itr.next();
				let key = itr.next();
				let expiry = itr.next();
				let name = itr.next();
				(key, None, None, expiry, name)
			} else if version == Some("v2") {
				let mut itr = line.1.splitn(6, ':');
				let _ = itr.next();
				let id = itr.next();
				let hash = itr.next();
				let key = itr.next();
				let expiry = itr.next();
				let name = itr.next();
				(key, id, hash, expiry, name)
			} else {
				error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(&format!(
					"Unknown log version on line {}", line.0))});
				continue;
			};
			//Key, expiry and name must be present.
			let (key, expiry, name) = if let (Some(x), Some(y), Some(z)) = (key, expiry, name) {
				let mut _key = Vec::new();
				let mut dec = Base64Decoder::new();
				match dec.data(x, &mut _key).and_then(|_|dec.end(&mut _key)) {
					Ok(_) => (),
					Err(_) => {
						error_cb(ConfigFlagsError{entry:entry,
							kind:ConfigFlagsErrorKind::Error(&format!(
							"Bad key on line {}", line.0))});
						continue;
					}
				};
				let _expiry = if y == "" {
					None
				} else {
					Some(match i64::from_str(y) {
						Ok(x) => x,
						Err(_) => {
							error_cb(ConfigFlagsError{entry:entry,
								kind:ConfigFlagsErrorKind::Error(&format!(
								"Bad expiry on line {}", line.0))});
							continue;
						}
					})
				};
				(_key, _expiry, z)
			} else {
				error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(&format!(
					"Parse error on line {}", line.0))});
				continue;
			};
			//If id is present, hash needs to be too.
			let (id, hash) = if let (Some(id), Some(hash)) = (id, hash) {
				let _hash = if hash == "sha256" {
					LogHashFunction::Sha256
				} else {
					error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(
						&format!("Unknown hash function on line {}", line.0))});
					continue;
				};
				let mut dec = Base64Decoder::new();
				let mut _id = Vec::new();
				match dec.data(id, &mut _id).and_then(|_|dec.end(&mut _id)) {
					Ok(_) => (),
					Err(_) => {
						error_cb(ConfigFlagsError{entry:entry,
							kind:ConfigFlagsErrorKind::Error(&format!(
							"Bad log ID on line {}", line.0))});
						continue;
					}
				};
				(_id, _hash)
			} else if let (None, None) = (id, hash) {
				(Vec::new(), LogHashFunction::Sha256)
			} else {
				error_cb(ConfigFlagsError{entry:entry, kind:ConfigFlagsErrorKind::Error(&format!(
					"Parse error on line {}", line.0))});
				continue;
			};
			let log = TrustedLog {
				name: name.to_owned(),
				key: key,
				v2_id: id,
				v2_hash: hash,
				expiry: expiry
			};
			self.trusted_logs.push(log);
		}
	}
	//Handle ctlog entry.
	fn handle_ocsp_maxvalid<F>(&mut self, value: &str, mut error_cb: &mut F, entry: ConfigFlagsEntry)
		where F: FnMut(ConfigFlagsError)
	{
		self.ocsp_maxvalid = match u64::from_str(value) {
			Ok(x) => x,
			Err(_) => {
				error_cb(ConfigFlagsError{entry:entry,
					kind:ConfigFlagsErrorKind::Error(&format!(
					"Can't parse ocsp-maxvalid value as number"))});
				return;
			}
		};
	}
	///Set configuration string `config`, using `error_cb` as error callback.
	///
	///The configuration is specified as comma-separated string. Each component can be flag, optionally prefixed
	///with `+`, `-` or `!`. If flag is prefixed with `+` or is unprefixed, it is enabled. If flag is prefixed
	///with `-` or `!`, it is disabled.
	///
	///Also, components can be of form `name=value`, where specified setting `name` is set to value `value`.
	///
	///The following `name=value` pair names are known:
	///
	/// * `blacklist`: Blacklists the specific 64-byte SHA-256 hex digest of SubjectPublicKeyInfo. If the server
	///sends a blacklisted certificate, the connection will fail. This is intended to handle CA and server key
	///compromises.
	/// * `trustanchor`: Takes in a name of file in either DER or PEM format containg a certificate to add into
	///trust root list. In case the file is in PEM format, it may contain multiple certificates; in this case,
	///all certificates are added into trust root list.
	/// * `ctlog`: Takes in a name of file containing CT logs to to add to trusted log list. See below for format
	///of the file.
	/// * `ocsp-maxvalid`: Specifies OCSP maximum validity in seconds. Responses that live longer than this
	///are not considered valid for staple (triggering connection error if stapling is needed). Conversely,
	///certificates that live at most this don't need OCSP stapling, even if feature or flag requires it.
	///
	///# Format of ctlog files:
	///
	///ctlog files consists of one or more lines, with each line containing a trusted CT log. Each line has one
	///of two forms:
	///
	/// * v1:<key>:<expiry>:<name>
	/// * v2:<id>/<hash>:<key>:<expiry>:<name>
	///
	///Where:
	///
	/// * `<id>` is base64 encoding of DER encoding the log OID, without the DER tag or length.
	/// * `<hash>` is the log hash function. Currently only `sha256` is supported.
	/// * `<key>`is base64 encoding of log key formatted as X.509 SubjectPublicKeyInfo.
	/// * `<expiry>` is the time log expired in seconds since Unix epoch, or blank if log has not expired yet.
	///Certificate-stapled SCTs are accepted up to expiry date from expired logs, other kinds of SCTs are not
	///accepted.
	/// * `<name>` is the name of the log.
	pub fn config_flags<F>(&mut self, config: &str, mut error_cb: F) where F: FnMut(ConfigFlagsError)
	{
		split_config_str(self, config, |objself, name, value, entry| {
			let argerr = if name == "blacklist" {
				if let &ConfigFlagsValue::Explicit(ref val) = &value {
					return objself.handle_blacklist(val, &mut error_cb, entry);
				} else {
					true
				}
			} else if name == "trustanchor" {
				if let &ConfigFlagsValue::Explicit(ref val) = &value {
					return objself.handle_trustanchor(val, &mut error_cb, entry);
				} else {
					true
				}
			} else if name == "ctlog" {
				if let &ConfigFlagsValue::Explicit(ref val) = &value {
					return objself.handle_ctlog(val, &mut error_cb, entry);
				} else {
					true
				}
			} else if name == "ocsp-maxvalid" {
				if let &ConfigFlagsValue::Explicit(ref val) = &value {
					return objself.handle_ocsp_maxvalid(val, &mut error_cb, entry);
				} else {
					true
				}
			} else {
				false
			};
			if argerr {
				error_cb(ConfigFlagsError{entry:entry,
					kind:ConfigFlagsErrorKind::ArgumentRequired(name)});
				return;
			}
			if let Some((page, mask)) = lookup_flag(name) {
				match value {
					ConfigFlagsValue::Disabled => objself.clear_flags(page, mask),
					ConfigFlagsValue::Enabled if name == ALLOW_BAD_CRYPTO_NAME =>
						//Allow-bad-crypto is not supported clientside.
						error_cb(ConfigFlagsError{entry:entry,
							kind:ConfigFlagsErrorKind::NoEffect(name)}),
					ConfigFlagsValue::Enabled => objself.set_flags(page, mask),
					ConfigFlagsValue::Explicit(_) => error_cb(ConfigFlagsError{entry:entry,
						kind:ConfigFlagsErrorKind::NoArgument(name)})
				}
			} else {
				error_cb(ConfigFlagsError{entry:entry,
					kind:ConfigFlagsErrorKind::UnrecognizedSetting(name)
				});
			}
		});
	}
	///Set the debugging callback to `log` and debugging mask to `mask`.
	///
	///The `mask` should be bitwise or of various `DEBUG_*` values.
	///
	///The `.message()` method of debugging callback is called if event that has its bitflag set in `mask`
	///occurs.
	pub fn set_debug(&mut self, mask: u64, log: Box<Logging+Send+Sync>)
	{
		self.log = Some((mask, Arc::new(log)));
	}
	///Add a trusted certificate transparency log `log`.
	pub fn add_ct_log(&mut self, log: &TrustedLog)
	{
		self.trusted_logs.push(log.clone());
	}
	///Add certificate blacklist entry for SubjectPublicKeyInfo SHA-256 hash `spkihash` (this is always 32
	///bytes).
	///
	///If blacklisted certificate key hash is encountered in handshake, the handshake will fail.
	pub fn blacklist(&mut self, spkihash: &[u8])
	{
		let mut x = [0; 32];
		if spkihash.len() != x.len() { return; }
		x.copy_from_slice(spkihash);
		self.killist.insert(x, ());
	}
	///Disallow using raw SPKI for server authentication.
	///
	///When using raw SPKI is disallowed, the client won't advertise support for raw public keys, even if it has
	///trust anchor pins that match server SPKIs (which would normally cause advertiment for RPKs).
	///
	///This is unlikely to be needed, unless the application is a browser, since most protocols can handle
	///server receiving misdirected requests without an issue.
	///
	///Note that all pins set continue to be valid.
	pub fn disallow_server_rpk(&mut self)
	{
		self.no_server_rpk = true;
	}
	///Make a new client session to host with name `host` (this name is sent as SNI).
	///
	///This in practicular fails if there are no items in trusted root certificate list.
	pub fn make_session(&self, host: &str) -> Result<ClientSession, TlsFailure>
	{
		fail_if!(self.trust_anchors.len() == 0, TlsFailure::NoTrustAnchors);
		let controller = HandshakeController::new(&self, host, self.log.clone());
		Ok(self.make_session_tail(controller))
	}
	///Make a new client session to host with name `host`, using host-specific pins `pins`.
	///
	///This is useful for implementing DANE (RFC6698) and HPKP (RFC7469)
	///
	///This in practicular fails if there are no items in trusted root certificate list, nor does pins contain
	///any trust anchors.
	pub fn make_session_pinned(&self, host: &str, pins: &[HostSpecificPin]) ->
		Result<ClientSession, TlsFailure>
	{
		let extra_ta_count = pins.iter().filter(|x|x.ta).count();
		//We want to check if the sum is bigger than 0.
		fail_if!(self.trust_anchors.len().saturating_add(extra_ta_count) == 0, TlsFailure::NoTrustAnchors);
		let controller = HandshakeController::new_pinned(&self, host, pins, self.log.clone());
		Ok(self.make_session_tail(controller))
	}
	///Make a client session WITH NO CHECKING WHATSOEVER.
	///
	///This makes a client session that does not check for certificates. It is meant for connections that
	///are authenticated via TLS-EXPORTER values.
	///
	///The following restrictions apply to these connections:
	///
	/// * SNI is disabled, and will not be sent.
	/// * RPK support indication is forcibly enabled.
	/// * The "EMS support required" is forcibly enabled (i.e. the server MUST support either EMS or TLS 1.3,
	///or the connection will fail).
	/// * get_server_names() never succeeds.
	///
	///# DANGER:
	///
	///Without additional verification via signing TLS-EXPORTER values, this is TRIVIALLY INSECURE. Hijacking
	///connections without verification is VERY EASY. If such connection is not hijacked, it is only because
	///nobody tried.
	pub fn make_session_insecure_nocheck(&self) -> Result<ClientSession, TlsFailure>
	{
		let controller = HandshakeController::new_insecure_nocheck(&self, self.log.clone());
		Ok(self.make_session_tail(controller))
	}
	fn make_session_tail(&self, controller: HandshakeController) -> ClientSession
	{
		let mut sess = _ClientSession {
			base: SessionBase::new(self.log.clone()),
			hs:ClientHandshake(controller)
		};
		//Since client goes first, immediately do the first TX cycle.
		sess.base.do_tx_request();
		//Ok.
		ClientSession(Arc::new(Mutex::new(sess)))
	}
}


struct ClientHandshake(HandshakeController);

impl ControlHandler for ClientHandshake
{
	fn handle_control(&mut self, rtype: u8, msg: &[u8], controller: &mut SessionController)
	{
		self.0.handle_control(rtype, msg, controller)
	}
	fn handle_tx_update(&mut self, controller: &mut SessionController)
	{
		self.0.handle_tx_update(controller)
	}
}

impl ClientHandshake
{
	fn borrow_inner(&self) -> &HandshakeController
	{
		&self.0
	}
	fn borrow_inner_mut(&mut self) -> &mut HandshakeController
	{
		&mut self.0
	}
}

//The internal state, send but pesumably not sync. Not cloneable.
struct _ClientSession
{
	base: SessionBase,
	hs: ClientHandshake,
}

///A client TLS session.
///
///This is the client end of TLS session.
///
///Use `.make_session()` or `.make_session_pinned()` of [`ClientConfiguration`] to create it.
///
///For operations supported, see trait [`Session`].
///
///If the session is cloned, both sessions refer to the same underlying TLS connection. This can be used to refer
///to the same session from multiple threads at once.
///
///[`ClientConfiguration`]: struct.ClientConfiguration.html
///[`Session`]: trait.Session.html
#[derive(Clone)]
pub struct ClientSession(Arc<Mutex<_ClientSession>>);

impl Session for ClientSession
{
	fn set_high_water_mark(&mut self, amount: usize)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_high_water_mark(amount),
			Err(_) => ()
		}
	}
	fn set_send_threshold(&mut self, threshold: usize)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_send_threshold(threshold),
			Err(_) => ()
		}
	}
	fn bytes_available(&self) -> usize
	{
		match self.0.lock() {
			Ok(ref x) => x.base.bytes_available(),
			Err(_) => 0
		}
	}
	fn bytes_queued(&self) -> usize
	{
		match self.0.lock() {
			Ok(ref x) => x.base.bytes_queued(),
			Err(_) => 0
		}
	}
	fn is_eof(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.is_eof(),
			Err(_) => true
		}
	}
	fn wants_read(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.wants_read(),
			Err(_) => false
		}
	}
	fn set_tx_irq(&mut self, handler: Box<TxIrqHandler+Send>)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_tx_irq(handler),
			Err(_) => ()
		}
	}
	fn set_appdata_rx_fn(&mut self, handler: Option<Box<RxAppdataHandler+Send>>)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_appdata_rx_fn(handler),
			Err(_) => ()
		}
	}
	fn read_tls<R:IoRead>(&mut self, stream: &mut R) -> IoResult<()>
	{
		//We can't read with lock held.
		const READ_BLOCKSIZE: usize = 16500;
		let mut buf = [0; READ_BLOCKSIZE];
		let rlen = stream.read(&mut buf)?;
		if rlen > buf.len() {
			return Err(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!("Stream \
				read of {} bytes into buffer of {}", rlen, buf.len()))));
		}
		let mut buf = &buf[..rlen];
		let res = match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.read_tls(&mut buf, &mut x.hs)
			},
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		};
		self.do_tls_tx_rx_cycle().map_err(|x|IoError::new(IoErrorKind::Other, x))?;
		res
	}
	fn submit_tls_record(&mut self, record: &mut [u8]) -> Result<(), TlsFailure>
	{
		let res = match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.submit_tls_record(record, &mut x.hs)
			},
			Err(_) => sanity_failed!("Can't lock TLS session")
		};
		self.do_tls_tx_rx_cycle()?;
		res
	}
	fn write_tls<W:IoWrite>(&mut self, stream: &mut W) -> IoResult<()>
	{
		//This can happen with waiting for signatures.
		self.do_tls_tx_rx_cycle().map_err(|x|IoError::new(IoErrorKind::Other, x))?;
		let ret = match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.write_tls(stream, &mut x.hs, false)
			}
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		};
		ret
	}
	fn zerolatency_write(&mut self, output: &mut [u8], input: &[u8]) -> Result<(usize, usize), TlsFailure>
	{
		//This can happen with waiting for signatures.
		self.do_tls_tx_rx_cycle()?;
		match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.zerolatency_write(&mut x.hs, output, input)
			},
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn estimate_zerolatency_size(&self, outsize: usize) -> usize
	{
		match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref();
				x.base.estimate_zerolatency_size(outsize)
			},
			Err(_) => 0
		}
	}
	fn can_tx_data(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.can_tx_data(),
			Err(_) => false
		}
	}
	fn handshake_completed(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.hs.borrow_inner().in_showtime(),
			Err(_) => false,	//Should not happen.
		}
	}
	fn send_eof(&mut self)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.send_eof(),
			Err(_) => ()
		}
	}
	fn aborted_by(&self) -> Option<AbortReason>
	{
		match self.0.lock() {
			Ok(ref x) => x.base.aborted_by(),
			Err(_) => Some(AbortReason::HandshakeError(TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
	fn extractor(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), TlsFailure>
	{
		match self.0.lock() {
			Ok(ref x) => x.base.extractor(label, context, buffer),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn get_alpn(&self) -> Option<Option<Arc<String>>>
	{
		match self.0.lock() {
			Ok(ref x) => x.hs.borrow_inner().get_alpn(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn get_server_names(&self) -> Option<Arc<Vec<String>>>
	{
		match self.0.lock() {
			Ok(ref x) => x.hs.borrow_inner().get_server_names(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn get_sni(&self) -> Option<Option<Arc<String>>>
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().get_sni(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn connection_info(&self) -> TlsConnectionInfo
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().connection_info(),
			Err(_) => TlsConnectionInfo {
				version: 0,
				ems_available: false,
				ciphersuite: 0,
				kex: None,
				signature: None,
				version_str: "",
				protection_str: "",
				hash_str: "",
				exchange_group_str: "",
				signature_str: "",
				validated_ct: false,
				validated_ocsp: false,
				validated_ocsp_shortlived: false,
			}
		}
	}
	fn get_record_size(&self, buffer: &[u8]) -> Option<usize>
	{
		match self.0.lock() {
			Ok(x) => x.base.get_record_size(buffer),
			Err(_) => Some(5)	//Shouldn't be here.
		}
	}
	fn begin_transacted_read<'a>(&self, buffer: &'a mut [u8]) -> Result<&'a [u8], TlsFailure>
	{
		match self.0.lock() {
			Ok(x) => x.base.begin_transacted_read(buffer),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn end_transacted_read(&mut self, len: usize) -> Result<(), TlsFailure>
	{
		match self.0.lock() {
			Ok(mut x) => x.base.end_transacted_read(len),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
}

impl IoRead for ClientSession
{
	fn read(&mut self, buf: &mut [u8]) -> IoResult<usize>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.read(buf),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
}

impl IoWrite for ClientSession
{
	fn write(&mut self, buf: &[u8]) -> IoResult<usize>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.write(buf),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
	fn flush(&mut self) -> IoResult<()>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.flush(),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
}

impl ClientSession
{
	fn do_tls_tx_rx_cycle(&mut self) -> Result<(),TlsFailure>
	{
		let mut inner = match self.0.lock() {
			Ok(x) => x,
			Err(_) => sanity_failed!("Can't lock TLS session")
		};
		let mut inner = inner.deref_mut();
		//Do TX cycles if needed.
		while inner.hs.borrow_inner().wants_tx() {
			if inner.hs.borrow_inner_mut().queue_hs_tx(&mut inner.base) {
				inner.base.finish_tx_handshake();
			}
		}
		//Re-arm the RX if needed. Note that previous TX cycle can have altered the condition.
		if inner.hs.borrow_inner().wants_rx() {
			inner.base.request_rx_handshake();
		}
		Ok(())
	}
}
