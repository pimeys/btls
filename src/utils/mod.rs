#![allow(missing_docs)]		//This stuff has fair amount of hidden re-exports for testing.

use ::stdlib::{Range, size_of};

//use btls_aux_serialization::*;
//use btls_aux_json::*;
//use btls_aux_sexp::*;
//use btls_aux_keyconvert::*;
//use btls_aux_securebuf::*;
//use btls_aux_time::*;

mod hsbuffer;
pub use self::hsbuffer::*;
mod queue;
pub use self::queue::*;

//Set this to never inline to prevent optimizations due to context, and allow it to be inspected.
#[inline(never)]
pub fn slice_eq_ct(a: &[u8], b: &[u8]) -> bool
{
	//TODO: This can be implemented more efficiently. This code compiles into byte loop.
	if a.len() != b.len() { return false; }
	let mut syndrome = 0;
	for i in a.iter().zip(b.iter()) {
		syndrome |= *i.0 ^ *i.1;
	}
	syndrome==0
}


///Return slice range of child relative to parent.
///
///# Parameters:
///
/// * `parent`: The parent slice.
/// * `child`: The child slice.
///
///# Returns:
///
/// * Range in `parent` that corresponds to `child`.
///
///# Failures:
///
/// * If `child` is not subslice of `parent`, fails with `()`.
/// * If `T` is a ZST, fails with `()`.
pub fn subslice_to_range<T:Sized>(parent: &[T], child: &[T]) -> Result<Range<usize>, ()>
{
	let elemsize = size_of::<T>();
	//The addresses of parent and child.
	let parent_addr = parent.as_ptr() as usize;
	let child_addr = child.as_ptr() as usize;
	//Compute offset of child in bytes.
	let child_offset = child_addr.checked_sub(parent_addr).ok_or(())?;
	//Check that the child address is aligned w.r.t. parent.
	fail_if!(child_offset.checked_rem(elemsize) != Some(0), ());
	//Compute starting element of child.
	let child_start = child_offset / elemsize;
	//Compute ending element of child.
	let child_end = child_start.checked_add(child.len()).ok_or(())?;
	//Check that the child is in range.
	fail_if!(child_end > parent.len(), ());
	//Ok.
	Ok(child_start..child_end)
}
