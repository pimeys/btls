use ::stdlib::{Display, FmtError, Formatter, min, swap, Vec};

pub struct HsBuffer
{
	buffer: Vec<u8>,
	ic_header: u32,
	hs_remain: u32,
	ic_header_len: u8,
	hs_type: u8,
}

const MAX_MSG_SIZE: u32 = 128*1024;

///Error in handshake buffer.
#[derive(Copy,Clone,Debug,PartialEq,Eq)]
pub enum HsBufferError
{
	///Handshake message too long.
	HandshakeMsgTooLong(u32, u32),
	#[doc(hidden)]
	Hidden__
}

impl HsBufferError
{
	///Get TLS alert code.
	pub fn tls_code(&self) -> u8
	{
		use self::HsBufferError::*;
		match self {
			&HandshakeMsgTooLong(_, _)  => tlserr!(UNEXPECTED_MESSAGE),
			&Hidden__ => tlserr!(INTERNAL_ERROR)
		}
	}
}

impl Display for HsBufferError
{
	fn fmt(&self, fmt: &mut Formatter) -> Result<(), FmtError>
	{
		use self::HsBufferError::*;
		match self {
			&HandshakeMsgTooLong(x, y) => fmt.write_fmt(format_args!("Handshake message too long ({} \
				bytes, limit is {})", x, y)),
			&Hidden__ => fmt.write_str("Hidden__"),
		}
	}
}

impl HsBuffer
{
	pub fn new() -> HsBuffer
	{
		HsBuffer {
			buffer: Vec::new(),
			ic_header: 0,
			hs_remain: 0,
			ic_header_len: 0,
			hs_type: 0
		}
	}
	//Check for idle.
	pub fn is_idle(&self) -> bool
	{
		self.buffer.is_empty()
	}
	//Extract message from record buffer. Returns rtype, contents and last-in-record flag on success.
	pub fn extract<'a>(&mut self, record: &mut &'a [u8]) ->
		Result<Option<(u8, Vec<u8>, bool)>, HsBufferError>
	{
		use self::HsBufferError::*;
		if self.hs_remain == 0 {
			//New message starts.
			while self.ic_header_len < 4 {
				if (*record).len() >= 1 {
					//The split point is in range, since length of record is at least 1.
					let (head, tail) = (*record).split_at(1);
					//The split was successful, so the first part has to have length of 1.
					let x = head[0] as u32;
					self.ic_header = self.ic_header << 8 | x;
					*record = tail;
					//This can't overflow, because by condition, ic_header_len is at most 3.
					self.ic_header_len = self.ic_header_len + 1;
				} else {
					return Ok(None)
				}
			}
			if self.ic_header_len < 4 { return Ok(None); }	//Incomplete header.
			self.hs_type = (self.ic_header >> 24) as u8;
			self.hs_remain = self.ic_header & 0xFFFFFF;
			self.ic_header_len = 0;
			self.ic_header = 0;
			fail_if!(self.hs_remain > MAX_MSG_SIZE, HandshakeMsgTooLong(self.hs_remain, MAX_MSG_SIZE));
			self.buffer.reserve(self.hs_remain as usize);
		}
		{
			let tocopy = min((*record).len(), self.hs_remain as usize);
			//tocopy is at most than (*record).len(), so the split will be in-bounds.
			let (head, tail) = (*record).split_at(tocopy);
			self.buffer.extend_from_slice(head);
			*record = tail;
			//tocopy, which equals head.len() is at most hs_remain, so this can't underflow.
			//And usize can fit any value that fits in u32.
			self.hs_remain -= head.len() as u32;
		}
		if self.hs_remain == 0 {
			let mut tmp = Vec::new();
			swap(&mut self.buffer, &mut tmp);
			Ok(Some((self.hs_type, tmp, (*record).len() == 0)))
		} else {
			Ok(None)
		}
	}
	//Is empty?
	pub fn empty(&self) -> bool
	{
		self.ic_header_len == 0 && self.hs_remain == 0
	}
}

#[test]
fn simple_record_extract()
{
	let data = [55, 0, 0, 4, 42, 53, 64, 11];
	let mut ex = HsBuffer::new();
	let mut datap = &data[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 55);
	assert_eq!(&hdata[..], &data[4..8]);
	assert_eq!(lflag, true);
	assert_eq!(datap, &[]);
	assert_eq!(ex.extract(&mut datap).unwrap().is_none(), true);
}

#[test]
fn record_extract_multi()
{
	let data = [55, 0, 0, 4, 42, 53, 64, 11, 52, 0, 0, 1, 3, 74, 0, 0, 0];
	let mut ex = HsBuffer::new();
	let mut datap = &data[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 55);
	assert_eq!(&hdata[..], &data[4..8]);
	assert_eq!(lflag, false);
	assert_eq!(datap.len(), 9);
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 52);
	assert_eq!(&hdata[..], &data[12..13]);
	assert_eq!(lflag, false);
	assert_eq!(datap.len(), 4);
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 74);
	assert_eq!(&hdata[..], &[]);
	assert_eq!(lflag, true);
	assert_eq!(datap.len(), 0);
	assert_eq!(ex.extract(&mut datap).unwrap().is_none(), true);
}

#[test]
fn record_extract_seq()
{
	let data1 = [55, 0, 0, 4, 42, 53, 64, 11];
	let data2 = [52, 0, 0, 1, 3];
	let mut ex = HsBuffer::new();
	let mut datap = &data1[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 55);
	assert_eq!(&hdata[..], &data1[4..8]);
	assert_eq!(lflag, true);
	assert_eq!(datap.len(), 0);
	let mut datap = &data2[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 52);
	assert_eq!(&hdata[..], &data2[4..5]);
	assert_eq!(lflag, true);
	assert_eq!(datap.len(), 0);
}

#[test]
fn record_extract_seq_splithdrdata()
{
	let data1 = [55, 0, 0, 4, 42, 53, 64, 11, 52, 0, 0, 1];
	let data2 = [3];
	let mut ex = HsBuffer::new();
	let mut datap = &data1[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 55);
	assert_eq!(&hdata[..], &data1[4..8]);
	assert_eq!(lflag, false);
	assert_eq!(datap.len(), 4);
	assert_eq!(ex.extract(&mut datap).unwrap().is_none(), true);
	assert_eq!(datap.len(), 0);
	let mut datap = &data2[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 52);
	assert_eq!(&hdata[..], &data2[0..1]);
	assert_eq!(lflag, true);
	assert_eq!(datap.len(), 0);
}

#[test]
fn record_extract_seq_splithdr()
{
	let data1 = [55, 0, 0, 4, 42, 53, 64, 11, 52, 0, 0];
	let data2 = [1, 3];
	let mut ex = HsBuffer::new();
	let mut datap = &data1[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 55);
	assert_eq!(&hdata[..], &data1[4..8]);
	assert_eq!(lflag, false);
	assert_eq!(datap.len(), 3);
	assert_eq!(ex.extract(&mut datap).unwrap().is_none(), true);
	assert_eq!(datap.len(), 0);
	let mut datap = &data2[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 52);
	assert_eq!(&hdata[..], &data2[1..2]);
	assert_eq!(lflag, true);
	assert_eq!(datap.len(), 0);
}

#[test]
fn record_extract_seq_splitdata()
{
	let data1 = [55, 0, 0, 4, 42, 53, 64, 11, 52, 0, 0, 2, 3];
	let data2 = [4];
	let mut ex = HsBuffer::new();
	let mut datap = &data1[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 55);
	assert_eq!(&hdata[..], &data1[4..8]);
	assert_eq!(lflag, false);
	assert_eq!(datap.len(), 5);
	assert_eq!(ex.extract(&mut datap).unwrap().is_none(), true);
	assert_eq!(datap.len(), 0);
	let mut datap = &data2[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 52);
	assert_eq!(&hdata[..], &[3, 4]);
	assert_eq!(lflag, true);
	assert_eq!(datap.len(), 0);
}

#[test]
fn record_extract_seq_splithdr_bad()
{
	let data1 = [55, 0, 0, 4, 42, 53, 64, 11, 52];
	let data2 = [0, 0];
	let data3 = [1, 3];
	let mut ex = HsBuffer::new();
	let mut datap = &data1[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 55);
	assert_eq!(&hdata[..], &data1[4..8]);
	assert_eq!(lflag, false);
	assert_eq!(datap.len(), 1);
	assert_eq!(ex.extract(&mut datap).unwrap().is_none(), true);
	let mut datap = &data2[..];
	assert_eq!(ex.extract(&mut datap).unwrap().is_none(), true);
	assert_eq!(datap.len(), 0);
	let mut datap = &data3[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 52);
	assert_eq!(&hdata[..], &data3[1..2]);
	assert_eq!(lflag, true);
	assert_eq!(datap.len(), 0);
}

#[test]
fn record_extract_large()
{
	let mut data = [0; 66056];
	data[0] = 55;
	data[1] = 1;
	data[2] = 2;
	data[3] = 4;
	let mut ex = HsBuffer::new();
	let mut datap = &data[..];
	let (htype, hdata, lflag) = ex.extract(&mut datap).unwrap().unwrap();
	assert_eq!(htype, 55);
	assert_eq!(hdata.len(), 66052);
	assert_eq!(lflag, true);
	assert_eq!(datap.len(), 0);
}
