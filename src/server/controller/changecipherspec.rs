use super::HandshakeState2;
use ::common::{Debugger, HandshakeHash, TlsFailure};
use ::record::{SessionBase, SessionController, Tls12MasterSecret, Tls12PremasterSecret};
use server::controller::finished::{Tls12ClientFinishedFields, Tls12ServerFinishedFields};

pub struct Tls12ClientChangeCipherSpecFields
{
	pub kill_connection_after_handshake: bool,
	pub premaster_secret: Tls12PremasterSecret,	//Premaster secret.
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
}

pub struct Tls12ServerChangeCipherSpecFields
{
	pub kill_connection_after_handshake: bool,
	pub master_secret: Tls12MasterSecret,	//The master secret.
	pub hs_hash: HandshakeHash,
}

impl Tls12ClientChangeCipherSpecFields
{
	pub fn rx_change_cipher_spec(self, controller: &mut SessionController, debug: Debugger) ->
		Result<HandshakeState2, TlsFailure>
	{
		//This is not true handshake message, so not added to handshake hash.
		let master_secret = Tls12MasterSecret::new(self.premaster_secret, &self.client_random,
			&self.server_random, self.hs_hash.checkpoint()?, debug.clone())?;
		controller.set_deprotector(master_secret.get_deprotector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Received ChangeCipherSpec, downstream \
			encryption enabled");
		Ok(HandshakeState2::Tls12ClientFinished(Tls12ClientFinishedFields{
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			master_secret: master_secret,
			hs_hash: self.hs_hash,
		}))
	}
}

impl Tls12ServerChangeCipherSpecFields
{
	pub fn tx_ccs(self, base: &mut SessionBase, debug: Debugger) -> Result<HandshakeState2, TlsFailure>
	{
		base.send_message_fragment(20, &[1]);
		base.change_protector(self.master_secret.get_protector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Sent ChangeCipherSpec, enabling upstream \
			encryption");
		Ok(HandshakeState2::Tls12ServerFinished(Tls12ServerFinishedFields{
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			master_secret: self.master_secret,
			hs_hash: self.hs_hash,
		}))
	}
}
