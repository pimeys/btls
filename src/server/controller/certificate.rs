use super::{HandshakeState2, tx_sct_ext_common, write_ext_fn, write_sub_fn};
use super::finished::Tls13ServerFinishedFields;
use super::serverkeyexchange::Tls12ServerKeyExchange1Fields;
use ::certificate::{CertificateIssuer, CFLAG_MUST_CT, CFLAG_MUST_EMS, CFLAG_MUST_RENEGO, CFLAG_MUST_STAPLE,
	CFLAG_MUST_TLS13, extract_spki, name_in_set2, OcspDumpedScts, OcspSanityCheckResults,
	sanity_check_certificate_chain};
use ::common::{CERTTYPE_RPK, CryptoTracker, Debugger, emit_handshake, EXT_SERVER_CERTIFICATE_TYPE,
	EXT_STATUS_REQUEST, format_tbs_tls13, HandshakeHash, HSTYPE_CERTIFICATE, HSTYPE_CERTIFICATE_STATUS,
	HSTYPE_CERTIFICATE_VERIFY, NamesContainer, OCSP_SINGLE, TLS_LEN_CERT_CONTEXT, TLS_LEN_CERT_ENTRY,
	TLS_LEN_CERT_LIST, TLS_LEN_EXTENSIONS, TLS_LEN_OCSP_RESPONSE, TLS_LEN_SIGNATURE, TlsFailure};
use ::record::{SessionBase, Tls13MasterSecret, Tls13TrafficSecretIn, Tls13TrafficSecretOut};
use ::features::FLAGS0_ENABLE_TLS13;
use ::logging::bytes_as_hex_block;
use ::server::{SaneCertVal, SaneOcspVal, ServerConfiguration};
use ::server_certificates::CertificateSigner;
use ::stdlib::{Arc, Box, Deref, from_utf8, String, ToOwned, Vec};

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::DiffieHellmanKey;
use btls_aux_hash::HashFunction;
use btls_aux_futures::FutureReceiver;
use btls_aux_serialization::Sink;
use btls_aux_signature_algo::SignatureType;
use btls_aux_time::{TimeInterface, TimeNow};

pub struct Tls12CertificateFields
{
	pub certificate: Box<CertificateSigner+Send>,
	pub key_share: Box<DiffieHellmanKey+Send>,
	pub send_spki_only: bool,
	pub sni_name: Option<String>,
	pub no_check_certificate: bool,
	pub ocsp_v1_offered: bool,
	pub ct_offered: bool,
	pub maybe_send_ocsp: bool,
	pub kill_connection_after_handshake: bool,
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
	pub ems_enabled: bool,
	pub protection: ProtectorType,
	pub prf: HashFunction,
}

pub struct Tls13CertificateFields
{
	pub server_hs_write: Tls13TrafficSecretOut,
	pub client_hs_write: Tls13TrafficSecretIn,
	pub certificate: Box<CertificateSigner+Send>,
	pub send_spki_only: bool,
	pub sni_name: Option<String>,
	pub no_check_certificate: bool,
	pub ocsp_v1_offered: bool,
	pub ct_offered: bool,
	pub kill_connection_after_handshake: bool,
	pub master_secret: Tls13MasterSecret,
	pub hs_hash: HandshakeHash,
}

pub struct Tls13CertificateVerify1Fields
{
	pub server_hs_write: Tls13TrafficSecretOut,
	pub client_hs_write: Tls13TrafficSecretIn,
	pub certificate: Box<CertificateSigner+Send>,
	pub kill_connection_after_handshake: bool,
	pub master_secret: Tls13MasterSecret,
	pub hs_hash: HandshakeHash,
}

pub struct Tls13CertificateVerify2Fields
{
	server_hs_write: Tls13TrafficSecretOut,
	client_hs_write: Tls13TrafficSecretIn,
	pub signature_future: FutureReceiver<Result<(u16, Vec<u8>),()>>,
	kill_connection_after_handshake: bool,
	master_secret: Tls13MasterSecret,
	hs_hash: HandshakeHash,
}

fn tx_ocsp_ext_tls13_common<S:Sink>(x: &mut S, certificate: &Box<CertificateSigner+Send>,
	debugger: Debugger, crypto_algs: &mut CryptoTracker) -> Result<(), TlsFailure>
{
	let ocsp = certificate.get_ocsp();
	let ocsp = if let Some(ref x) = ocsp.0 { x } else { return Ok(()); };

	sanity_check!(ocsp.len() > 0, "Certificate has empty OCSP staple data");
	debug!(HANDSHAKE_EVENTS debugger, "Sent OCSP staple");
	crypto_algs.ocsp_validated();
	write_ext_fn(x, EXT_STATUS_REQUEST, "status_reqeust", |x|{
		try_buffer!(x.write_u8(OCSP_SINGLE));
		write_sub_fn(x, "OCSP response", TLS_LEN_OCSP_RESPONSE, |x|{
			try_buffer!(x.write_slice(&ocsp));
			Ok(())
		})
	})
}

struct TxCertificateCommonParameters<'a>
{
	tls13: bool,
	certificate: &'a Box<CertificateSigner+Send>,
	send_spki_only: bool,
	sni_name: Option<String>,
	no_check_certificate: bool,
	ocsp_v1_offered: bool,
	ct_offered: bool,
	maybe_send_ocsp: bool,
}

fn tx_certificate_common<'a>(base: &mut SessionBase, params: TxCertificateCommonParameters<'a>,
	names: &mut NamesContainer, debug: Debugger, config: &ServerConfiguration, hs_hash: &mut HandshakeHash,
	state: &str, crypto_algs: &mut CryptoTracker) -> Result<(), TlsFailure>
{
	let mut have_sct = false;
	let (chain, ocsp) = (params.certificate.get_certificate(), params.certificate.get_ocsp());
	let have_ocsp = ocsp.0.is_some();
	//Check if there are SCTs in OCSP response.
	if let &Some(ref x) = &ocsp.0 {
		have_sct |= OcspDumpedScts::from(x).map(|x|x.list.len() > 0).unwrap_or(false);
	}

	let certarr = &chain.0;
	let eecert = match certarr.get(0) {
		Some(x) => &(x.0)[..],
		None => fail!(TlsFailure::EmptyCertChain(match params.sni_name {
			Some(ref x) => x.deref(),
			None => "<default>"
		}.to_owned()))
	};
	let mut intermediates = Vec::<&[u8]>::new();
	for i in certarr.iter().skip(1) {
		intermediates.push(&i.0);
	}
	//Check if there are dedicated SCTs.
	have_sct |= params.certificate.get_scts().0.is_some();

	let mut features = CFLAG_MUST_EMS | CFLAG_MUST_RENEGO;	//Always supported.
	if (config.flags[0] | FLAGS0_ENABLE_TLS13) != 0 { features |= CFLAG_MUST_TLS13; }
	if have_ocsp { features |= CFLAG_MUST_STAPLE; }

	//Don't sanity-check the certificate if sending RPK or ACME response.
	if !params.send_spki_only && !params.no_check_certificate {
		let reference_name = match params.sni_name {
			Some(ref x) => Some(x.deref()),
			None => match config.default_sni_name {
				Some(ref x) => Some(x.deref()),
				None => None
			}
		};
		if have_sct { features |= CFLAG_MUST_CT; }
		let mut namelist: Option<Arc<Vec<String>>> = None;
		let mut c_issuer: Option<Arc<Vec<u8>>> = None;
		let mut c_serial: Option<Arc<Vec<u8>>> = None;
		let mut dummy = false;		//Require OCSP flag, we aren't interested in it.
		let mut dummy2 = false;		//Require SCT flag, we aren't interested in it.

		//Check cache.
		let caddr = chain.0.deref() as &_ as *const _ as usize;
		let time_now = TimeNow.get_time();
		if let Ok(x) = config.sane_certificates.read() {
			if let Some(ref cache) = x.get(&caddr) {
				//Try upgrading reference to check its validity. If it can be up-
				//graded, check that it is not expired and that all needed flags
				//are there (flags are internally set, so an attacker can't force
				//repeated validations).
				if cache.ccref.upgrade().is_some() && time_now < cache.expires &&
					cache.flags & !features == 0 {
					namelist = Some(cache.names.clone());
					c_issuer = Some(cache.issuer.clone());
					c_serial = Some(cache.serial.clone());
				}
			}
		};

		//We need to check that the certificate is valid for host.
		let invalid = match (&reference_name, &namelist) {
			(&Some(ref refname), &Some(ref names)) => !name_in_set2(names.iter().map(|x|
				x.as_bytes()), refname.as_bytes()),
			(_, _) => true
		};
		if invalid {
			//The cached validation is not valid.
			namelist = None;
		}

		let cert_known_valid = namelist.is_some();
		if let Some(ref x) = namelist {
			//Cached validation. The certificate is known good, so data can be released
			//immediately.
			names.set_server_names(x.clone());
			names.release_server_names();
		} else {
			//Only run sanity checks if certificate is not known to pass those.
			let (good_until, need_features, namelist, t_issuer, t_serial) =
				match sanity_check_certificate_chain(eecert, &intermediates, reference_name,
					&time_now, true, features,
					|eecert_p,_,_,_,_,_,v_until, need_features|{
					//Get the server names and release those immediately.
					let namelist: Arc<Vec<String>> = Arc::new(eecert_p.dnsnames.iter().
						filter_map(|x|from_utf8(x).ok()).map(|x|x.to_owned()).
						collect());
					let t_issuer = Arc::new(eecert_p.issuer.as_raw_issuer().to_owned());
					let t_serial = Arc::new(eecert_p.serial_number.to_owned());
					Ok((v_until, need_features, namelist, t_issuer, t_serial))
				}, (), &config.killist, &mut dummy, &mut dummy2) {
				Ok(x) => x,
				Err(ref x) => fail!(TlsFailure::InterlockCertificate(x.clone()))
			};
			c_issuer = Some(t_issuer.clone());
			c_serial = Some(t_serial.clone());
			names.set_server_names(namelist.clone());
			names.release_server_names();
			//Save good-until, associating with chain. Note that the key may already exist
			//In this case we know it is stale and we want to update it, which insert does.
			if let Ok(mut x) = config.sane_certificates.write() {
				x.insert(caddr, SaneCertVal{
					ccref: Arc::downgrade(&chain.0),
					expires: good_until,
					flags: need_features,
					names: namelist.clone(),
					issuer: t_issuer,
					serial: t_serial,
				});
			};
		}
		debug!(HANDSHAKE_EVENTS debug, "Certificate sanity check passed");
		if let Some(ref ocspcontent) = ocsp.0 {
			let (eecert_issuer, eecert_serial) = match (c_issuer, c_serial) {
				(Some(x), Some(y)) => (x, y),
				(_, _) => sanity_failed!("No certificate issuer&serial available")
			};
			let ocspaddr = ocspcontent.deref() as &_ as *const _ as usize;
			let known_valid_ocsp = if let Ok(x) = config.sane_ocsp.read() {
				if let Some(ref cache) = x.get(&ocspaddr) {
					//Check the entry is not expired and belongs to the certificate.
					cache.certaddr == caddr && cache.ocspref.upgrade().is_some() &&
						time_now < cache.expires && cert_known_valid
				} else {
					false
				}
			} else {
				false
			};

			if !known_valid_ocsp {
				let res = OcspSanityCheckResults::from(ocspcontent, CertificateIssuer(
					eecert_issuer.deref()), eecert_serial.deref(), &TimeNow).map_err(|x|
					TlsFailure::InterlockOcsp(x))?;
				if let Ok(mut x) = config.sane_ocsp.write() {
					x.insert(ocspaddr, SaneOcspVal{
						certaddr: caddr,
						ocspref: Arc::downgrade(ocspcontent),
						expires: res.not_after
					});
				};
			}
			debug!(HANDSHAKE_EVENTS debug, "OCSP sanity check passed");
		}
	}

	let mut content = Vec::new();
	if params.tls13 {
		//The context field must be empty.
		write_sub_fn(&mut content, "certificate context", TLS_LEN_CERT_CONTEXT, |_|Ok(()))?;
	}
	write_sub_fn(&mut content, "certificate chain", TLS_LEN_CERT_LIST, |x|{
		if params.send_spki_only {
			//Just send the SPKI of the first certificate.
			if params.tls13 {
			//Just send the SPKI of the first certificate.
				write_sub_fn(x, "certificate", TLS_LEN_CERT_ENTRY, |x|{
					try_buffer!(x.write_slice(extract_spki(eecert).map_err(|x|
						TlsFailure::CantExtractSpki(x))?));
					Ok(())
				})?;
				write_sub_fn(x, "Certificate extensions", TLS_LEN_EXTENSIONS, |x|{
					//Server key type. If using RPK, send this.
					write_ext_fn(x, EXT_SERVER_CERTIFICATE_TYPE,
						"server_certificate_type", |y|{
							try_buffer!(y.write_u8(CERTTYPE_RPK));
							Ok(())
						})?;
					Ok(())
				})?;
			} else {
				try_buffer!(x.write_slice(extract_spki(eecert).map_err(|x|
					TlsFailure::CantExtractSpki(x))?));
			}
		} else {
			let mut first = true;
			for i in certarr.iter() {
				write_sub_fn(x, "certificate", TLS_LEN_CERT_LIST, |x|{
					try_buffer!(x.write_slice(&i.0));
					Ok(())
				})?;
				if params.tls13 {
					write_sub_fn(x, "Certificate extensions", TLS_LEN_EXTENSIONS, |x|{
						//OCSP.
						if params.ocsp_v1_offered && !params.send_spki_only && first {
							tx_ocsp_ext_tls13_common(x, params.certificate, debug.
							clone(), crypto_algs)?;
						}
						//Extension SCT.
						if params.ct_offered && !params.send_spki_only && first {
							tx_sct_ext_common(x, params.certificate, debug.clone(),
							crypto_algs)?;
						}
						Ok(())
					})?;
				}
				first = false;
			}
		}
		Ok(())
	})?;
	emit_handshake(debug.clone(), HSTYPE_CERTIFICATE, &content, base, hs_hash, state)?;
	//Send OCSP message if we have one. Note, maybe_send_ocsp should be never be set for TLS 1.3.
	if let (true, Some(ref ocspcontent)) = (params.maybe_send_ocsp, ocsp.0) {
		sanity_check!(ocspcontent.len() > 0, "Certificate has empty OCSP staple data");
		let mut content = Vec::new();
		try_buffer!(content.write_u8(OCSP_SINGLE));
		write_sub_fn(&mut content, "OCSP response", TLS_LEN_OCSP_RESPONSE, |x|{
			try_buffer!(x.write_slice(ocspcontent));
			Ok(())
		})?;
		emit_handshake(debug.clone(), HSTYPE_CERTIFICATE_STATUS, &content, base, hs_hash, state)?;
		debug!(HANDSHAKE_EVENTS debug, "Sent OCSP staple");
	}
	Ok(())
}

impl Tls12CertificateFields
{
	pub fn tx_certificate(self, base: &mut SessionBase, names: &mut NamesContainer, debug: Debugger,
		config: &ServerConfiguration, state: &str, crypto_algs: &mut CryptoTracker) ->
		Result<HandshakeState2, TlsFailure>
	{
		let certificate = self.certificate;
		let mut hs_hash = self.hs_hash;
		tx_certificate_common(base, TxCertificateCommonParameters{tls13: false, certificate:
			&certificate, send_spki_only: self.send_spki_only, sni_name: self.sni_name,
			no_check_certificate: self.no_check_certificate, ocsp_v1_offered: self.ocsp_v1_offered,
			ct_offered: self.ct_offered, maybe_send_ocsp: self.maybe_send_ocsp}, names, debug, config,
			&mut hs_hash, state, crypto_algs)?;
		Ok(HandshakeState2::Tls12ServerKeyExchange1(Tls12ServerKeyExchange1Fields{
			key_share: self.key_share,
			certificate: certificate,
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
			ems_enabled: self.ems_enabled,
			protection: self.protection,
			prf: self.prf,
		}))
	}
}

impl Tls13CertificateFields
{
	pub fn tx_certificate(self, base: &mut SessionBase, names: &mut NamesContainer, debug: Debugger,
		config: &ServerConfiguration, state: &str, crypto_algs: &mut CryptoTracker) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut certificate = self.certificate;
		let mut hs_hash = self.hs_hash;
		//maybe_send_ocsp is always false for TLS 1.3.
		tx_certificate_common(base, TxCertificateCommonParameters{tls13: true, certificate: &mut certificate,
			send_spki_only: self.send_spki_only, sni_name: self.sni_name, no_check_certificate:
			self.no_check_certificate, ocsp_v1_offered: self.ocsp_v1_offered, ct_offered:
			self.ct_offered, maybe_send_ocsp: false}, names, debug, config, &mut hs_hash, state,
			crypto_algs)?;
		Ok(HandshakeState2::Tls13CertificateVerify1(Tls13CertificateVerify1Fields{
			certificate: certificate,
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_hs_write: self.client_hs_write,
			server_hs_write: self.server_hs_write,
			master_secret: self.master_secret,
			hs_hash: hs_hash,
		}))
	}
}

impl Tls13CertificateVerify1Fields
{
	pub fn tx_certificate_verify(self, _base: &mut SessionBase, debug: Debugger) ->
		Result<HandshakeState2, TlsFailure>
	{
		//Sign the thing and emit the signature.
		let tbs = format_tbs_tls13(self.hs_hash.checkpoint()?, true)?;
		debug!(CRYPTO_CALCS debug, "Server TBS:\n{}", bytes_as_hex_block(&tbs, ">"));
		let signature_future = self.certificate.sign(&tbs);
		debug!(HANDSHAKE_EVENTS debug, "Requesting signature for key exchange");
		Ok(HandshakeState2::Tls13CertificateVerify2(Tls13CertificateVerify2Fields {
			signature_future: signature_future,
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_hs_write: self.client_hs_write,
			server_hs_write: self.server_hs_write,
			master_secret: self.master_secret,
			hs_hash: self.hs_hash,
		}))
	}
}

impl Tls13CertificateVerify2Fields
{
	pub fn tx_certificate_verify(self, base: &mut SessionBase, debug: Debugger, state: &str,
		crypto_algs: &mut CryptoTracker) -> Result<HandshakeState2, TlsFailure>
	{
		let is_ready = self.signature_future.settled();
		if !is_ready { return Ok(HandshakeState2::Tls13CertificateVerify2(self)); }

		let (sigalgo, signature) = self.signature_future.read().map_err(|_|assert_failure!(
			"Signature request gave no answer"))?.map_err(|_|assert_failure!(
			"Can't sign handshake"))?;
		if let Some(x) = SignatureType::by_tls_id(sigalgo) {
			crypto_algs.set_signature(x);
		} else {
			sanity_failed!("Trying to sign handshake with unknown signature scheme {:x}", sigalgo);
		};
		let mut content = Vec::new();
		try_buffer!(content.write_u16(sigalgo));
		write_sub_fn(&mut content, "Signature", TLS_LEN_SIGNATURE, |x|{
			try_buffer!(x.write_slice(signature.as_ref()));
			Ok(())
		})?;
		debug!(HANDSHAKE_EVENTS debug, "Signed the key exchange");

		let mut hs_hash = self.hs_hash;
		emit_handshake(debug.clone(), HSTYPE_CERTIFICATE_VERIFY, &content, base, &mut hs_hash, state)?;

		Ok(HandshakeState2::Tls13ServerFinished(Tls13ServerFinishedFields {
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_hs_write: self.client_hs_write,
			server_hs_write: self.server_hs_write,
			master_secret: self.master_secret,
			hs_hash: hs_hash,
		}))
	}
}
