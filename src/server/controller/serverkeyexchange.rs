use super::{HandshakeState2, write_sub_fn};
use ::common::{CryptoTracker, Debugger, emit_handshake, format_tbs_tls12, HandshakeHash, HSTYPE_SERVER_KEY_EXCHANGE,
	TLS_LEN_SIGNATURE, TLS_LEN_TLS12_PUBKEY, TlsFailure};
use ::logging::bytes_as_hex_block;
use ::record::SessionBase;
use ::server::controller::serverhellodone::Tls12ServerHelloDoneFields;
use ::server_certificates::CertificateSigner;
use ::stdlib::{Box, Vec};

use btls_aux_aead::ProtectorType;
use btls_aux_dhf::DiffieHellmanKey;
use btls_aux_futures::FutureReceiver;
use btls_aux_hash::HashFunction;
use btls_aux_serialization::Sink;
use btls_aux_signature_algo::SignatureType;


pub struct Tls12ServerKeyExchange1Fields
{
	pub certificate: Box<CertificateSigner+Send>,
	pub key_share: Box<DiffieHellmanKey+Send>,
	pub kill_connection_after_handshake: bool,
	pub client_random: [u8; 32],
	pub server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
	pub ems_enabled: bool,
	pub protection: ProtectorType,
	pub prf: HashFunction,
}

pub struct Tls12ServerKeyExchange2Fields
{
	incomplete_message: Vec<u8>,
	pub signature_future: FutureReceiver<Result<(u16, Vec<u8>),()>>,
	key_share: Box<DiffieHellmanKey+Send>,
	kill_connection_after_handshake: bool,
	client_random: [u8; 32],
	server_random: [u8; 32],
	pub hs_hash: HandshakeHash,
	pub ems_enabled: bool,
	pub protection: ProtectorType,
	pub prf: HashFunction,
}

impl Tls12ServerKeyExchange1Fields
{
	pub fn tx_server_key_exchange(self, _base: &mut SessionBase, debug: Debugger) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut content = Vec::new();
		let marker = content.marker_posonly();

		//Emit the parameters.
		try_buffer!(content.write_u8(3));	//Named group.
		try_buffer!(content.write_u16(self.key_share.tls_id()));		//The group.
		let pubkey = self.key_share.pubkey().map_err(|x|assert_failure!("Can't get own DH public key: {}",
			x))?;
		write_sub_fn(&mut content, "Public key", TLS_LEN_TLS12_PUBKEY, |x|{
		try_buffer!(x.write_slice(pubkey.as_ref()));
			Ok(())
		})?;

		//Sign the parameters and emit the signature.
		let mut tbs = None;
		let mut err = Ok(());
		content.callback_after(marker, &mut |dhparams| {
			let x = format_tbs_tls12(dhparams, &self.client_random,
				&self.server_random);
			match x {
				Ok(y) => { tbs = Some(y); },
				Err(y) => { err = Err(y); }
			}
		});
		let tbs = tbs.ok_or(assert_failure!("Can't obtain handshake data to sign"))?;
		debug!(CRYPTO_CALCS debug, "Server TBS:\n{}", bytes_as_hex_block(&tbs, ">"));
		let signature_future = self.certificate.sign(&tbs);
		debug!(HANDSHAKE_EVENTS debug, "Requesting signature for key exchange");
		Ok(HandshakeState2::Tls12ServerKeyExchange2(Tls12ServerKeyExchange2Fields {
			key_share: self.key_share,
			incomplete_message: content,
			signature_future: signature_future,
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: self.hs_hash,
			ems_enabled: self.ems_enabled,
			protection: self.protection,
			prf: self.prf,
		}))
	}
}

impl Tls12ServerKeyExchange2Fields
{
	pub fn tx_server_key_exchange(self, base: &mut SessionBase, debug: Debugger, state: &str,
		crypto_algs: &mut CryptoTracker) -> Result<HandshakeState2, TlsFailure>
	{
		let is_ready = self.signature_future.settled();
		if !is_ready { return Ok(HandshakeState2::Tls12ServerKeyExchange2(self)); }

		let (sigalgo, signature) = self.signature_future.read().map_err(|_|assert_failure!(
			"Signature request gave no answer"))?.map_err(|_|assert_failure!(
			"Can't sign handshake"))?;
		if let Some(x) = SignatureType::by_tls_id(sigalgo) {
			crypto_algs.set_signature(x);
		} else {
			sanity_failed!("Trying to sign handshake with unknown signature scheme {:x}", sigalgo);
		};
		let mut content = self.incomplete_message;
		try_buffer!(content.write_u16(sigalgo));
		write_sub_fn(&mut content, "Signature", TLS_LEN_SIGNATURE, |x|{
			try_buffer!(x.write_slice(signature.as_ref()));
			Ok(())
		})?;
		debug!(HANDSHAKE_EVENTS debug, "Signed the key exchange");

		let mut hs_hash = self.hs_hash;
		emit_handshake(debug, HSTYPE_SERVER_KEY_EXCHANGE, &content, base, &mut hs_hash, state)?;
		Ok(HandshakeState2::Tls12ServerHelloDone(Tls12ServerHelloDoneFields {
			key_share: self.key_share,
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_random: self.client_random,
			server_random: self.server_random,
			hs_hash: hs_hash,
			ems_enabled: self.ems_enabled,
			protection: self.protection,
			prf: self.prf,
		}))
	}
}
