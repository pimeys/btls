use super::{HandshakeState2, Tls12ShowtimeFields, Tls13ShowtimeFields};
use ::common::{Debugger, emit_handshake, HandshakeHash, HandshakeMessage, HashMessages, HSTYPE_FINISHED, TlsFailure};
use ::record::{SessionBase, SessionController, Tls12MasterSecret, Tls13MasterSecret, Tls13TrafficSecretIn,
	Tls13TrafficSecretOut};
use ::server::controller::changecipherspec::Tls12ServerChangeCipherSpecFields;

pub struct Tls12ClientFinishedFields
{
	pub kill_connection_after_handshake: bool,
	pub master_secret: Tls12MasterSecret,	//The master secret.
	pub hs_hash: HandshakeHash,
}

pub struct Tls12ServerFinishedFields
{
	pub kill_connection_after_handshake: bool,
	pub master_secret: Tls12MasterSecret,	//The master secret.
	pub hs_hash: HandshakeHash,
}

pub struct Tls13ServerFinishedFields
{
	pub server_hs_write: Tls13TrafficSecretOut,
	pub client_hs_write: Tls13TrafficSecretIn,
	pub kill_connection_after_handshake: bool,
	pub master_secret: Tls13MasterSecret,
	pub hs_hash: HandshakeHash,
}

pub struct Tls13ClientFinishedFields
{
	pub server_app_write: Tls13TrafficSecretOut,
	pub client_app_write: Tls13TrafficSecretIn,
	pub client_hs_write: Tls13TrafficSecretIn,
	pub kill_connection_after_handshake: bool,
	pub hs_hash: HandshakeHash,
}

impl Tls13ServerFinishedFields
{
	pub fn tx_finished(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		let htype = HSTYPE_FINISHED;
		let mac = self.server_hs_write.generate_finished(&hs_hash.checkpoint()?, debug.clone())?;
		emit_handshake(debug.clone(), htype, mac.as_ref(), base, &mut hs_hash, state)?;
		let appkeys_hash = hs_hash.checkpoint()?;
		let client_app_write = self.master_secret.traffic_secret_in(&appkeys_hash, debug.clone())?;
		let server_app_write = self.master_secret.traffic_secret_out(&appkeys_hash, debug.clone())?;
		let extractor = self.master_secret.extractor(&appkeys_hash, debug.clone())?;
		base.change_protector(server_app_write.to_protector(debug.clone())?);
		base.change_extractor(extractor);
		debug!(HANDSHAKE_EVENTS debug, "Sent Finished, upstream encryption keys changed");
		Ok(HandshakeState2::Tls13ClientFinished(Tls13ClientFinishedFields{
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_app_write: client_app_write,
			server_app_write: server_app_write,
			client_hs_write: self.client_hs_write,
			hs_hash: hs_hash,
		}))
	}
}

impl Tls13ClientFinishedFields
{
	pub fn rx_finished<'a>(self, msg: &[u8], controller: &mut SessionController, debug: Debugger,
		raw_msg: HandshakeMessage<'a>) -> Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		self.client_hs_write.check_finished(&hs_hash.checkpoint()?, msg, debug.clone())?;
		controller.set_deprotector(self.client_app_write.to_deprotector(debug.clone())?);
		debug!(HANDSHAKE_EVENTS debug, "Received finished, downstream \
			encryption keys changed");

		hs_hash.add(raw_msg, debug)?;	//The final handshake hash. Needed for e.g. RMS.
		Ok(HandshakeState2::Tls13Showtime(Tls13ShowtimeFields{
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_app_write: self.client_app_write,
			server_app_write: self.server_app_write,
			rekey_in_progress: false,
		}))
	}
}

impl Tls12ClientFinishedFields
{
	pub fn rx_finished<'a>(self, msg: &[u8], controller: &mut SessionController, debug: Debugger,
		raw_msg: HandshakeMessage<'a>) -> Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		self.master_secret.check_finished(&hs_hash.checkpoint()?, debug.clone(), msg)?;
		controller.set_extractor(self.master_secret.get_exporter(debug.clone())?);
		//We compute EMS key in HS demux since we need hash of this message.
		hs_hash.add(raw_msg, debug.clone())?;
		Ok(HandshakeState2::Tls12ServerChangeCipherSpec(Tls12ServerChangeCipherSpecFields{
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			master_secret: self.master_secret,
			hs_hash: hs_hash,
		}))
	}
}


impl Tls12ServerFinishedFields
{
	pub fn tx_finished(self, base: &mut SessionBase, debug: Debugger, state: &str) ->
		Result<HandshakeState2, TlsFailure>
	{
		let mut hs_hash = self.hs_hash;
		let htype = HSTYPE_FINISHED;
		let mac = self.master_secret.generate_finished(&hs_hash.checkpoint()?, debug.clone())?;
		emit_handshake(debug.clone(), htype, mac.as_ref(), base, &mut hs_hash, state)?;
		Ok(HandshakeState2::Tls12Showtime(Tls12ShowtimeFields{
			kill_connection_after_handshake: self.kill_connection_after_handshake,
		}))
	}
}
