use super::{emit_alpn_extension, HandshakeState2, write_ext_fn, write_sub_fn};
use ::common::{Debugger, emit_handshake, EXT_RECORD_SIZE_LIMIT, HandshakeHash, HSTYPE_ENCRYPTED_EXTENSIONS,
	TLS_LEN_EXTENSIONS, NamesContainer, TlsFailure};
use ::record::{SessionBase, Tls13MasterSecret, Tls13TrafficSecretIn, Tls13TrafficSecretOut};
use server::controller::certificate::Tls13CertificateFields;
use ::server_certificates::CertificateSigner;
use ::stdlib::{Box, String, Vec};

use btls_aux_serialization::Sink;

pub struct Tls13EncryptedExtensionsFields
{
	pub server_hs_write: Tls13TrafficSecretOut,
	pub client_hs_write: Tls13TrafficSecretIn,
	pub certificate: Box<CertificateSigner+Send>,
	pub send_spki_only: bool,
	pub sni_name: Option<String>,
	pub no_check_certificate: bool,
	pub ocsp_v1_offered: bool,
	pub ct_offered: bool,
	pub reduced_record_size: bool,
	pub kill_connection_after_handshake: bool,
	pub master_secret: Tls13MasterSecret,
	pub hs_hash: HandshakeHash,
}

impl Tls13EncryptedExtensionsFields
{
	pub fn tx_encrypted_extensions(self, base: &mut SessionBase, names: &mut NamesContainer, debug: Debugger,
		state: &str) -> Result<HandshakeState2, TlsFailure>
	{
		let mut content = Vec::new();
		write_sub_fn(&mut content, "EncryptedExtensions extensions", TLS_LEN_EXTENSIONS, |x|{
			//Extension ALPN... We give the ALPN if present.
			emit_alpn_extension(x, names)?;
			//Extension record_size_limit, send this if reduced in TLS 1.2.
			if self.reduced_record_size {
				write_ext_fn(x, EXT_RECORD_SIZE_LIMIT, "record_size_limit", |x|{
					//Always 16384.
					try_buffer!(x.write_u16(16384));
					Ok(())
				})?;
			}
			//OCSP, CT and RPK flag are sent in the certificate message.
			Ok(())
		})?;
		let mut hs_hash = self.hs_hash;
		emit_handshake(debug, HSTYPE_ENCRYPTED_EXTENSIONS, &content, base, &mut hs_hash, state)?;
		Ok(HandshakeState2::Tls13Certificate(Tls13CertificateFields {
			certificate: self.certificate,
			ct_offered: self.ct_offered,
			no_check_certificate: self.no_check_certificate,
			ocsp_v1_offered: self.ocsp_v1_offered,
			send_spki_only: self.send_spki_only,
			sni_name: self.sni_name,
			kill_connection_after_handshake: self.kill_connection_after_handshake,
			client_hs_write: self.client_hs_write,
			server_hs_write: self.server_hs_write,
			master_secret: self.master_secret,
			hs_hash: hs_hash,
		}))
	}
}
