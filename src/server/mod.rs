use ::{AbortReason, RxAppdataHandler, Session, TlsConnectionInfo, TxIrqHandler};
use ::common::{builtin_killist, TlsFailure};
use ::features::{CONFIG_PAGES, ConfigFlagsError, ConfigFlagsErrorKind, ConfigFlagsValue, DEFAULT_FLAGS, lookup_flag,
	set_combine_flags_clear, set_combine_flags_set, split_config_str, REQUIRE_CT_NAME, REQUIRE_OCSP_NAME,
	DEBUG_NO_SHARES_NAME};
use ::logging::Logging;
pub use ::messages::AlpnList;
use ::record::{ControlHandler,SessionBase, SessionController};
use ::server_certificates::{Certificate, CertificateLookup, CertificateSigner};
use ::server_keys::LocalKeyPair;
use ::stdlib::{Arc, Box, BTreeMap, Deref, DerefMut, IoError, IoErrorKind, IoRead, IoResult, IoWrite, Mutex, RwLock,
	String, ToOwned, UNIX_EPOCH, Weak, Vec};
use ::system::{Path, PathBuf, SystemTime};

use btls_aux_futures::FutureCallback;
use btls_aux_time::Timestamp;

mod controller;
use self::controller::HandshakeController;

#[derive(Clone)]
struct AcmeChallengeTable
{
	entries: BTreeMap<String, Option<String>>,
}

impl AcmeChallengeTable
{
	fn new() -> AcmeChallengeTable
	{
		AcmeChallengeTable{entries: BTreeMap::new()}
	}
}

fn millisec_clock() -> i64
{
	let now = SystemTime::now();
	let (now, dir) = match now.duration_since(UNIX_EPOCH) {
		Ok(x) => (x, true),
		Err(x) => (x.duration(), false),
	};
	let now = (now.as_secs() * 1000 + (now.subsec_nanos() / 1000000) as u64) as i64;
	if dir { now } else { -now }
}

///ALPN selection callback.
///
///This trait contains a callback to call when ALPN needs to be selected.
pub trait AlpnCallback
{
	///The ALPN needs to be selected.
	///
	///This method is called after ClientHello with ALPN extension has been received. It should respond by
	///selecting the ALPN to use.
	///
	///# Parameters:
	///
	/// * `self`: The callback.
	/// * `sni`: The SNI for connection, `None` if there is no SNI.
	/// * `alpn_list`: List of ALPNs.
	///
	///# Return value:
	///
	/// * If ALPN is to be negotiated, `Ok(Some(x))`, where x is the zero-based index to negotiate. Returning
	///index out of bounds results in handshake failure.
	/// * If no ALPN is to be sent back, `Ok(None)`.
	/// * To fail the handshake, return `Err(err)`, where `err` is the error.
	fn select_alpn<'a>(&mut self, sni: Option<&str>, alpn_list: AlpnList<'a>) ->
		Result<Option<usize>, TlsFailure>;
	///Return a clone of this object.
	///
	///# Parameters:
	///
	/// * `self`: The callback.
	///
	///# Return value:
	///
	/// * Boxed clone of the callback.
	fn clone_self(&self) -> Box<AlpnCallback+Send>;
}

///Server configuration.
///
///This structure contains various parts of the server configuration.
///
///# Creating a configuration:
///
///You can create the configuration object as:
///
///```no-run
///```
///
///# Setting certificate store and creating configuration (required).
///
///You can use [`CertificateDirectory`] as the certificate lookup (unless you need something like "keyless
///SSL" or "LURK SKI"), like this:
///
///```no-run
///let mut cert_store = CertificateDirectory::new(cert_dir_path, logger);
///let mut config = ServerConfiguration::from(&cert_store);
///```
///
///# Setting supported ALPNs (optional):
///
///You can set up supported ALPNs like follows:
///
///```no-run
///config.add_alpn("h2");
///config.add_alpn("http/1.1");
///```
///
///Note that if there are supported ALPNs, and client hello contains the ALPN extension, the client hello will be
///rejected if there is no overlap in ALPN values.
///
///# Setting require EMS (optional):
///
///You can set EMS to be required as follows:
///
///```no-run
///config.set_flags(0, FLAGS0_REQUIRE_EMS);
///```
///
///This is useful if you are going to use exporters, since those won't work without EMS.
///
///Note: If the negotiatied TLS version is 1.3, that is interpretted as sufficient to pass the EMS requirement,
///even if EMS is deprecated in TLS 1.3.
///
///# Setting TLS version range (optional):
///
///Enabling TLS 1.3 support:
///
///```no-run
///config.set_flags(0, FLAGS0_ENABLE_TLS13);
///```
///
///Requiring TLS 1.3.
///
///```no-run
///config.set_flags(0, FLAGS0_ENABLE_TLS13);
///config.clear_flags(0, FLAGS0_ENABLE_TLS12);
///```
///
///# Setting ACME challenge directory (strongly recommended to let admin configure this if binding to port 443
///for an extended time).
///
///```no-run
///config.set_acme_dir(path_from_config);
///```
///
///
///# Making session
///
///When you have it all set up, you can get server session using `.make_session()`.
///
///[`CertificateDirectory`]: server_certificates/struct.CertificateDirectory.html
pub struct ServerConfiguration
{
	//Certificate lookup routine.
	lookup_cert: Box<CertificateLookup+Send>,
	//Supported ALPN's, in order of decreasing perference.
	supported_alpn: Vec<String>,
	//ALPN selection routine, if any. Overrides supported_alpn.
	alpn_function: Option<Box<AlpnCallback+Send>>,
	//Flags page0.
	flags: [u64; CONFIG_PAGES],
	//The ACME challenge table.
	acme_challenges: Arc<RwLock<AcmeChallengeTable>>,
	//Logging config.
	log: Option<(u64, Arc<Box<Logging+Send+Sync>>)>,
	//Default SNI name.
	default_sni_name: Option<String>,
	//Certificate Killist.
	killist: BTreeMap<[u8; 32], ()>,
	//ACME path.
	acme_path: Option<PathBuf>,
	//Certificate sanity cache.
	sane_certificates: Arc<RwLock<BTreeMap<usize, SaneCertVal>>>,
	//OCSP sanity cache.
	sane_ocsp: Arc<RwLock<BTreeMap<usize, SaneOcspVal>>>,
	//Handshake timeout (milliseconds).
	handshake_timeout: u32,
	//ACME validation key.
	acme_key: Option<Arc<LocalKeyPair>>,
}

struct SaneCertVal
{
	ccref: Weak<Vec<Certificate>>,
	expires: Timestamp,
	flags: u32,
	names: Arc<Vec<String>>,
	issuer: Arc<Vec<u8>>,
	serial: Arc<Vec<u8>>,
}

struct SaneOcspVal
{
	certaddr: usize,
	ocspref: Weak<Vec<u8>>,
	expires: Timestamp,
}

impl Clone for ServerConfiguration
{
	fn clone(&self) -> ServerConfiguration
	{
		ServerConfiguration {
			lookup_cert:self.lookup_cert.clone_self(),
			supported_alpn:self.supported_alpn.clone(),
			flags: self.flags.clone(),
			acme_challenges:self.acme_challenges.clone(),
			log:self.log.clone(),
			default_sni_name: self.default_sni_name.clone(),
			killist: self.killist.clone(),
			acme_path: self.acme_path.clone(),
			sane_certificates: self.sane_certificates.clone(),
			sane_ocsp: self.sane_ocsp.clone(),
			handshake_timeout: self.handshake_timeout.clone(),
			alpn_function: match &self.alpn_function {
				&Some(ref x) => Some(x.clone_self()),
				&None => None
			},
			acme_key: self.acme_key.clone(),
		}
	}
}

impl<'a> From<&'a CertificateLookup> for ServerConfiguration
{
	fn from(lookup: &'a CertificateLookup) -> ServerConfiguration
	{
		ServerConfiguration {
			lookup_cert: lookup.clone_self(),
			supported_alpn: Vec::new(),
			flags: DEFAULT_FLAGS,
			acme_challenges: Arc::new(RwLock::new(AcmeChallengeTable::new())),
			log: None,
			default_sni_name: None,
			killist: builtin_killist(),
			acme_path: None,
			sane_certificates: Arc::new(RwLock::new(BTreeMap::new())),
			sane_ocsp: Arc::new(RwLock::new(BTreeMap::new())),
			handshake_timeout: 0,
			alpn_function: None,
			acme_key: LocalKeyPair::new_transient_ecdsa_p256().ok().map(|x|Arc::new(x)),
		}
	}
}

impl ServerConfiguration
{
	///Set handshake timeout.
	///
	///If handshake has not finished after this much time from creation of the server session, the handshake
	///is deemed to have timed out.
	///
	///# Parameters:
	///
	/// * `self`: The configuration to modify.
	/// * `timeout`: The timeout in milliseconds. 0 disables the timeout.
	pub fn set_handshake_timeout(&mut self, timeout: u32)
	{
		self.handshake_timeout = timeout;
	}
	///Set ACME challenge path.
	///
	///This is STRONGLY RECOMMENDED to be admin-configurable if you bind to port 443.
	///
	///# Parameters:
	///
	/// * `self`: The configuration to modify.
	/// * `path`: The path to set.
	pub fn set_acme_dir<P:AsRef<Path>>(&mut self, path: P)
	{
		self.acme_path = Some(path.as_ref().to_path_buf());
	}
	///Set ALPN callback function.
	///
	///# Parameters:
	///
	/// * `self`: The configuration to modify.
	/// * `func`: The ALPN callback function.
	///
	///# Notes:
	///
	/// * This function overrides any entries added by `.add_alpn()`.
	pub fn set_alpn_function(&mut self, func: Box<AlpnCallback+Send>)
	{
		self.alpn_function = Some(func);
	}
	///Add supported ALPN.
	///
	///# Parameters:
	///
	/// * `self`: The configuration to modify.
	/// * `alpn`: The ALPN to add.
	///
	///# Notes:
	///
	/// * The ALPNs are given in order of decreasing perference.
	/// * `.set_alpn_function()` overrides this.
	pub fn add_alpn(&mut self, alpn: &str)
	{
		self.supported_alpn.push(alpn.to_owned());
	}
	///Set flags.
	///
	///# Parameters:
	///
	/// * `self`: The configuration to modify.
	/// * `page`: The page number to set flags on.
	/// * `mask`: The mask of flags to set.
	pub fn set_flags(&mut self, page: usize, mask: u64)
	{
		if page < CONFIG_PAGES { self.flags[page] = set_combine_flags_set(page, mask, self.flags[page]); }
	}
	///Clear flags.
	///
	///# Parameters:
	///
	/// * `self`: The configuration to modify.
	/// * `page`: The page number to set flags on.
	/// * `mask`: The mask of flags to clear.
	pub fn clear_flags(&mut self, page: usize, mask: u64)
	{
		if page < CONFIG_PAGES { self.flags[page] = set_combine_flags_clear(page, mask, self.flags[page]); }
	}
	///Set/Clear flags according to config string.
	///
	///# Parameters:
	///
	/// * `self`: The configuration to modify.
	/// * `config`: The config string, a comma-separated string. Each word may be prefixed with '+' to enable
	///(default) or '-' or '!' to disable.
	/// * `error_cb`: Callback to call about unknown flags. Passed the index (0-based) that triggered
	///the warning, a flag telling if setting was tried to be enabled and a flag telling the setting is unknown
	//(false) or attempt to set it to that value was ignored (true).
	pub fn config_flags<F>(&mut self, config: &str, mut error_cb: F) where F: FnMut(ConfigFlagsError)
	{
		split_config_str(self, config, |objself, name, value, entry| {
			let argerr = if name == "acmedir" {
				if let &ConfigFlagsValue::Explicit(ref val) = &value {
					let p: &Path = val.as_ref();
					objself.acme_path = Some(p.to_path_buf());
					return;
				} else {
					true
				}
			} else {
				false
			};
			if argerr {
				error_cb(ConfigFlagsError{entry:entry,
					kind:ConfigFlagsErrorKind::ArgumentRequired(name)});
				return;
			}
			if let Some((page, mask)) = lookup_flag(name) {
				match value {
					ConfigFlagsValue::Disabled => objself.clear_flags(page, mask),
					ConfigFlagsValue::Enabled if name == REQUIRE_CT_NAME =>
						//Require-ct is not supported serverside.
						error_cb(ConfigFlagsError{entry:entry,
							kind:ConfigFlagsErrorKind::NoEffect(name)}),
					ConfigFlagsValue::Enabled if name == REQUIRE_OCSP_NAME =>
						//Require-ocsp is not supported serverside.
						error_cb(ConfigFlagsError{entry:entry,
							kind:ConfigFlagsErrorKind::NoEffect(name)}),
					ConfigFlagsValue::Enabled if name == DEBUG_NO_SHARES_NAME =>
						//debug-no-shares is not supported serverside.
						error_cb(ConfigFlagsError{entry:entry,
							kind:ConfigFlagsErrorKind::NoEffect(name)}),
					ConfigFlagsValue::Enabled => objself.set_flags(page, mask),
					ConfigFlagsValue::Explicit(_) => error_cb(ConfigFlagsError{entry:entry,
						kind:ConfigFlagsErrorKind::NoArgument(name)})
				}
			} else {
				error_cb(ConfigFlagsError{entry:entry,
					kind:ConfigFlagsErrorKind::UnrecognizedSetting(name)});
			}
		});
	}
	///Create an ACME challenge response endpoint.
	///
	///# Parameters:
	///
	/// * `self`: The settings.
	/// * `challenge`: Challenge to respond to.
	/// * `response`: Optional response to challenge.
	///
	///# Failures:
	///
	/// * Error string.
	///
	///# Notes:
	///
	/// * If handshake with matching challenge is accepted, that session will immediately disconnect upon
	///   reaching showtime state.
	pub fn create_acme_challenge(&mut self, challenge: &str, response: Option<&str>) ->
		Result<(), TlsFailure>
	{
		match self.acme_challenges.write() {
			Ok(mut x) => x.entries.insert(challenge.to_owned(), response.map(|x|x.to_owned())),
			Err(_) => sanity_failed!("Can't lock ACME challenge table")
		};
		Ok(())
	}
	///Delete an ACME challenge response endpoint.
	///
	///# Parameters:
	///
	/// * `self`: The settings.
	/// * `challenge`: Challenge to delete.
	///
	///# Failures:
	///
	/// * Error string.
	pub fn delete_acme_challenge(&mut self, challenge: &str) -> Result<(), TlsFailure>
	{
		match self.acme_challenges.write() {
			Ok(mut x) => x.entries.remove(challenge),
			Err(_) => sanity_failed!("Can't lock ACME challenge table")
		};
		Ok(())
	}
	///Set debugging mode.
	///
	///# Parameters:
	///
	/// * `self`: The configuration to modify.
	/// * `mask`: Mask of debugging events (DEBUG_*) to log.
	/// * `log`: The log to log the events to.
	pub fn set_debug(&mut self, mask: u64, log: Box<Logging+Send+Sync>)
	{
		self.log = Some((mask, Arc::new(log)));
	}
	///Set default server name.
	///
	///This server name is used if client does not indicate one via server_name extension.
	///
	///# Parameters:
	///
	/// * `self`: The configuration to modify.
	/// * `name`: The default server name.
	pub fn set_default_server_name(&mut self, name: &str)
	{
		self.default_sni_name = Some(name.to_owned());
	}
	///Create a server session with these settings.
	///
	///# Parameters:
	///
	/// * `self`: The settings.
	///
	///# Return value.
	///
	/// * The session.
	///
	///# Failures:
	///
	/// * Error string.
	pub fn make_session(&self) -> Result<ServerSession, TlsFailure>
	{
		let now = millisec_clock();
		let controller = HandshakeController::new(&self, self.log.clone());
		let mut sess = _ServerSession {
			base: SessionBase::new(self.log.clone()),
			hs:ServerHandshake(controller),
			handshake_deadline: if self.handshake_timeout != 0 {
				now + self.handshake_timeout as i64
			} else {
				i64::max_value()
			}
		};
		//Immediately request RX handshake, for receiving the ClientHello.
		sess.base.request_rx_handshake();
		//Ok.
		Ok(ServerSession(Arc::new(Mutex::new(sess))))
	}
}

struct ServerHandshake(HandshakeController);

impl ControlHandler for ServerHandshake
{
	fn handle_control(&mut self, rtype: u8, msg: &[u8], controller: &mut SessionController)
	{
		self.0.handle_control(rtype, msg, controller)
	}
	fn handle_tx_update(&mut self, controller: &mut SessionController)
	{
		self.0.handle_tx_update(controller)
	}
}

impl ServerHandshake
{
	fn borrow_inner(&self) -> &HandshakeController
	{
		&self.0
	}
	fn borrow_inner_mut(&mut self) -> &mut HandshakeController
	{
		&mut self.0
	}
}

//The internal state, send but pesumably not sync. Not cloneable.
struct _ServerSession
{
	base: SessionBase,
	hs: ServerHandshake,
	handshake_deadline: i64,	//Milliseconds since UNIX epoch.
}


///A server TLS session.
///
///This is the server end of TLS session.
///
///Use `.make_session()` of [`ServerConfiguration`] to create it.
///
///For operations supported, see trait [`Session`].
///
///If the session is cloned, both sessions refer to the same underlying TLS connection. This can be used to refer
///to the same session from multiple threads at once.
///
///[`ServerConfiguration`]: struct.ServerConfiguration.html
///[`Session`]: trait.Session.html
#[derive(Clone)]
pub struct ServerSession(Arc<Mutex<_ServerSession>>);

impl Session for ServerSession
{
	fn set_high_water_mark(&mut self, amount: usize)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_high_water_mark(amount),
			Err(_) => ()
		}
	}
	fn set_send_threshold(&mut self, threshold: usize)
	{
		match self.0.lock() {
			Ok(mut x) => x.base.set_send_threshold(threshold),
			Err(_) => ()
		}
	}
	fn bytes_available(&self) -> usize
	{
		match self.0.lock() {
			Ok(ref x) => x.base.bytes_available(),
			Err(_) => 0
		}
	}
	fn bytes_queued(&self) -> usize
	{
		match self.0.lock() {
			Ok(ref x) => x.base.bytes_queued(),
			Err(_) => 0
		}
	}
	fn is_eof(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.is_eof(),
			Err(_) => true
		}
	}
	fn wants_read(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.wants_read(),
			Err(_) => false
		}
	}
	fn set_tx_irq(&mut self, handler: Box<TxIrqHandler+Send>)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_tx_irq(handler),
			Err(_) => ()
		}
	}
	fn set_appdata_rx_fn(&mut self, handler: Option<Box<RxAppdataHandler+Send>>)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.set_appdata_rx_fn(handler),
			Err(_) => ()
		}
	}
	fn read_tls<R:IoRead>(&mut self, stream: &mut R) -> IoResult<()>
	{
		//We can't read with lock held.
		const READ_BLOCKSIZE: usize = 16500;
		let mut buf = [0; READ_BLOCKSIZE];
		let rlen = stream.read(&mut buf)?;
		if rlen > buf.len() {
			return Err(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!("Stream \
				read of {} bytes into buffer of {}", rlen, buf.len()))));
		}
		let mut buf = &buf[..rlen];
		let selfc = self.clone();
		let res = match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				let res = x.base.read_tls(&mut buf, &mut x.hs)?;
				//Arrange do_tx_request to be called when certlookup_future settles, if needed.
				x.hs.borrow_inner_mut().callback_with_certlookup_future(&mut x.base, move |sb, y|{
					if y.settled_cb(Box::new(DoTxRequestOnSettle(selfc.clone()))).is_none() {
						//Already settled.
						sb.do_tx_request();
					}
				});
				res
			},
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		};
		self.do_tls_tx_rx_cycle().map_err(|x|IoError::new(IoErrorKind::Other, x))?;
		Ok(res)
	}
	fn submit_tls_record(&mut self, record: &mut [u8]) -> Result<(), TlsFailure>
	{
		let selfc = self.clone();
		let res = match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				let res = x.base.submit_tls_record(record, &mut x.hs)?;
				//Arrange do_tx_request to be called when certlookup_future settles, if needed.
				x.hs.borrow_inner_mut().callback_with_certlookup_future(&mut x.base, move |sb, y|{
					if !y.settled_cb(Box::new(DoTxRequestOnSettle(selfc.clone()))).is_none() {
						//Already settled.
						sb.do_tx_request();
					}
				});
				res
			},
			Err(_) => sanity_failed!("Can't lock TLS session")
		};
		self.do_tls_tx_rx_cycle()?;
		Ok(res)
	}
	fn write_tls<W:IoWrite>(&mut self, stream: &mut W) -> IoResult<()>
	{
		//This can happen with waiting for signatures.
		self.do_tls_tx_rx_cycle().map_err(|x|IoError::new(IoErrorKind::Other, x))?;
		match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.write_tls(stream, &mut x.hs, false)
			},
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
	fn zerolatency_write(&mut self, output: &mut [u8], input: &[u8]) -> Result<(usize, usize), TlsFailure>
	{
		//This can happen with waiting for signatures.
		self.do_tls_tx_rx_cycle()?;
		match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref_mut();
				x.base.zerolatency_write(&mut x.hs, output, input)
			},
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn estimate_zerolatency_size(&self, outsize: usize) -> usize
	{
		match self.0.lock() {
			Ok(ref mut x) => {
				let x = x.deref();
				x.base.estimate_zerolatency_size(outsize)
			},
			Err(_) => 0
		}
	}
	fn can_tx_data(&self) -> bool
	{
		match self.0.lock() {
			Ok(ref x) => x.base.can_tx_data(),
			Err(_) => false
		}
	}
	fn handshake_completed(&self) -> bool
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().in_showtime(),
			Err(_) => false,	//Should not happen.
		}
	}
	fn send_eof(&mut self)
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.send_eof(),
			Err(_) => ()
		}
	}
	fn aborted_by(&self) -> Option<AbortReason>
	{
		match self.0.lock() {
			Ok(ref x) => {
				if !x.hs.0.ignore_handshake_timeout() && millisec_clock() > x.handshake_deadline {
					Some(AbortReason::HandshakeError(TlsFailure::HandshakeTimedOut))
				} else {
					x.base.aborted_by()
				}
			},
			Err(_) => Some(AbortReason::HandshakeError(TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
	fn extractor(&self, label: &str, context: Option<&[u8]>, buffer: &mut [u8]) -> Result<(), TlsFailure>
	{
		match self.0.lock() {
			Ok(ref x) => x.base.extractor(label, context, buffer),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn get_alpn(&self) -> Option<Option<Arc<String>>>
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().get_alpn(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn get_server_names(&self) -> Option<Arc<Vec<String>>>
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().get_server_names(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn get_sni(&self) -> Option<Option<Arc<String>>>
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().get_sni(),
			Err(_) => None		//Shouldn't be here.
		}
	}
	fn connection_info(&self) -> TlsConnectionInfo
	{
		match self.0.lock() {
			Ok(x) => x.hs.borrow_inner().connection_info(),
			Err(_) => TlsConnectionInfo {
				version: 0,
				ems_available: false,
				ciphersuite: 0,
				kex: None,
				signature: None,
				version_str: "",
				protection_str: "",
				hash_str: "",
				exchange_group_str: "",
				signature_str: "",
				validated_ct: false,
				validated_ocsp: false,
				validated_ocsp_shortlived: false,
			}
		}
	}
	fn get_record_size(&self, buffer: &[u8]) -> Option<usize>
	{
		match self.0.lock() {
			Ok(x) => x.base.get_record_size(buffer),
			Err(_) => Some(5)	//Shouldn't be here.
		}
	}
	fn begin_transacted_read<'a>(&self, buffer: &'a mut [u8]) -> Result<&'a [u8], TlsFailure>
	{
		match self.0.lock() {
			Ok(x) => x.base.begin_transacted_read(buffer),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
	fn end_transacted_read(&mut self, len: usize) -> Result<(), TlsFailure>
	{
		match self.0.lock() {
			Ok(mut x) => x.base.end_transacted_read(len),
			Err(_) => sanity_failed!("Can't lock TLS session")
		}
	}
}

impl IoRead for ServerSession
{
	fn read(&mut self, buf: &mut [u8]) -> IoResult<usize>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.read(buf),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
}

impl IoWrite for ServerSession
{
	fn write(&mut self, buf: &[u8]) -> IoResult<usize>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.write(buf),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
	fn flush(&mut self) -> IoResult<()>
	{
		match self.0.lock() {
			Ok(ref mut x) => x.base.flush(),
			Err(_) => fail!(IoError::new(IoErrorKind::Other, TlsFailure::from(assert_failure!(
				"Can't lock TLS session"))))
		}
	}
}

impl ServerSession
{
	fn do_tls_tx_rx_cycle(&mut self) -> Result<(), TlsFailure>
	{
		let selfc = self.clone();
		let mut inner = match self.0.lock() {
			Ok(x) => x,
			Err(_) => sanity_failed!("Can't lock TLS session")
		};
		//Do TX cycles if needed.
		while inner.hs.borrow_inner().wants_tx() {
			let inner = inner.deref_mut();
			if inner.hs.borrow_inner_mut().queue_hs_tx(&mut inner.base) {
				inner.base.finish_tx_handshake();
			}
			//Arrange do_tx_request to be called when signature_future settles, if needed.
			inner.hs.borrow_inner_mut().callback_with_signature_future(&mut inner.base, |sb, y|{
				if !y.settled_cb(Box::new(DoTxRequestOnSettle(selfc.clone()))).is_none() {
					//Already settled.
					sb.do_tx_request();
				}
			});
		}
		//Re-arm the RX if needed. Note that previous TX cycle can have altered the condition.
		if inner.hs.borrow_inner().wants_rx() {
			inner.base.request_rx_handshake();
		}
		Ok(())
	}
}

struct DoTxRequestOnSettle(ServerSession);

type ChooserFT = Result<Box<CertificateSigner+Send>, ()>;
type SignerFT = Result<(u16, Vec<u8>),()>;

impl FutureCallback<ChooserFT> for DoTxRequestOnSettle
{
	fn on_settled(&mut self, val: ChooserFT) -> ChooserFT
	{
		match (self.0).0.lock() {
			Ok(ref mut y) => y.base.do_tx_request(),
			Err(_) => ()
		};
		val
	}
}

impl FutureCallback<SignerFT> for DoTxRequestOnSettle
{
	fn on_settled(&mut self, val: SignerFT) -> SignerFT
	{
		match (self.0).0.lock() {
			Ok(ref mut y) => y.base.do_tx_request(),
			Err(_) => ()
		};
		val
	}
}
