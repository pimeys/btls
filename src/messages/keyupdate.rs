use ::common::TlsFailure;

///The TLS KeyUpdate message
pub struct TlsKeyUpdate
{
	///Requested update?
	pub requested: bool,
}

impl TlsKeyUpdate
{
	pub fn parse(msg: &[u8]) -> Result<TlsKeyUpdate, TlsFailure>
	{
		fail_if!(msg.len() != 1, TlsFailure::KeyUpdateBadLength(msg.len()));
		//msg.len()=1, so this is in-bounds.
		let requested = msg[0] == 1;
		Ok(TlsKeyUpdate {
			requested: requested
		})
	}
}
