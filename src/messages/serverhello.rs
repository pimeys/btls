use super::{Debugger, ExtensionAssignments, ExtensionParseError, parse_extensions, SctList};
use ::common::{Ciphersuite, EXT_ALPN, EXT_CT, EXT_EMS, EXT_KEY_SHARE, EXT_RECORD_SIZE_LIMIT, EXT_RENEGO_INFO,
	EXT_SERVER_CERTIFICATE_TYPE, EXT_SERVER_NAME, EXT_STATUS_REQUEST, TLS_LEN_TLS13_PUBKEY, TlsFailure,
	TlsVersion};
use ::stdlib::{from_utf8, Vec};

use btls_aux_dhf::Dhf;
use btls_aux_serialization::Source;

///The TLS ServerHello message
pub struct TlsServerHello<'a>
{
	pub server_version: TlsVersion,
	pub random: [u8; 32],
	pub ciphersuite: Ciphersuite,
	pub cert_type: Option<u8>,
	pub maybe_send_status: bool,
	pub ems_supported: bool,
	pub renego_ok: bool,
	pub alpn: Option<&'a str>,
	pub certificate_transparency: Option<Vec<&'a [u8]>>,
	pub record_size_limit: Option<u16>,
	pub keyshare: Option<(Dhf, &'a [u8])>
}

#[derive(Copy,Clone,Debug)]
enum TlsServerHelloExt
{
	ServerName,
	StatusRequest,
	Alpn,
	CertificateTransparency,
	ServerCertificateType,
	ExtendedMasterSecret,
	KeyShare,
	RenegoInfo,
	RecordSizeLimit,
}

impl ExtensionAssignments for TlsServerHelloExt
{
	fn get_entry(extid: u16, is_tls12: bool) -> Option<Self>
	{
		Some(match extid {
			EXT_SERVER_NAME if is_tls12 => TlsServerHelloExt::ServerName,
			EXT_STATUS_REQUEST if is_tls12 => TlsServerHelloExt::StatusRequest,
			EXT_ALPN if is_tls12 => TlsServerHelloExt::Alpn,
			EXT_CT if is_tls12 => TlsServerHelloExt::CertificateTransparency,
			EXT_SERVER_CERTIFICATE_TYPE if is_tls12 => TlsServerHelloExt::ServerCertificateType,
			EXT_EMS if is_tls12 => TlsServerHelloExt::ExtendedMasterSecret,
			EXT_KEY_SHARE if !is_tls12 => TlsServerHelloExt::KeyShare,
			EXT_RENEGO_INFO if is_tls12 => TlsServerHelloExt::RenegoInfo,
			EXT_RECORD_SIZE_LIMIT if is_tls12 => TlsServerHelloExt::RecordSizeLimit,
			_ => return None
		})
	}
	fn get_bit(&self) -> u32
	{
		match *self {
			TlsServerHelloExt::ServerName =>              0,
			TlsServerHelloExt::StatusRequest =>           1,
			TlsServerHelloExt::Alpn =>                    2,
			TlsServerHelloExt::CertificateTransparency => 3,
			TlsServerHelloExt::ServerCertificateType =>   4,
			TlsServerHelloExt::ExtendedMasterSecret =>    5,
			TlsServerHelloExt::KeyShare =>                6,
			TlsServerHelloExt::RenegoInfo =>              7,
			TlsServerHelloExt::RecordSizeLimit =>         8,
		}
	}
}

impl<'a> TlsServerHello<'a>
{
	pub fn parse(msg: &'a [u8], debugger: Debugger) -> Result<TlsServerHello<'a>, TlsFailure>
	{
		let mut msg = Source::new(msg);
		let mut record_size_limit = None;
		let mut ems_supported = false;
		let mut renego_ok = false;
		let mut keyshare = None;
		let mut cert_type = None;
		let mut maybe_send_status = false;
		let mut certificate_transparency = None;
		let mut alpn = None;

		let real_version = msg.read_u16(TlsFailure::CantParseSh)?;
		let server_version = TlsVersion::by_tls_id(real_version, false)?;
		let is_tls12 = server_version.is_tls12();
		let tlsver = if is_tls12 { 2 } else { 3 };

		//Random.
		let mut random = [0u8; 32];
		msg.read_array(&mut random, TlsFailure::CantParseSh)?;

		if is_tls12 {
			//Session id, ignored.
			let sessid = msg.read_slice(..32, TlsFailure::CantParseSh)?;
			fail_if!(sessid.len() > 32, TlsFailure::CantParseSh);
		}

		//Read ciphersuite, and initialize PRF.
		let csnum = msg.read_u16(TlsFailure::CantParseSh)?;
		let ciphersuite = match Ciphersuite::by_tls_id(csnum) {
			Some(x) => x,
			None => fail!(TlsFailure::BogusShCiphersuite(csnum)),
		};

		if is_tls12 {
			//Compression, must be 0.
			match msg.read_u8(TlsFailure::CantParseSh)? {
				0 => (),
				x => fail!(TlsFailure::BogusShCompression(x)),
			};
		}

		//Extensions.
		parse_extensions(&mut msg, is_tls12, |etype, mut epayload|{
			debug!(TLS_EXTENSIONS debugger, "ServerHello: Received extension {:?} ({} bytes).",
				etype, epayload.as_slice().len());
			match etype {
				TlsServerHelloExt::ServerName => {
					//Just ignore this.
				},
				TlsServerHelloExt::StatusRequest => {
					maybe_send_status = true;
				},
				TlsServerHelloExt::Alpn => {
					//Bonus points for 3(!) nested lengths around value we want...
					let length1 = epayload.read_u16(TlsFailure::CantParseAlpn)?;
					let length2 = epayload.read_u8(TlsFailure::CantParseAlpn)?;
					fail_if!(length2 == 0, TlsFailure::AlpnEmptyProtocol);
					let alpn_raw = epayload.read_remaining();
					//The lengths can't be even near overflow.
					let len_p1 = alpn_raw.len() + 1;
					fail_if!(length1 as usize != len_p1, TlsFailure::AlpnExpectedLen1Val(len_p1,
						length1 as usize));
					fail_if!(length2 as usize != alpn_raw.len(), TlsFailure::AlpnExpectedLen2Val(
						alpn_raw.len(), length2 as usize));
					let _alpn = from_utf8(alpn_raw).map_err(|_|TlsFailure::AlpnNonUtf8Protocol)?;
					alpn = Some(_alpn);
				},
				TlsServerHelloExt::CertificateTransparency => {
					let scts = SctList::new(epayload.read_remaining()).map_err(|_|
						TlsFailure::CantParseSct)?;
					debug!(HANDSHAKE_EVENTS debugger, "Received {} new SCTs via TLS extension.",
						scts.iter().count());
					certificate_transparency = Some(scts.iter().collect());
				},
				TlsServerHelloExt::ServerCertificateType => {
					let format = epayload.read_u8(TlsFailure::CantParseSCertType)?;
					cert_type = Some(format);
				},
				TlsServerHelloExt::ExtendedMasterSecret => {
					ems_supported = true;
				},
				TlsServerHelloExt::KeyShare => {
					let group_id = epayload.read_u16(TlsFailure::CantParseKeyShare)?;
					let pubkey = epayload.read_slice(TLS_LEN_TLS13_PUBKEY,
						TlsFailure::CantParseKeyShare)?;
					fail_if!(pubkey.len() == 0, TlsFailure::CantParseKeyShare);
					let group = match Dhf::by_tls_id(group_id) {
						Some(y) => y,
						None => fail!(TlsFailure::BogusKeyShareGroup(group_id))
					};
					keyshare = Some((group, pubkey))
				}
				TlsServerHelloExt::RenegoInfo => {
					fail_if!(epayload.read_remaining() != &[0],
						TlsFailure::RenegotiationAttackDetected);
					renego_ok = true;
				},
				TlsServerHelloExt::RecordSizeLimit => {
					let maxsize = epayload.read_u16(TlsFailure::CantParseRecSizeLim)?;
					fail_if!(maxsize < 64, TlsFailure::BadRecSizeLim(maxsize as usize));
					record_size_limit = Some(maxsize);
				}
			}
			Ok(())
		}, |etype, _|fail!(TlsFailure::BogusShExtension(etype, tlsver))).map_err(|x|match x {
			ExtensionParseError::CantParse => TlsFailure::CantParseSh,
			ExtensionParseError::Duplicate(y) => TlsFailure::DuplicateShExtension(y),
			ExtensionParseError::JunkAfter(y) => TlsFailure::JunkAfterShExtension(y),
			ExtensionParseError::Wrapped(y) => y
		})?;
		fail_if!(!msg.at_end(), TlsFailure::CantParseSh);

		Ok(TlsServerHello {
			server_version: server_version,
			random: random,
			ciphersuite: ciphersuite,
			maybe_send_status: maybe_send_status,
			cert_type: cert_type,
			keyshare: keyshare,
			ems_supported: ems_supported,
			renego_ok: renego_ok,
			certificate_transparency: certificate_transparency,
			alpn: alpn,
			record_size_limit: record_size_limit,
		})
	}
}
