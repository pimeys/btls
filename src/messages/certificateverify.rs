use super::{DigitallySigned, DigitallySignedReadError as DSRE};
use ::common::TlsFailure;

use btls_aux_serialization::Source;

///The TLS CertificateVerify message
pub struct TlsCertificateVerify<'a>
{
	///The digital signature.
	pub signature: DigitallySigned<'a>
}

impl<'a> TlsCertificateVerify<'a>
{
	pub fn parse(msg: &'a [u8]) -> Result<TlsCertificateVerify<'a>, TlsFailure>
	{
		let mut msg = Source::new(msg);
		let signature = match DigitallySigned::parse(&mut msg) {
			Ok(x) => x,
			Err(DSRE::CantParse) => fail!(TlsFailure::CantParseCv),
			Err(DSRE::BogusAlgorithm(x)) => fail!(TlsFailure::BogusCvSignatureAlgo(x)),
		};
		fail_if!(!msg.at_end(), TlsFailure::CantParseCv);
		Ok(TlsCertificateVerify {
			signature: signature
		})
	}
}
