use super::Debugger;
use ::common::{TLS_LEN_OCSP_RESPONSE, TlsFailure};

use btls_aux_serialization::Source;

///The TLS CertificateStatus message
pub struct TlsCertificateStatus<'a>
{
	///The response type.
	pub resptype: u8,
	///The response payload.
	pub respdata: &'a [u8]
}

impl<'a> TlsCertificateStatus<'a>
{
	pub fn parse(msg: &'a [u8], debugger: Debugger) -> Result<TlsCertificateStatus<'a>, TlsFailure>
	{
		let mut msg = Source::new(msg);
		debug!(HANDSHAKE_EVENTS debugger, "Received stapled OCSP response.");
		let (resptype, respdata) = {
			let res_type = msg.read_u8(TlsFailure::CantParseCs)?;
			let response = msg.read_slice(TLS_LEN_OCSP_RESPONSE, TlsFailure::CantParseCs)?;
			fail_if!(response.len() == 0, TlsFailure::CsEmptyResponse);
			fail_if!(!msg.at_end(), TlsFailure::CantParseCs);
			(res_type, response)
		};
		Ok(TlsCertificateStatus {
			resptype: resptype,
			respdata: respdata,
		})
	}
}
