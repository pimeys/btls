//!Types related to certificate handling on client
//!
//!This module contains the following types:
//!
//! * Representing data as either the data itself, or hash thereof: `DataRepresentation`.
//! * A HSTS/DANE pin/TA (and similar things): `HostSpecificPin`.
//! * A PKIX trust anchor: `TrustAnchor`.

use ::certificate::{ParsedCertificate,TrustAnchorValue};
use ::stdlib::{AsRef, Deref, IoRead, Range, String, ToOwned, Vec};
use ::system::{File, Path};

use btls_aux_hash::ChecksumFunction;
use btls_aux_serialization::Sink;

///Data representation
///
///This strcture represents piece of data either as value or as hash of value
pub enum DataRepresentation
{
	///The actual value of data.
	Value(Vec<u8>),
	///The SHA-256 hash of data.
	Sha256([u8;32]),
	///The SHA-384 hash of data.
	Sha384([u8;48]),
	///The SHA-512 hash of data.
	Sha512([u8;64]),
}

impl Clone for DataRepresentation
{
	fn clone(&self) -> DataRepresentation
	{
		match self {
			&DataRepresentation::Value(ref x) => DataRepresentation::Value(x.clone()),
			&DataRepresentation::Sha256(ref x) => DataRepresentation::Sha256(*x),
			&DataRepresentation::Sha384(ref x) => DataRepresentation::Sha384(*x),
			&DataRepresentation::Sha512(ref x) => DataRepresentation::Sha512(*x),
		}
	}
}

impl DataRepresentation
{
	///Does the specified data match the representation?
	///
	///# Parameters:
	///
	/// * `self`: The representation to check against.
	/// * `data`: The data to check.
	///
	///# Returns:
	///
	/// * True if it matches, false if does not.
	pub fn matches(&self, data: &[u8]) -> bool
	{
		match self {
			&DataRepresentation::Value(ref x) => x.deref() == data,
			&DataRepresentation::Sha256(ref x) => {
				let hash = match ChecksumFunction::Sha256.calculate(data) {
					Ok(x) => x, Err(_) => return false
				};
				hash.as_ref() == &x[..]
			},
			&DataRepresentation::Sha384(ref x) => {
				let hash = match ChecksumFunction::Sha384.calculate(data) {
					Ok(x) => x, Err(_) => return false
				};
				hash.as_ref() == &x[..]
			},
			&DataRepresentation::Sha512(ref x) => {
				let hash = match ChecksumFunction::Sha512.calculate(data) {
					Ok(x) => x, Err(_) => return false
				};
				hash.as_ref() == &x[..]
			},
		}
	}
}

///Host-specific certificate pin / trust anchor.
///
///This structure contains a host-specific certificate pin or trust anchor.
///
///It can be used e.g. for implemeting DANE TLSA (RFC6698) support or HPKP (RFC 7469).
///
///# Notes:
///
///Putting a pin that is pinned (or no pin is actually pinned) and trust anchor on EE SPKI will automatically light
///up RPK support.
///
///There are currently following limits for matching:
///
/// * Built-in roots can only be matched by SPKI.
/// * Pins can only match certificates in chain, or built-in roots. This applies even if data
///   representation is uncompressed and whole certificate matching is used.
#[derive(Clone)]
pub struct HostSpecificPin
{
	///CA or EE flag. If true, matches only CAs, if false, matches only EEs, if none, matches
	///both.
	pub ca_flag: Option<bool>,
	///Pin flag. If set, the entry acts as a pin.
	pub pin: bool,
	///Trust Anchor flag. If set, the entry acts as a trust anchor.
	pub ta: bool,
	///SPKI flag. If set, entry matches SPKI, otherwise it matches entiere certificate.
	pub spki: bool,
	///The data to match against.
	pub data: DataRepresentation,
}

impl HostSpecificPin
{
	///Trust server key with specified SHA-256.
	///
	///# Parameters:
	///
	/// * `sha256`: The SHA-256 of the key.
	/// * `whole_cert`: If true, hash the whole cert instead of just the key (the SPKI).
	/// * `issuer`: If true, accept CA certificates instead of end-entity certificates.
	///
	///# Returns:
	///
	/// * The pin object.
	///
	///# Notes:
	///
	/// * The object is marked as trust anchor but not pin, so don't mix this with pinning.
	/// * The certificate has to be sent by the server for this to work properly (only really
	///   matters for CA certificates, since server keys are always sent).
	///
	///
	///# Examples:
	///
	///This might be handy for those testing certificates:
	///
	///```no-run
	///let config = ClientConfiguration::new();
	///let mut connection = config.make_session_pinned(TEST_SERVER_NAME, &[trust_server_key_by_sha256(TEST_SERVER_KEYHASH, false, false)]);
	///```
	///
	///This lets you to connect to server `TEST_SERVER_NAME` having self-signed certificate
	///with key SHA-256 fingerprint of `TEST_SERVER_KEYHASH`.
	pub fn trust_server_key_by_sha256(sha256: &[u8; 32], whole_cert: bool, issuer: bool) ->
		HostSpecificPin
	{
		HostSpecificPin {
			ca_flag: Some(issuer),
			pin: false,
			ta: true,
			spki: !whole_cert,
			data: DataRepresentation::Sha256(*sha256)
		}
	}
	///Create a host specific pin from raw DNS TLSA data.
	///
	///# Parameters:
	///
	/// * `raw_tlsa`: The raw RRDATA of TLSA record.
	///
	///# Returns:
	///
	/// * The pin object.
	///
	///# Failures:
	///
	/// * If the data is malformed, fails with `()`.
	pub fn from_tlsa_raw(raw_tlsa: &[u8]) -> Result<HostSpecificPin, ()>
	{
		//The three-byte header has to be present.
		fail_if!(raw_tlsa.len() < 3, ());
		let (ca_flag, pin, ta) = match raw_tlsa[0] {
			0 => (Some(true), true, false),
			1 => (Some(false), true, false),
			2 => (Some(true), true, true),
			3 => (Some(false), true, true),
			_ => fail!(())
		};
		let spki = match raw_tlsa[1] {
			0 => false,
			1 => true,
			_ => fail!(())
		};
		let rdata = &raw_tlsa[3..];		//The actual matching data.
		let data = match raw_tlsa[2] {
			0 => DataRepresentation::Value(rdata.to_owned()),
			1 => DataRepresentation::Sha256({
				let mut val = [0; 32];
				fail_if!(rdata.len() != 32, ());	//Has to be one SHA-256 hash.
				val.copy_from_slice(rdata);
				val
			}),
			2 => DataRepresentation::Sha512({
				let mut val = [0; 64];
				fail_if!(rdata.len() != 64, ());	//Has to be one SHA-512 hash.
				val.copy_from_slice(rdata);
				val
			}),
			_ => fail!(())
		};
		Ok(HostSpecificPin{ca_flag:ca_flag,pin:pin,ta:ta,spki:spki,data:data})
	}
}

///Hash function used by log.
#[derive(Copy,Clone,PartialEq,Eq)]
pub enum LogHashFunction
{
	///SHA-256
	Sha256,
}

///A trusted log.
///
///This structure represents a trusted Certificate Transparency log.
#[derive(Clone)]
pub struct TrustedLog
{
	///Name of log.
	pub name: String,
	///Log key (SPKI format).
	pub key: Vec<u8>,
	///Log Certificate Transparency v2 identifier (2-127 bytes, or empty if not v2 log).
	pub v2_id: Vec<u8>,
	///Log hash function for CTv2.
	pub v2_hash: LogHashFunction,
	///Time log expired (if set, only precerts are accepted, and only up to this bound). If log is fully valid,
	///this is set to None.
	pub expiry: Option<i64>,
}

///A trust anchor.
///
///This structure represents a trust anchor to evaluate trust against.
///
///There are three basic ways of creating this structure:
///
/// * Pass the subject (as X.509 Name) and public key (as X.509 SubjectPublicKeyInfo), along with optional
///   name constraints to `TrustAnchor::from_name_spki()`.
/// * Pass certificate data in DER form to `TrustAnchor::from_certificate()`.
/// * Pass filename of file containg the certificate data in DER form to
///   `TrustAnchor::from_certificate_file()`.
#[derive(Clone)]
pub struct TrustAnchor
{
	backing: Vec<u8>,
	subject: Range<usize>,
	spki: Range<usize>,
	allowed_names: Range<usize>,
	disallowed_names: Range<usize>,
}

#[derive(Clone)]
struct ListAdapter<'a,'b:'a>(&'a [&'b str], usize);

impl<'a,'b:'a> Iterator for ListAdapter<'a, 'b>
{
	type Item=&'b [u8];
	fn next(&mut self) -> Option<&'b [u8]> {
		let index = self.1;
		if index >= self.0.len() { return None; }
		self.1 += 1;
		Some(self.0[index].as_bytes())	//Always in range by above check.
	}
}

impl TrustAnchor
{
	///Create new trust anchor from subject name and SPKI.
	///
	///# Parameters:
	///
	/// * `subject`: The subject name, as X.509 Name structure.
	/// * `spki`: The X.509 SubjectPublicKeyInfo structure for the signature key.
	/// * `allowed_names`: Optional name constraints (if empty, all names are allowed).
	/// * `disallowed_names`: Optional name constraints.
	///
	///# Return value:
	///
	/// * The new trust anchor.
	///
	///# Failures:
	///
	/// * If the data is just too mangled, fails with descriptive error.
	pub fn from_name_spki(subject: &[u8], spki: &[u8], allow_names: &[&str], disallow_names: &[&str]) ->
		Result<TrustAnchor, String>
	{
		TrustAnchor::from_data(subject, spki, ListAdapter(allow_names, 0),
			ListAdapter(disallow_names, 0)).map_err(|_|"Internal error".to_owned())
	}
	///Create a trust anchor from DER-formatted certificate.
	///
	///# Parameters:
	///
	/// * `certificate`: The certificate to use as trust anchor.
	///
	///# Return value:
	///
	/// * The new trust anchor.
	///
	///# Failures:
	///
	/// * If the data is just too mangled, fails with descriptive error.
	pub fn from_certificate<R:IoRead>(certificate: &mut R) -> Result<TrustAnchor, String>
	{
		let mut data = Vec::new();
		certificate.read_to_end(&mut data).map_err(|x|format!("{}", x))?;
		let ta = ParsedCertificate::from(&data[..]).map_err(|x|format!("{}", x))?;
		Self::from_data(ta.subject.as_raw_subject(), ta.pubkey, ta.names_allow.iter(),
			ta.names_disallow.iter()).map_err(|_|"Internal error".to_owned())
	}
	fn from_data<'a,T1:Iterator<Item=&'a [u8]>+Clone,T2:Iterator<Item=&'a [u8]>+Clone>(subject: &[u8],
		spki: &[u8], allow_names: T1, disallow_names: T2) -> Result<TrustAnchor, ()>
	{
		let mut need_size = 0;
		need_size += subject.len();
		need_size += spki.len();
		for i in allow_names.clone() { need_size += 2 + i.len(); }
		for i in disallow_names.clone() { need_size += 2 + i.len(); }
		let mut backing = Vec::with_capacity(need_size);

		backing.write_slice(subject)?;
		let subject_range = 0..backing.len();
		backing.write_slice(spki)?;
		let spki_range = subject_range.end..backing.len();
		for i in allow_names.clone() {
			fail_if!(i.len() > 65535, ());
			backing.write_u16(i.len() as u16)?;
			backing.write_slice(i)?;
		}
		let allow_range = spki_range.end..backing.len();
		for i in disallow_names.clone() {
			fail_if!(i.len() > 65535, ());
			backing.write_u16(i.len() as u16)?;
			backing.write_slice(i)?;
		}
		let disallow_range = allow_range.end..backing.len();

		Ok(TrustAnchor {
			backing: backing,
			subject: subject_range,
			spki: spki_range,
			allowed_names: allow_range,
			disallowed_names: disallow_range,
		})
	}
	///Create a trust anchor from DER-formatted certificate file.
	///
	///# Parameters:
	///
	/// * `certificate`: The filename of certificate to use as trust anchor.
	///
	///# Return value:
	///
	/// * The new trust anchor.
	///
	///# Failures:
	///
	/// * On error, returns descriptive error.
	pub fn from_certificate_file<P:AsRef<Path>>(certificate: P) -> Result<TrustAnchor, String>
	{
		let certificate = certificate.as_ref();
		let mut _certificate = File::open(certificate).map_err(|x|format!("Can't open trust anchor `{}`: {}",
			certificate.display(), x))?;
		Self::from_certificate(&mut _certificate)
	}
	//Borrow trust anchor as internal trust anchor. This method is hidden from docs as it is considered
	//internal.
	#[doc(hidden)]
	pub fn to_internal_form(&self) -> (Vec<u8>, TrustAnchorValue)
	{
		static BLANK: [u8;0] = [];
		let subject = check_range(&self.backing, &self.subject).map(|x|&self.backing[x]).ok().unwrap_or(
			&BLANK[..]);
		(subject.to_owned(), TrustAnchorValue::new(self.backing.clone(), self.spki.clone(),
			self.allowed_names.clone(), self.disallowed_names.clone()))
	}
}

fn check_range(backing: &[u8], range: &Range<usize>) -> Result<Range<usize>, ()>
{
	fail_if!(range.end < range.start, ());
	fail_if!(range.end > backing.len(), ());
	fail_if!(range.start > backing.len(), ());
	Ok(range.clone())
}
