use super::tlsconsts::{CS_ECDHE_ECDSA_AES128, CS_ECDHE_ECDSA_AES256, CS_ECDHE_ECDSA_CHACHA20,
	CS_ECDHE_RSA_AES128, CS_ECDHE_RSA_AES256, CS_ECDHE_RSA_CHACHA20,
	CS_TLS13_AES128, CS_TLS13_AES256, CS_TLS13_CHACHA20};
use ::common::KeyExchangeType;

use btls_aux_aead::ProtectorType;
use btls_aux_hash::HashFunction;

#[derive(Copy,Clone,PartialEq,Eq,Debug)]
pub enum Ciphersuite
{
	TlsEcdheRsaWithAes128GcmSha256,
	TlsEcdheRsaWithAes256GcmSha384,
	TlsEcdheRsaWithChacha20Poly1305Sha256,
	TlsEcdheEcdsaWithAes128GcmSha256,
	TlsEcdheEcdsaWithAes256GcmSha384,
	TlsEcdheEcdsaWithChacha20Poly1305Sha256,
	Tls13WithAes128GcmSha256,
	Tls13WithAes256GcmSha384,
	Tls13WithChacha20Poly1305Sha256,
}

impl Ciphersuite
{
	//Just give one valid ciphersuite.
	pub fn random_suite() -> Ciphersuite
	{
		Ciphersuite::Tls13WithChacha20Poly1305Sha256
	}
	pub fn all_suites() -> &'static [Ciphersuite]
	{
		static ALL: [Ciphersuite; 9] = [
			Ciphersuite::TlsEcdheRsaWithAes128GcmSha256,
			Ciphersuite::TlsEcdheRsaWithAes256GcmSha384,
			Ciphersuite::TlsEcdheRsaWithChacha20Poly1305Sha256,
			Ciphersuite::TlsEcdheEcdsaWithAes128GcmSha256,
			Ciphersuite::TlsEcdheEcdsaWithAes256GcmSha384,
			Ciphersuite::TlsEcdheEcdsaWithChacha20Poly1305Sha256,
			Ciphersuite::Tls13WithAes128GcmSha256,
			Ciphersuite::Tls13WithAes256GcmSha384,
			Ciphersuite::Tls13WithChacha20Poly1305Sha256,
		];
		&ALL[..]
	}
	pub fn by_tls_id(id: u16) -> Option<Ciphersuite>
	{
		match id {
			CS_ECDHE_RSA_AES128 => Some(Ciphersuite::TlsEcdheRsaWithAes128GcmSha256),
			CS_ECDHE_RSA_AES256 => Some(Ciphersuite::TlsEcdheRsaWithAes256GcmSha384),
			CS_ECDHE_RSA_CHACHA20 => Some(Ciphersuite::TlsEcdheRsaWithChacha20Poly1305Sha256),
			CS_ECDHE_ECDSA_AES128 => Some(Ciphersuite::TlsEcdheEcdsaWithAes128GcmSha256),
			CS_ECDHE_ECDSA_AES256 => Some(Ciphersuite::TlsEcdheEcdsaWithAes256GcmSha384),
			CS_ECDHE_ECDSA_CHACHA20 => Some(Ciphersuite::TlsEcdheEcdsaWithChacha20Poly1305Sha256),
			CS_TLS13_AES128 => Some(Ciphersuite::Tls13WithAes128GcmSha256),
			CS_TLS13_AES256 => Some(Ciphersuite::Tls13WithAes256GcmSha384),
			CS_TLS13_CHACHA20 => Some(Ciphersuite::Tls13WithChacha20Poly1305Sha256),
			_ => None,
		}
	}
	pub fn to_tls_id(&self) -> u16
	{
		match self {
			&Ciphersuite::TlsEcdheRsaWithAes128GcmSha256 => CS_ECDHE_RSA_AES128,
			&Ciphersuite::TlsEcdheRsaWithAes256GcmSha384 => CS_ECDHE_RSA_AES256,
			&Ciphersuite::TlsEcdheRsaWithChacha20Poly1305Sha256 => CS_ECDHE_RSA_CHACHA20,
			&Ciphersuite::TlsEcdheEcdsaWithAes128GcmSha256 => CS_ECDHE_ECDSA_AES128,
			&Ciphersuite::TlsEcdheEcdsaWithAes256GcmSha384 => CS_ECDHE_ECDSA_AES256,
			&Ciphersuite::TlsEcdheEcdsaWithChacha20Poly1305Sha256 => CS_ECDHE_ECDSA_CHACHA20,
			&Ciphersuite::Tls13WithAes128GcmSha256 => CS_TLS13_AES128,
			&Ciphersuite::Tls13WithAes256GcmSha384 => CS_TLS13_AES256,
			&Ciphersuite::Tls13WithChacha20Poly1305Sha256 => CS_TLS13_CHACHA20,
		}
	}
	pub fn get_key_exchange(&self) -> KeyExchangeType
	{
		match self {
			&Ciphersuite::TlsEcdheRsaWithAes128GcmSha256 => KeyExchangeType::Tls12EcdheRsa,
			&Ciphersuite::TlsEcdheRsaWithAes256GcmSha384 => KeyExchangeType::Tls12EcdheRsa,
			&Ciphersuite::TlsEcdheRsaWithChacha20Poly1305Sha256 => KeyExchangeType::Tls12EcdheRsa,
			&Ciphersuite::TlsEcdheEcdsaWithAes128GcmSha256 => KeyExchangeType::Tls12EcdheEcdsa,
			&Ciphersuite::TlsEcdheEcdsaWithAes256GcmSha384 => KeyExchangeType::Tls12EcdheEcdsa,
			&Ciphersuite::TlsEcdheEcdsaWithChacha20Poly1305Sha256 => KeyExchangeType::Tls12EcdheEcdsa,
			&Ciphersuite::Tls13WithAes128GcmSha256 => KeyExchangeType::Tls13,
			&Ciphersuite::Tls13WithAes256GcmSha384 => KeyExchangeType::Tls13,
			&Ciphersuite::Tls13WithChacha20Poly1305Sha256 => KeyExchangeType::Tls13,
		}
	}
	pub fn get_protector(&self) -> ProtectorType
	{
		match self {
			&Ciphersuite::TlsEcdheRsaWithAes128GcmSha256 => ProtectorType::Aes128Gcm,
			&Ciphersuite::TlsEcdheRsaWithAes256GcmSha384 => ProtectorType::Aes256Gcm,
			&Ciphersuite::TlsEcdheRsaWithChacha20Poly1305Sha256 => ProtectorType::Chacha20Poly1305,
			&Ciphersuite::TlsEcdheEcdsaWithAes128GcmSha256 => ProtectorType::Aes128Gcm,
			&Ciphersuite::TlsEcdheEcdsaWithAes256GcmSha384 => ProtectorType::Aes256Gcm,
			&Ciphersuite::TlsEcdheEcdsaWithChacha20Poly1305Sha256 => ProtectorType::Chacha20Poly1305,
			&Ciphersuite::Tls13WithAes128GcmSha256 => ProtectorType::Aes128Gcm,
			&Ciphersuite::Tls13WithAes256GcmSha384 => ProtectorType::Aes256Gcm,
			&Ciphersuite::Tls13WithChacha20Poly1305Sha256 => ProtectorType::Chacha20Poly1305,
		}
	}
	pub fn get_prf(&self) -> HashFunction
	{
		match self {
			&Ciphersuite::TlsEcdheRsaWithAes128GcmSha256 => HashFunction::Sha256,
			&Ciphersuite::TlsEcdheRsaWithAes256GcmSha384 => HashFunction::Sha384,
			&Ciphersuite::TlsEcdheRsaWithChacha20Poly1305Sha256 => HashFunction::Sha256,
			&Ciphersuite::TlsEcdheEcdsaWithAes128GcmSha256 => HashFunction::Sha256,
			&Ciphersuite::TlsEcdheEcdsaWithAes256GcmSha384 => HashFunction::Sha384,
			&Ciphersuite::TlsEcdheEcdsaWithChacha20Poly1305Sha256 => HashFunction::Sha256,
			&Ciphersuite::Tls13WithAes128GcmSha256 => HashFunction::Sha256,
			&Ciphersuite::Tls13WithAes256GcmSha384 => HashFunction::Sha384,
			&Ciphersuite::Tls13WithChacha20Poly1305Sha256 => HashFunction::Sha256,
		}
	}
	pub fn algo_const(&self) -> u64
	{
		match self {
			&Ciphersuite::TlsEcdheRsaWithAes128GcmSha256 => 1,
			&Ciphersuite::TlsEcdheRsaWithAes256GcmSha384 => 2,
			&Ciphersuite::TlsEcdheRsaWithChacha20Poly1305Sha256 => 4,
			&Ciphersuite::TlsEcdheEcdsaWithAes128GcmSha256 => 8,
			&Ciphersuite::TlsEcdheEcdsaWithAes256GcmSha384 => 16,
			&Ciphersuite::TlsEcdheEcdsaWithChacha20Poly1305Sha256 => 32,
			&Ciphersuite::Tls13WithAes128GcmSha256 => 64,
			&Ciphersuite::Tls13WithAes256GcmSha384 => 128,
			&Ciphersuite::Tls13WithChacha20Poly1305Sha256 => 256,
		}
	}
	pub fn is_chacha(&self) -> bool
	{
		match self {
			&Ciphersuite::TlsEcdheRsaWithAes128GcmSha256 => false,
			&Ciphersuite::TlsEcdheRsaWithAes256GcmSha384 => false,
			&Ciphersuite::TlsEcdheRsaWithChacha20Poly1305Sha256 => true,
			&Ciphersuite::TlsEcdheEcdsaWithAes128GcmSha256 => false,
			&Ciphersuite::TlsEcdheEcdsaWithAes256GcmSha384 => false,
			&Ciphersuite::TlsEcdheEcdsaWithChacha20Poly1305Sha256 => true,
			&Ciphersuite::Tls13WithAes128GcmSha256 => false,
			&Ciphersuite::Tls13WithAes256GcmSha384 => false,
			&Ciphersuite::Tls13WithChacha20Poly1305Sha256 => true,
		}
	}
}

pub const BAD_CIPHERSUITES: [u16; 57] = [
	0x0001, 0x0002, 0x0003, 0x0004, 0x0005, 0x0006, 0x0008, 0x0009, 0x000B, 0x000C, 0x000E, 0x000F,
	0x0011, 0x0011, 0x0014, 0x0015, 0x0017, 0x0018, 0x0019, 0x001A, 0x001E, 0x0020, 0x0022, 0x0024,
	0x0026, 0x0027, 0x0028, 0x0029, 0x002A, 0x002B, 0x002C, 0x002D, 0x002E, 0x003B, 0x008A, 0x008E,
	0x0092, 0x00B0, 0x00B1, 0x00B4, 0x00B5, 0x00B8, 0x00B9, 0xC001, 0xC002, 0xC006, 0xC007, 0xC00B,
	0xC00C, 0xC010, 0xC011, 0xC015, 0xC016, 0xC033, 0xC039, 0xC03A, 0xC03B,
];
